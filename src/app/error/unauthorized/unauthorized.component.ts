/**
 * Created by Bastien on 21/02/2017.
 */

import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'unauthorized',
  templateUrl: 'unauthorized.component.html',
  styleUrls: ['unauthorized.component.css']
})

export class UnauthorizedComponent {

  constructor(private router: Router) {}

  goToLogin() {
    this.router.navigate(["/login"]);
  }

}


