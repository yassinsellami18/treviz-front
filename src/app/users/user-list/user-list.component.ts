/**
 * Created by Bastien on 15/02/2017.
 */

import {Component, OnInit, OnDestroy} from '@angular/core';
import {UserService} from '../../core/services/user.service';
import {User} from '../../shared/models/users/user.model';
import {FormControl} from '@angular/forms';
import {Skill} from '../../shared/models/tags/skill.model';
import {SkillService} from '../../core/services/skill.service';
import {TagService} from '../../core/services/tag.service';
import {Tag} from '../../shared/models/tags/tag.model';
import {Subject} from 'rxjs';





@Component({
  selector: 'app-user-list',
  templateUrl: 'user-list.component.html',
  styleUrls: ['user-list.component.scss']
})
export class UserListComponent implements OnInit, OnDestroy {

  // Search by name
  searchUserStream = new Subject<string>();
  users: User[] = [];
  name = '';

  // Skill Autocomplete
  filteredSkills: any;
  skillCtrl: FormControl;
  skills: Skill[] = [];
  currentSkills: string[] = [];

  // Interests autocomplete
  filteredTags: any;
  tagCtrl: FormControl;
  tags: Tag[] = [];
  currentTags: string[] = [];

  displayNotFoundMessage = false;
  displayProgressBar = true;

  moreToLoad = true;

  private alive = true;

  constructor(private userService: UserService,
              private skillService: SkillService,
              private tagService: TagService) {

    // Search by username
    this.searchUserStream
      .debounceTime(300)
      .distinctUntilChanged()
      .takeWhile(() => this.alive)
      .subscribe(
        term => {
          this.loadUsers(term);
        }
      );

    // Search by skill
    // TODO : different calls instead of fetching all skills
    this.skillCtrl = new FormControl();
    this.skillService.getSkillsHttp()
      .takeWhile(() => this.alive)
      .subscribe(
        data => {
          this.skills = data;
        },
        err => console.log('impossible to fetch skills')
      );
    this.filteredSkills = this.skillCtrl.valueChanges
      .startWith(null)
      .map(name => this.filterSkill(name));

    // Search by tag
    // TODO : different calls instead of fetching all skills
    this.tagCtrl = new FormControl();
    this.tagService.getTagsHttp()
      .takeWhile(() => this.alive)
      .subscribe(
        data => {
          this.tags = data;
        },
        err => console.log('impossible to fetch tags')
      );
    this.filteredTags = this.tagCtrl.valueChanges
      .startWith(null)
      .map(name => this.filterTag(name));
  }

  ngOnInit(): void {
    this.loadUsers('');
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  search(term: string) {
    this.searchUserStream.next(term);
    this.displayProgressBar = true;
  }

  filterSkill(val: string) {
    return val ? this.skills.filter((skill) => new RegExp(val, 'gi').test(skill.name)) : this.skills;
  }

  addSkill(skill: string) {
    if (skill !== '' && this.skills.findIndex(indexedSkill => indexedSkill.name === skill) === -1) {
      this.currentSkills.push(skill);
    }
  }

  removeSkill(skill: string) {
    this.currentSkills.splice(this.currentSkills.indexOf(skill), 1);
  }

  filterTag(val: string) {
    return val ? this.tags.filter((tag) => new RegExp(val, 'gi').test(tag.name)) : this.tags;
  }

  addTag(tag: string) {
    if (tag !== '' && this.tags.findIndex(indexedTag => indexedTag.name === tag) === -1) {
      this.currentTags.push(tag);
    }
  }

  removeTag(tag: string) {
    this.currentTags.splice(this.currentTags.indexOf(tag), 1);
  }

  emptyFilters() {
    this.currentSkills = [];
    this.currentTags = [];
    this.loadUsers('');
  }

  loadUsers(term: string, offset = 0, nb = 20) {
    this.displayProgressBar = true;
    this.userService
      .getUsers({name: term,
                tags: this.currentTags,
                skills: this.currentSkills,
                offset: offset,
                nb: nb})
      .takeWhile(() => this.alive)
      .subscribe(
          data => {
            this.users = this.users.concat(data);
            this.displayProgressBar = false;
            this.moreToLoad = data.length === nb;
            this.displayNotFoundMessage = data.length > 0;
          },
          err => console.log(err),
          () => console.log('Request Complete')
        );
  }

}
