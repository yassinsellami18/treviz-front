
import {Component, Inject, OnDestroy} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {User} from '../../shared/models/users/user.model';
import {UserDto} from '../../shared/models/users/user.model.dto';
import {FormControl} from '@angular/forms';
import {Skill} from '../../shared/models/tags/skill.model';
import {SkillService} from '../../core/services/skill.service';
import {Observable} from 'rxjs';
import {Tag} from '../../shared/models/tags/tag.model';
import {TagService} from '../../core/services/tag.service';
import {UserService} from '../../core/services/user.service';



@Component({
  selector: 'app-user-edit-dialog',
  templateUrl: 'user-edit-dialog.component.html',
  styleUrls: ['user-edit-dialog.component.scss']
})
export class UserEditDialogComponent implements OnDestroy {

  userDto: UserDto;

  skillCtrl: FormControl;
  filteredSkills: Observable<Skill[]>;
  skills: Skill[] = [];

  tagCtrl: FormControl;
  filteredTags: Observable<Tag[]>;
  tags: Tag[] = [];

  submitted: boolean;
  private alive = true;

  constructor(public dialogRef: MatDialogRef<UserEditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: User,
              private skillService: SkillService,
              private tagService: TagService,
              private userService: UserService) {

    this.userDto = new UserDto();
    this.userDto.username = data.username;
    this.userDto.firstName = data.firstName;
    this.userDto.lastName = data.lastName;
    data.description ? this.userDto.description = data.description : this.userDto.description = '';
    this.userDto.avatarUrl = data.avatarUrl;
    this.userDto.backgroundImageUrl = data.backgroundImageUrl;
    if (data.skills) {
      this.userDto.skills = data.skills.map(a => a.name);

    }
    if (data.interests) {
      this.userDto.interests = data.interests.map(a => a.name);
    }

    this.skillCtrl = new FormControl();
    this.skillService.getSkillsHttp()
      .takeWhile(() => this.alive)
      .subscribe(
        skills => {
          this.skills = skills;
        },
        err => console.log('impossible to fetch skills')
      );

    this.filteredSkills = this.skillCtrl.valueChanges
      .startWith(null)
      .map(name => this.filterSkill(name));

    this.tagCtrl = new FormControl();
    this.tagService.getTagsHttp()
      .takeWhile(() => this.alive)
      .subscribe(
        tags => {
          this.tags = tags;
        },
        err => console.log('impossible to fetch tags')
      );

    this.filteredTags = this.tagCtrl.valueChanges
      .startWith(null)
      .map(name => this.filterTag(name));

    this.submitted = false;
  }

  ngOnDestroy() {
    this.alive = false;
  }

  filterSkill(val: string) {
    return val ? this.skills.filter((skill) => new RegExp(val, 'gi').test(skill.name)) : this.skills;
  }

  addSkill(skill: string) {
    if (skill !== '' && this.userDto.skills.indexOf(skill) === -1) {
      this.userDto.skills.push(skill);
    }
  }

  removeSkill(skill: string) {
    this.userDto.skills.splice(this.userDto.skills.indexOf(skill), 1);
  }

  filterTag(val: string) {
    return val ? this.tags.filter((tag) => new RegExp(val, 'gi').test(tag.name)) : this.tags;
  }

  addTag(tag: string) {
    if (tag !== '' && this.userDto.interests.indexOf(tag) === -1) {
      this.userDto.interests.push(tag);
    }
  }

  removeTag(tag: string) {
    this.userDto.interests.splice(this.userDto.interests.indexOf(tag), 1);
  }

  onSubmit() {
    this.submitted = true;

    if (this.userDto.skills === []) {
      this.userDto.skills = undefined;
    }
    if (this.userDto.interests === []) {
      this.userDto.skills = undefined;
    }

    this.userService.putUser(this.data.username, this.userDto)
      .takeWhile(() => this.alive)
      .subscribe(
        data => this.dialogRef.close(data),
        err => {
          this.submitted = false;
            console.log(err);
            alert('An error occured');
          }
      );
  }

}
