import { Component, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import { UserService } from '../../core/services/user.service';
import { User } from '../../shared/models/users/user.model';
import { ProjectService } from '../../core/services/project/project.service';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Project } from '../../shared/models/project/project.model';
import { Community } from '../../shared/models/community/community.model';
import { CommunityService } from '../../core/services/community/community.service';
import { ProjectInvitation } from '../../shared/models/project/project-invitation.model';
import { ProjectCandidacy } from '../../shared/models/project/project-candidacy.model';
import { ProjectCandidacyService } from '../../core/services/project/project-candidacy.service';
import { ProjectInvitationService } from '../../core/services/project/project-invitation.service';
import { CommunityCandidacyService } from '../../core/services/community/community-candidacy.service';
import { CommunityInvitationService } from '../../core/services/community/community-invitation.service';
import { UserEditDialogComponent } from './user-edit-dialog.component';
import { CommunityInvitation } from '../../shared/models/community/community-invitation.model';
import { CommunityCandidacy } from '../../shared/models/community/community-candidacy.model';
import { ChatRoomCreateDialogComponent } from '../../chat/chat-room-create-dialog/chat-room-create-dialog.component';
import { ProjectJob } from '../../shared/models/project/project-job.model';
import { ProjectJobService } from '../../core/services/project/project-job.service';
import { CurrentUserService } from '../../core/services/current-user.service';


/**
 * Created by Bastien on 26/02/2017.
 */
@Component({
    selector: 'app-user-detail',
    templateUrl: 'user-detail.component.html',
    styleUrls: ['user-detail.component.scss']
})
export class UserDetailComponent implements OnInit, OnDestroy {

  user: User;

  projects: Project[] = [];
  projectInvitations: ProjectInvitation[] = [];
  projectCandidacies: ProjectCandidacy[] = [];

  communities: Community[] = [];
  communityInvitations: CommunityInvitation[] = [];
  communityCandidacies: CommunityCandidacy[] = [];

  jobs: ProjectJob[] = [];

  newRoomRef: MatDialogRef<ChatRoomCreateDialogComponent>;

  editPossible = false;
  avatarBeingUploaded = false;
  projectsFetched = false;
  communitiesFetched = false;
  userFetched = false;

  private alive = true;

  constructor(private userService: UserService,
              private projectService: ProjectService,
              private projectCandidacyService: ProjectCandidacyService,
              private projectInvitationService: ProjectInvitationService,
              private projectJobService: ProjectJobService,
              private communityService: CommunityService,
              private communityCandidacyService: CommunityCandidacyService,
              private communityInvitationService: CommunityInvitationService,
              private currentUserService: CurrentUserService,
              private route: ActivatedRoute,
              public dialog: MatDialog,
              public viewContainerRef: ViewContainerRef,
              public router: Router) { }

  ngOnInit() {
    this.fetchData();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  private fetchData() {

    this.route.params
      .takeWhile(() => this.alive)
      .subscribe((params: Params) => {

        this.clear();

        const username = params['username'];

        this.editPossible = (username === localStorage.getItem('username'));

        this.userService.getUser(username)
          .takeWhile(() => this.alive)
          .subscribe(
            data => this.user = data,
            err => {
              console.log(err);
              alert('This user could not be fetched.');
            },
            () => {
              this.userFetched = true;
            }
          );

        this.projectService.getProjects({user: username})
          .takeWhile(() => this.alive)
          .subscribe(
            data => this.projects = data,
            err => console.log(err),
            () => {
              this.projectsFetched = true;
            }
          );

        this.projectInvitationService.getProjectInvitations({user: username})
          .takeWhile(() => this.alive)
          .subscribe(
            data => this.projectInvitations = data,
            err => console.log(err)
          );

        this.projectCandidacyService.getProjectCandidacies({user: username})
          .takeWhile(() => this.alive)
          .subscribe(
            data => this.projectCandidacies = data,
            err => console.log(err)
          );

        this.projectJobService.getJobs({holder: username})
          .takeWhile(() => this.alive)
          .subscribe(
            data => this.jobs = data,
            err => console.log(err)
          );

        this.communityService.getCommunities({user: username})
          .takeWhile(() => this.alive)
          .subscribe(
            data => this.communities = data,
            err => console.log(err),
            () => {
              this.communitiesFetched = true;
            }
          );

        this.communityInvitationService.getCommunityInvitations({user: username})
          .takeWhile(() => this.alive)
          .subscribe(
            data => this.communityInvitations = data,
            err => console.log(err)
          );

        this.communityCandidacyService.getCommunityCandidacies({user: username})
          .takeWhile(() => this.alive)
          .subscribe(
            data => this.communityCandidacies = data,
            err => console.log(err)
          );

      });
  }

  goToCommunity(community: Community): void {
    this.router.navigate(['/communities', community.hash]);
  }

  openEditDialog() {
    const config = new MatDialogConfig();
    config.data = this.user;

    const dialogRef = this.dialog.open(UserEditDialogComponent, config);

    dialogRef.afterClosed().takeWhile(() => this.alive)
      .subscribe( result => {
    });

    dialogRef.afterClosed().takeWhile(() => this.alive)
      .subscribe(result => {
      if (typeof result !== 'undefined' && result !== null && result !== false) {
        this.user = result;
      }
    });
  }

  changeAvatar(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];

      this.avatarBeingUploaded = true;

      this.userService.postAvatar(file, this.user)
        .takeWhile(() => this.alive)
        .subscribe(
          data => {
            this.user = data;
            this.currentUserService.getCurrentUser().avatarUrl = data.avatarUrl;
            this.avatarBeingUploaded = false;
          },
          error => {
            alert('The image could not be uploaded. It must not be over 500KB.');
            this.avatarBeingUploaded = false;
          }
        );
    }
  }

  changeBackgroundPicture(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];

      this.avatarBeingUploaded = true;

      this.userService.postBackgroundImage(file, this.user)
        .takeWhile(() => this.alive)
        .subscribe(
          data => {
            this.user = data;
            this.avatarBeingUploaded = false;
          },
          error => {
            alert('The image could not be uploaded. It must not be over 500KB.');
            this.avatarBeingUploaded = false;
          }
        );
    }
  }

  openChatCreation() {
    const config = new MatDialogConfig();
    config.viewContainerRef = this.viewContainerRef;
    this.newRoomRef = this.dialog.open(ChatRoomCreateDialogComponent, config);

    this.newRoomRef.componentInstance.addUser(this.user);

    this.newRoomRef.afterClosed().takeWhile(() => this.alive)
      .subscribe(
      data => {
        if (data) {
          this.router.navigate(['/chat']);
        }
      },
      err => console.log(err)
    );

  }

  /**
   * This function removes all the data that was stored in this component.
   * When the user is redirected from /users/x to /users/y, it therefore refreshes all the data.
   */
  clear() {
    this.user = null;
    this.projects = [];
    this.projectInvitations = [];
    this.projectCandidacies = [];
    this.communities = [];
    this.communityInvitations = [];
    this.communityCandidacies = [];
    this.editPossible = false;
    this.userFetched = false;
    this.projectsFetched = false;
    this.communitiesFetched = false;
    this.avatarBeingUploaded = false;
  }
}
