import { Component, OnDestroy, OnInit } from '@angular/core';
import { TaskService } from '../core/services/kanban/task.service';
import { ProjectJobService } from '../core/services/project/project-job.service';
import { Task } from '../shared/models/kanban/task.model';
import { ProjectJob } from '../shared/models/project/project-job.model';
import { CurrentUserService } from '../core/services/current-user.service';



/**
 * Created by huber on 18/02/2017.
 */
@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  tasks: Task[] = [];
  tasksFetched = false;

  supervisedTasks: Task[] = [];
  supervisedTasksLoaded = false;

  jobs: ProjectJob[] = [];
  jobsFetched = false;

  stepperStatus = {
    skillsSpecified: this.currentUserService.getCurrentUser().skills.length > 0,
    tagsSpecified: this.currentUserService.getCurrentUser().interests.length > 0,
    descriptionFilled: this.currentUserService.getCurrentUser().description,
    projects: this.currentUserService.getCurrentUser().projectMemberships.length > 0,
    communities: this.currentUserService.getCurrentUser().communityMemberships.length > 0,
  };

  private alive = true;

  constructor(private taskService: TaskService,
              private jobService: ProjectJobService,
              public currentUserService: CurrentUserService) { }

  ngOnInit(): void {

    this.taskService.getTasks({assignee: localStorage.getItem('username')})
      .takeWhile(() => this.alive)
      .finally(() => this.tasksFetched = true)
      .subscribe(
        data => this.tasks = data,
        err => console.log(err)
      );

    this.taskService.getTasks({supervisor: localStorage.getItem('username')})
      .takeWhile(() => this.alive)
      .finally(() => this.supervisedTasksLoaded = true)
      .subscribe(
        data => this.supervisedTasks = data,
        err => console.log(err)
      );

    this.jobService.getJobs({holder: localStorage.getItem('username')})
      .takeWhile(() => this.alive)
      .finally(() => this.jobsFetched = true)
      .subscribe(
        data => this.jobs = data,
        err => console.log(err)
      );
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  /**
   * Updates a task, or removes it if it was archived.
   * @param task Task to update
   */
  updateTask(task: Task): void {
    if (task.archived) {
      this.deleteTask(task);
    } else {
      this.supervisedTasks[this.supervisedTasks.findIndex(indexedTask => indexedTask.hash === task.hash)] = task;
    }
  }

  /**
   * Removes a task from the display.
   * @param task Task to remove from the display.
   */
  deleteTask(task: Task): void {
    this.supervisedTasks.splice(this.supervisedTasks.findIndex(indexedTask => indexedTask.hash === task.hash), 1);
  }

}
