import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from './core/services/auth/auth.service';
import {CurrentUserService} from './core/services/current-user.service';
import {ChatRoomService} from './core/services/chat/chat-room.service';
import {NotificationService} from './core/services/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  private alive = true;

  constructor(private auth: AuthService,
              private chatService: ChatRoomService,
              private notificationService: NotificationService,
              private currentUserService: CurrentUserService
  ) { }

  /*
   * If the user is already logged in, load the current user in the appropriate service and trigger notifications.
   */
  ngOnInit(): void {
    if (this.auth.loggedIn()) {

      this.currentUserService.loadCurrentUser();
      this.currentUserService.loading
        .subscribe(
          state => {
            if (!(state.loadingRooms || state.loadingCommunities || state.loadingProjects || this.notificationService.listening)) {
              this.notificationService.listenToNotifications();
            }
          }
        );
    }
  }

  ngOnDestroy () {
    this.alive = false;
  }

}
