import {
  Component, OnInit, Input, AfterViewChecked, ViewChild, ElementRef, OnChanges, OnDestroy,
  Output, EventEmitter
} from '@angular/core';
import { Room } from '../../shared/models/chat/room.model';
import { MessageDto } from '../../shared/models/chat/message.model.dto';
import { MatSidenav, MatDialog } from '@angular/material';
import { NotificationService } from '../../core/services/notification.service';
import { ChatRoomDetailDialogComponent } from '../chat-room-detail-dialog/chat-room-detail-dialog.component';
import { ChatMessageService } from '../../core/services/chat/chat-message.service';
import {ChatRoomService} from '../../core/services/chat/chat-room.service';
import {CurrentUserService} from '../../core/services/current-user.service';
import {SubscriptionLike as ISubscription} from 'rxjs';


/**
 * Created by huber on 22/07/2017.
 */
@Component({
    selector: 'app-chat-room',
    templateUrl: 'chat-room.component.html',
    styleUrls: ['chat-room.component.scss']
})
export class ChatRoomComponent implements OnChanges, OnDestroy, AfterViewChecked {

    @Input()
    room: Room;

    @Input()
    chatlist: MatSidenav;

    @Output()
    onChatRoomUpdate = new EventEmitter<Room>();

    firstLoad = true;

    messageToPost = new MessageDto();
    submitted = false;

    noMoreMessagesToLoad = false;
    loadingMessages = true;
    newMessages = true;

    isRoomMember: boolean;

    private subscription: ISubscription;

    @ViewChild('scrollMe')
    private myScrollContainer: ElementRef;

    private alive = true;

    constructor(private chatMessageService: ChatMessageService,
                private chatRoomService: ChatRoomService,
                private notificationService: NotificationService,
                private currentUserService: CurrentUserService,
                public dialog: MatDialog) {

    }

    /**
     * When the input room changes, checks if the user is member of the room.
     * If he or she is, fetch the messages.
     */
    ngOnChanges() {
      this.clear();
      if (this.room != null) {
        this.isRoomMember = this.room.users.some(user => user.username === localStorage.getItem('username'));
        if ( this.room.messages === undefined ) {
          this.room.messages = [];
        }
        if (this.isRoomMember) {
          this.loadMessages();
        } else {
          this.loadingMessages = false;
          this.noMoreMessagesToLoad = true;
          this.firstLoad = false;
        }

        this.notificationService.subscribeToNotificationService()
          .takeWhile(() => this.alive)
          .subscribe(
            msg => {
              if (msg.type === 'CHAT_POST_MESSAGE' && msg.content.room_hash === this.room.hash) {
                this.room.active = false;
                this.currentUserService.getCurrentUser().rooms
                  .find(room => room.hash === msg.content.room_hash).messages.push(msg.content);
                this.newMessages = true;
              }
            }
          );
      }
    }

    ngOnDestroy() {
      this.alive = false;
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
    }

    ngAfterViewChecked() {
      if (this.newMessages && !this.loadingMessages) {
        this.scrollToBottom();
        this.newMessages = false;
      }
    }

    loadMessages() {
      this.loadingMessages = true;
      this.subscription = this.chatMessageService.getMessages(this.room.hash, {limit: 20, offset: this.room.messages.length})
        .finally(() => {
          this.firstLoad = false;
          this.loadingMessages = false;
        })
        .subscribe(
          data => {
            this.room.active = false;
            this.noMoreMessagesToLoad = (data.length < 20);
            if (data.length > 0) {
              this.room.messages = data
                .reverse()
                .concat(this.room.messages);
            }
          },
          err => console.log(err)
        );
    }

    scrollToBottom(): void {
      try {
        this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
      } catch (err) {
        console.log(err);
      }
    }

    sendMessage() {
      this.submitted = true;
      // Change \n with \n\n so that markdown module interprets this as a line break.
      this.messageToPost.text = this.messageToPost.text.split('\n').join('\n\n');
      this.chatMessageService.postMessage(this.room.hash, this.messageToPost)
        .subscribe(
          data => {
            const recipients = this.room.users.map(user => user.username);
            recipients.splice(this.room.users.findIndex(user => user.username === localStorage.getItem('username')), 1);
            const notification = {
              type: 'CHAT_POST_MESSAGE',
              content: data,
              recipients: recipients
            };
            this.notificationService.sendNotification(notification);

            // Add the message to the room.
            this.currentUserService.getCurrentUser().rooms.find(room => room.hash === data.room_hash).messages.push(data);

            if (data.room_hash === this.room.hash) {
              this.newMessages = true;
              this.messageToPost = new MessageDto();
            }
            },
          err => console.log(err),
          () => this.submitted = false
        );
    }

    private clear(): void {
      if (this.subscription) {
        this.subscription.unsubscribe();
      }
      this.noMoreMessagesToLoad = false;
      this.loadingMessages = true;
      this.newMessages = true;
      this.messageToPost = new MessageDto();
      this.submitted = false;
    }

    openDialog() {
      const dialogRef = this.dialog.open(ChatRoomDetailDialogComponent);
      dialogRef.componentInstance.room = this.room;
      dialogRef.afterClosed().subscribe(
        data => {
          if (data) {
            this.room = data;
            this.onChatRoomUpdate.emit(data);
          }
        },
        err => console.log(err)
      );
    }

}
