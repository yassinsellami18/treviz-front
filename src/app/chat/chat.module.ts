import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { MatSidenavModule, MatToolbarModule } from '@angular/material';
import { ChatListComponent } from './chat-list/chat-list.component';
import { ChatRoutingModule } from './chat-routing.module';
import { ChatRoomDetailDialogComponent } from './chat-room-detail-dialog/chat-room-detail-dialog.component';
import { ChatRoomCreateDialogComponent } from './chat-room-create-dialog/chat-room-create-dialog.component';
import { ChatRoomComponent } from './chat-room/chat-room.component';
import {ChatMessageComponent} from "./chat-message/chat-message.component";

@NgModule({
  imports: [
    SharedModule,
    ChatRoutingModule,
    MatSidenavModule,
    MatToolbarModule,
  ],
  declarations: [
    ChatListComponent,
    ChatMessageComponent,
    ChatRoomComponent,
    ChatRoomCreateDialogComponent,
    ChatRoomDetailDialogComponent,
  ],
  exports: [],
  providers: [],
  entryComponents: [
    ChatRoomCreateDialogComponent,
    ChatRoomDetailDialogComponent,
  ]
})
export class ChatModule { }
