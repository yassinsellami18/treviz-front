import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '../core/services/auth/auth-gard.service';
import {ChatListComponent} from './chat-list/chat-list.component';

export const routes: Routes = [
  { path: '', component: ChatListComponent, canActivate: [ AuthGuard ]},
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class ChatRoutingModule { }
