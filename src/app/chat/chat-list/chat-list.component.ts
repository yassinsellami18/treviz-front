import {OnInit, Component, ViewChild, OnDestroy} from '@angular/core';
import {MatSidenav, MatDialog, MatDialogConfig} from '@angular/material';
import { ChatRoomCreateDialogComponent } from '../chat-room-create-dialog/chat-room-create-dialog.component';
import { ChatRoomService } from '../../core/services/chat/chat-room.service';
import { Room } from '../../shared/models/chat/room.model';
import {CurrentUserService} from '../../core/services/current-user.service';
import {Project} from '../../shared/models/project/project.model';
import {Community} from '../../shared/models/community/community.model';
import {SubscriptionLike as ISubscription} from 'rxjs';
import {NotificationService} from '../../core/services/notification.service';


/**
 * Created by huber on 11/07/2017.
 */
@Component({
  selector: 'app-chat',
  templateUrl: 'chat-list.component.html',
  styleUrls: ['chat-list.component.scss']
})
export class ChatListComponent implements OnInit, OnDestroy {

  selectedElement: Project|Community;
  roomsInWhichMember: Room[] = [];
  roomsInWhichNotMember: Room[] = [];
  selectedRoom: Room;

  @ViewChild('chatlist')
  chatlist: MatSidenav;

  private subscriptions: ISubscription;
  private alive = true;

  constructor(private chatRoomService: ChatRoomService,
              public currentUserService: CurrentUserService,
              public notificationService: NotificationService,
              public dialog: MatDialog) {}

  /**
   * At component initiation, remove the notification for new messages.
   * Fetch rooms that are not linked to any project or community.
   */
  ngOnInit() {
    this.chatRoomService.newMessages = false;
    this.roomsInWhichMember = this.currentUserService.getCurrentUser().rooms
      .filter((room: Room) => room.project === undefined && room.community === undefined);
  }

  /**
   * Unsubscribe from observables at component destruction.
   */
  ngOnDestroy() {
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }
    this.alive = false;
  }

  /**
   * Display the rooms of a project, community, or those linked to none.
   * Unsubscribe from previous observables.
   * @param {Project} project
   * @param {Community} community
   */
  showRooms({project = null, community = null}: {project?: Project, community?: Community}): void {
    if (project) {
      this.selectedElement = project;
      this.notificationService.pendingNotifications.chat -= project.pendingNotification;
      this.selectedElement.pendingNotification = 0;
      this.roomsInWhichMember = this.currentUserService.getCurrentUser().rooms
        .filter((room: Room) => room.project !== undefined && room.project.hash === project.hash);
    } else if (community) {
      this.selectedElement = community;
      this.notificationService.pendingNotifications.chat -= community.pendingNotification;
      this.selectedElement.pendingNotification = 0;
      this.roomsInWhichMember = this.currentUserService.getCurrentUser().rooms
        .filter((room: Room) => room.project !== undefined && room.community.hash === community.hash);
    } else {
      this.selectedElement = undefined;
      this.notificationService.pendingNotifications.chat -= this.notificationService.pendingNotifications.unlinkedChat;
      this.roomsInWhichMember = this.currentUserService.getCurrentUser().rooms
        .filter((room: Room) => room.project === undefined && room.community === undefined);
      this.notificationService.pendingNotifications.unlinkedChat = 0;
    }
    if (this.roomsInWhichMember.length > 0) {
      this.selectedRoom = this.roomsInWhichMember[0];
    } else {
      this.selectedRoom = undefined;
    }

    this.loadRoomsInWhichNotMember({project, community});
  }

  /**
   * Load the rooms that are linked to a project or community, but in which the user is not already member.
   * @param {Project} project
   * @param {Community} community
   */
  loadRoomsInWhichNotMember({project = null, community = null}: {project?: Project, community?: Community}) {

    /**
     * Remove existing rooms and unsubscribe from any observable that may add any.
     * @type {any[]}
     */
    this.roomsInWhichNotMember = [];
    if (this.subscriptions) {
      this.subscriptions.unsubscribe();
    }

    const options = {
      project: '',
      community: ''
    };

    project !== null ? options.project = project.hash : options.project = '';
    community !== null ? options.community = community.hash : options.community = '';

    this.subscriptions = this.chatRoomService.getRooms(options)
      .takeWhile(() => this.alive)
      .subscribe(
        rooms => {
          this.roomsInWhichNotMember = rooms
            .filter((room: Room) => !room.users.find(user => user.username === localStorage.getItem('username')));
        },
        err => console.log(err)
      );
  }

  goToRoom(room: Room) {
    this.selectedRoom = room;
    if (window.innerWidth <= 768) {
      this.chatlist.close();
    }
  }

  /**
   * Opens the Chat room creation dialog.
   * When the dialog is closed, adds the created chat room to the list.
   */
  openCreateDialog() {
    const config = new MatDialogConfig();
    config.data = {
      project: '',
      community: ''
    };

    /*
     * Checks if the property 'shortDescription' exists on the object. If it does, it's a project.
     */
    if (this.selectedElement) {
      if (this.selectedElement.hasOwnProperty('shortDescription')) {
        config.data.project = this.selectedElement.hash;
      } else {
        config.data.community = this.selectedElement.hash;
      }
    }
    const dialogRef = this.dialog.open(ChatRoomCreateDialogComponent, config);
    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(
      data => {
        if (data) {
          this.selectedRoom = data;
          this.roomsInWhichMember.push(data);
          this.currentUserService.getCurrentUser().rooms.push(data);
        }
      },
      err => console.log(err)
    );
  }

  /**
   * Updates a room's name if it was updated, puts the room in the correct array (roomsInWhichMember or roomsInWhichNotMember)
   * @param {Room} room
   */
  updateChatRoomStatus(room: Room) {
    const roomsInWhichMemberIndex = this.roomsInWhichMember.findIndex(indexedRoom => indexedRoom.hash === room.hash);
    const roomsInWhichNotMemberIndex = this.roomsInWhichNotMember.findIndex(indexedRoom => indexedRoom.hash === room.hash);
    const isUserMember = room.users.some(user => user.username === localStorage.getItem('username'));

    console.log('Room in which member index ?:' + roomsInWhichMemberIndex);
    console.log('Rooms in which not member index ?:' + roomsInWhichNotMemberIndex);
    console.log('Is member of the room ?');
    console.log(isUserMember);

    if (isUserMember) {
      if (roomsInWhichMemberIndex > -1 ) {
        this.roomsInWhichMember[roomsInWhichMemberIndex] = room;
        console.log('Displayed room updated');
      } else {
        console.log('Room moved from not member to member area.');
        this.roomsInWhichMember.push(room);
        this.roomsInWhichNotMember.splice(roomsInWhichNotMemberIndex, 1);
      }
    } else {
      if (roomsInWhichNotMemberIndex > -1 ) {
        this.roomsInWhichNotMember[roomsInWhichNotMemberIndex] = room;
        console.log('updated room in which the user is not member');
      } else {
        console.log('Room moved from member to not member area.');
        this.roomsInWhichNotMember.push(room);
        this.roomsInWhichMember.splice(roomsInWhichMemberIndex, 1);
      }
    }
  }


}
