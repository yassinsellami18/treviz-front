import { Component, Inject, OnDestroy } from '@angular/core';
import { RoomDto } from '../../shared/models/chat/room.model.dto';
import { User } from '../../shared/models/users/user.model';
import { FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { UserService } from '../../core/services/user.service';
import { NotificationService } from '../../core/services/notification.service';
import { ChatRoomService } from '../../core/services/chat/chat-room.service';





@Component({
  selector: 'app-chat-room-create-dialog',
  templateUrl: 'chat-room-create-dialog.component.html',
  styleUrls: ['chat-room-create-dialog.component.scss']
})
export class ChatRoomCreateDialogComponent implements OnDestroy {

  public room = new RoomDto();
  public filteredUsers: User[];
  public userCtrl: FormControl;
  public usersToInvite: User[] = [];

  public isSubmitted = false;

  private alive = true;

  constructor(public dialogRef: MatDialogRef<ChatRoomCreateDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private chatRoomService: ChatRoomService,
              private userService: UserService,
              private notificationService: NotificationService) {

    if (data.project !== '') {
      this.room.project = data.project;
    } else if (data.community !== '') {
      this.room.community = data.project;
    }

    this.userCtrl = new FormControl();
    this.userCtrl.valueChanges
      .takeWhile(() => this.alive)
      .debounceTime(300)
      .distinctUntilChanged()
      .subscribe(
        name => {
          this.filteredUsers = [];
          if (name != null && name.length >= 2) {
            this.userService.getUsers({name: name})
              .takeWhile(() => this.alive)
              .subscribe(
                users => this.filteredUsers = users,
                err => console.log(err)
              );
          }
        }
      );
  }

  ngOnDestroy() {
    this.alive = false;
  }

  addUser(user: User): void {
    if (user != null) {
      this.usersToInvite.push(user);
    }
  }

  addUserFromUsername(name): void {
    const user = this.filteredUsers.filter((s) => new RegExp(name, 'gi').test(s.username))[0];
    this.addUser(user);
  }

  removeUser(user: User): void {
    this.usersToInvite.splice(this.usersToInvite.indexOf(user), 1);
  }

  onSubmit(): void {
    this.isSubmitted = true;
    for (const user of this.usersToInvite) {
      this.room.users.push(user.username);
    }
    this.chatRoomService.postRoom(this.room)
      .takeWhile(() => this.alive)
      .subscribe(
        room => {
          const notification = {
            type: 'CHAT_POST_ROOM',
            recipients: this.usersToInvite.map(user => user.username),
            content: room
          };
          room.active = true;
          // this.chatRoomService.parseRooms([room]);
          this.notificationService.sendNotification(notification);
          this.dialogRef.close(room);
        },
        err => alert(err),
        () => this.isSubmitted = false
      );
  }

}
