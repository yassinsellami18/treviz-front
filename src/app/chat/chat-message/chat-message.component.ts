import { Component, Input, OnInit } from '@angular/core';
import { Message } from '../../shared/models/chat/message.model';

@Component({
  moduleId: module.id,
  selector: 'app-chat-message',
  templateUrl: 'chat-message.component.html',
  styleUrls: ['chat-message.component.scss']
})
export class ChatMessageComponent implements OnInit {

  @Input() message: Message;
  time = new Date();

  constructor() {
  }

  ngOnInit() {
  }
}
