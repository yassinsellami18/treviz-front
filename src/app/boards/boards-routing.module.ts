import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../core/services/auth/auth-gard.service';
import { BoardsComponent } from './boards.component';

export const routes: Routes = [
  { path: '', component: BoardsComponent, canActivate: [ AuthGuard ]},
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class BoardsRoutingModule { }
