import { Component, OnInit, OnDestroy } from '@angular/core';
import { BoardService } from '../core/services/kanban/board.service';
import { CurrentUserService } from '../core/services/current-user.service';
import { Board } from '../shared/models/kanban/board.model';
import { Project } from '../shared/models/project/project.model';
import { MatDialog } from '@angular/material';
import { BoardsSearchDialogComponent } from './boards-search-dialog/boards-search-dialog/boards-search-dialog.component';
import { User } from '../shared/models/users/user.model';

@Component({
    selector: 'app-boards',
    templateUrl: 'boards.component.html',
    styleUrls: ['./boards.component.scss']
})
export class BoardsComponent implements OnInit, OnDestroy {

    private alive = true;
    public boardsFetched = false;
    public personalBoardsFetched = false;
    public selectedElementBoardsFetched = false;
    public query = false;
    public groupedPersonalBoards: {project: Project, boards: Board[]}[] = [];
    public groupedBoards: {project: Project, boards: Board[]}[] = [];
    public selectedElement: {project?: Project, boards: Board[], user?: User} = null;

    constructor(public dialog: MatDialog,
                private boardsService: BoardService,
                private currentUserService: CurrentUserService) {}

    ngOnInit() {
        // Firstly, fetch the boards of a user
        this.boardsService.getBoards({user: this.currentUserService.getCurrentUser().username})
            .takeWhile(() => this.alive)
            .subscribe(
                boards => {
                    // Group those boards according to their project
                    boards.forEach((board: Board) => {
                        if (this.groupedPersonalBoards.some((element) => element.project.hash === board.project.hash)) {
                            this.groupedPersonalBoards.find((element) => element.project.hash === board.project.hash).boards.push(board);
                        } else {
                            this.groupedPersonalBoards.push({
                                project: board.project,
                                boards: [board]
                            });
                        }
                    });

                    // Then fetch all the boards, group them, and removes those in which the user takes part.
                    this.boardsService.getBoards({})
                        .subscribe(
                            generalBoards => generalBoards.forEach((board: Board) => {
                                if (!boards.some((personalBoard) => personalBoard.hash === board.hash)) {
                                    if (this.groupedBoards.some((element) => element.project.hash === board.project.hash)) {
                                        this.groupedBoards.find((element) => element.project.hash === board.project.hash)
                                            .boards.push(board);
                                    } else {
                                        this.groupedBoards.push({
                                            project: board.project,
                                            boards: [board]
                                        });
                                    }
                                }
                            }),
                            err => console.log(err),
                            () => this.boardsFetched = true
                        );
                },
                err => console.log(err),
                () => this.personalBoardsFetched = true
            );
    }

    ngOnDestroy() {
        this.alive = false;
    }

    openSearchDialog(): void {
        const dialogRef = this.dialog.open(BoardsSearchDialogComponent);

        dialogRef.afterClosed()
            .takeWhile(() => this.alive)
            .subscribe(
            result => {
                if (result == null) { return; }
                if (result.firstName != null) {
                    this.selectedElement = { user: result, boards: [] };
                    this.selectedElementBoardsFetched = false;
                    // Result is a user
                    this.boardsService.getBoards({user: result.username})
                        .takeWhile(() => this.alive)
                        .subscribe(
                            boards => this.selectedElement.boards = boards,
                            err => console.log(err),
                            () => this.selectedElementBoardsFetched = true
                        );
                } else if (result.hash != null) {
                    // Result is a project
                    this.selectedElement = { project: result, boards: [] };
                    this.selectedElementBoardsFetched = false;
                    this.boardsService.getBoards({project: result.hash})
                        .takeWhile(() => this.alive)
                        .subscribe(
                            boards => this.selectedElement.boards = boards,
                            err => console.log(err),
                            () => this.selectedElementBoardsFetched = true
                        );
                }
            }
        );
    }

    clearSearch(): void {
        this.selectedElement = null;
    }

}
