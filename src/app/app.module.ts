import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module'; // Routing Module
import { JwtModule, JwtModuleOptions } from '@auth0/angular-jwt'; // Authentication with JWT

/*
 * Components
 */
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { InfoDialogComponent } from './info/info.component';

/* Core and Shared Modules */
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { AccountModule } from './account/account.module';
import { AuthModule } from './auth.module';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from 'environments/environment';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    InfoDialogComponent,
    InfoDialogComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    AuthModule,
    BrowserAnimationsModule,
    CoreModule.forRoot(),
    SharedModule,
    AccountModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production })
  ],
  bootstrap: [
    AppComponent,
  ],
  entryComponents: [
    InfoDialogComponent,
  ]
})
export class AppModule { }
