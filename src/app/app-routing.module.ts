import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// components that we will create routes for
import { HomeComponent } from './home/home.component';
import { AuthGuard } from './core/services/auth/auth-gard.service';

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [ AuthGuard ]},
  { path: 'chat', loadChildren: 'app/chat/chat.module#ChatModule' },
  { path: 'communities', loadChildren: 'app/communities/communities.module#CommunitiesModule' },
  { path: 'jobs', loadChildren: 'app/jobs/jobs.module#JobsModule' },
  { path: 'projects', loadChildren: 'app/projects/projects.module#ProjectsModule' },
  { path: 'boards', loadChildren: 'app/boards/boards.module#BoardsModule' },
  { path: 'users', loadChildren: 'app/users/users.module#UsersModule'},
  { path: 'settings', loadChildren: 'app/settings/settings.module#SettingsModule'},
  { path: '', loadChildren: 'app/account/account.module#AccountModule'},
  { path: '', loadChildren: 'app/error/error.module#ErrorModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
