import {Component} from "@angular/core";

@Component({
  selector: 'app-success-snack-bar',
  templateUrl: 'success-snack-bar.component.html',
})
export class SuccessSnackBarComponent {}
