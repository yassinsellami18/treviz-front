import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {Community} from '../../models/community/community.model';
import {Project} from '../../models/project/project.model';
import {PostsService} from '../../../core/services/posts.service';
import {Post} from '../../models/newsfeed/post.model';
import {PostDto} from '../../models/newsfeed/post.model.dto';
import {Task} from "../../models/kanban/task.model";
import {ProjectJob} from "../../models/project/project-job.model";
import {Document} from "../../models/document/document.model";



@Component({
    moduleId: module.id,
    selector: 'app-post-list',
    templateUrl: 'post-list.component.html',
    styleUrls: ['post-list.component.scss']
})
export class PostListComponent implements OnInit, OnDestroy {

  @Input()
  public community: Community;

  @Input()
  public project: Project;

  @Input()
  public task: Task;

  @Input()
  public job: ProjectJob;

  @Input()
  public document: Document;

  @Input()
  public canPost = true;

  /**
   * When set to true, the posts will display where they come from (project, community, task...)
   * This is useful in the home post thread, where posts are mixed from various sources.
   * @type {boolean}
   */
  @Input()
  public showOrigin = false;

  public posts: Post[] = [];

  public postDto = new PostDto();

  public loading = true;

  public submitted = false;

  private alive = true;

  constructor(private postsService: PostsService) {}

  ngOnInit() {
    const options = {
      community: '',
      project: '',
      task: '',
      job: '',
      document: '',
    };

    if (this.community) {
      options.community = this.community.hash;
      this.postDto.community = this.community.hash;
    } else if (this.project) {
      options.project = this.project.hash;
      this.postDto.project = this.project.hash;
    } else if (this.task) {
      options.task = this.task.hash;
      this.postDto.task = this.task.hash;
    } else if (this.job) {
      options.job = this.job.hash;
      this.postDto.job = this.job.hash;
    } else if (this.document) {
      options.document = this.document.hash;
      this.postDto.document = this.document.hash;
    }

    this.postsService.getPosts(options)
      .finally(() => this.loading = false)
      .takeWhile(() => this.alive)
      .subscribe(
        posts => this.posts = posts,
        err => console.log(err),
        () => this.loading = false
      );
  }

  ngOnDestroy() {
    this.alive = false;
  }

  publishPost() {
    this.submitted = true;

    this.postsService.postPost(this.postDto)
      .takeWhile(() => this.alive)
      .subscribe(
        data => {
          this.posts.unshift(data);
          this.postDto.message = '';
          this.postDto.link = '';
          this.submitted = false;
        },
        err => console.log(err)
      );
  }

}
