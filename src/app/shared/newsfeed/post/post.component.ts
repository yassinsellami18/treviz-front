import {Component, Input, OnDestroy} from '@angular/core';
import { Post } from '../../models/newsfeed/post.model';
import {CommentDto} from '../../models/newsfeed/comment.model.dto';
import {CommentsService} from '../../../core/services/comments.service';

@Component({
    moduleId: module.id,
    selector: 'app-post',
    templateUrl: 'post.component.html',
    styleUrls: ['post.component.scss']
})
export class PostComponent implements OnDestroy {

  @Input()
  post: Post;

  @Input()
  showOrigin = false;

  comment = new CommentDto();
  submitted = false;

  private alive = true;

  constructor(private commentService: CommentsService) {}

  ngOnDestroy() {
    this.alive = false;
  }

  submitComment() {
    this.submitted = true;
    this.commentService.postComment(this.post.hash, this.comment)
      .takeWhile(() => this.alive)
      .subscribe(
        data => {
          this.post.comments.push(data);
          this.comment.message = '';
          },
        err => console.log(err),
        () => this.submitted = false
      );
  }

}
