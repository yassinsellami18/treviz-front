import { Component, Input, OnInit } from '@angular/core';
import { Comment } from '../../models/newsfeed/comment.model';

@Component({
    moduleId: module.id,
    selector: 'app-comment',
    templateUrl: 'comment.component.html',
    styleUrls: ['comment.component.scss']
})
export class CommentComponent implements OnInit {

  @Input()
  comment: Comment;

  constructor() {}

  ngOnInit() {
    if (this.comment) {
      const now = new Date();
      const commentPublicationDate = new Date(this.comment.publicationDate);

      if (now.getTime() - commentPublicationDate.getTime() < 600000) {
        this.comment.interval = 'A l\'instant';
      } else {
        if (now.getTime() - commentPublicationDate.getTime() < 3600000) {
          this.comment.interval = Math.floor((now.getTime() - commentPublicationDate.getTime()) / 60000) + ' min';
        } else {
          if (now.getTime() - commentPublicationDate.getTime() < 86400000) {
            this.comment.interval = Math.floor((now.getTime() - commentPublicationDate.getTime()) / 3600000) + ' h';
          } else {
            this.comment.interval = commentPublicationDate.toLocaleDateString('fr-FR');
          }
        }
      }
    }
  }

}
