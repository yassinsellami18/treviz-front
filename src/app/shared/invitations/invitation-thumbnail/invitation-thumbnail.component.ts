import {Component, EventEmitter, Input, Output} from '@angular/core';
import {ProjectInvitation} from '../../models/project/project-invitation.model';
import {ProjectInvitationService} from '../../../core/services/project/project-invitation.service';
import {CommunityInvitation} from '../../models/community/community-invitation.model';
import {CommunityInvitationService} from '../../../core/services/community/community-invitation.service';


@Component({
  selector: 'app-invitation-thumbnail',
  templateUrl: 'invitation-thumbnail.component.html',
  styleUrls: ['invitation-thumbnail.component.scss']
})
export class InvitationThumbnailComponent {

  @Input() projectInvitation: ProjectInvitation;

  @Input() communityInvitation: CommunityInvitation;

  @Input() canApprove = false;

  @Input() canDiscard = false;

  @Output() onProjectInvitationRemoval = new EventEmitter<ProjectInvitation>();

  @Output() onCommunityInvitationRemoval = new EventEmitter<CommunityInvitation>();

  private alive = false;

  constructor(private projectInvitationService: ProjectInvitationService,
              private communityInvitationService: CommunityInvitationService) {
  }

  /*
   * Invitation management
   */
  acceptInvitation(): void {
    if (this.projectInvitation) {
      this.projectInvitationService.acceptProjectInvitation(this.projectInvitation.hash)
        .takeWhile(() => this.alive)
        .subscribe(
          result => {
            this.onProjectInvitationRemoval.emit(this.projectInvitation);
          },
          err => console.log(err)
        );
    } else if (this.communityInvitation) {
      this.communityInvitationService.acceptCommunityInvitation(this.communityInvitation.hash)
        .takeWhile(() => this.alive)
        .subscribe(
          result => {
            this.onCommunityInvitationRemoval.emit(this.communityInvitation);
          },
          err => console.log(err)
        );
    }

  }

  removeInvitation(): void {
    if (this.projectInvitation) {
      this.projectInvitationService.deleteProjectInvitation(this.projectInvitation.hash)
        .takeWhile(() => this.alive)
        .subscribe(
          result => {
            this.onProjectInvitationRemoval.emit(this.projectInvitation);
          },
          err => console.log(err)
        );
    } else if (this.communityInvitation) {
      this.communityInvitationService.deleteCommunityInvitation(this.communityInvitation.hash)
        .takeWhile(() => this.alive)
        .subscribe(
          result => {
            this.onCommunityInvitationRemoval.emit(this.communityInvitation);
          },
          err => console.log(err)
        );
    }

  }

}
