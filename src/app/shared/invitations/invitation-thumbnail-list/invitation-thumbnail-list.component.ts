import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ProjectInvitation} from '../../models/project/project-invitation.model';
import {ProjectInvitationService} from '../../../core/services/project/project-invitation.service';
import {ProjectJob} from '../../models/project/project-job.model';
import {User} from '../../models/users/user.model';
import {Project} from '../../models/project/project.model';
import {CommunityInvitation} from '../../models/community/community-invitation.model';
import {CommunityInvitationService} from '../../../core/services/community/community-invitation.service';
import {Community} from '../../models/community/community.model';



@Component({
  selector: 'app-invitation-thumbnail-list',
  templateUrl: 'invitation-thumbnail-list.component.html',
  styleUrls: ['invitation-thumbnail-list.component.scss']
})
export class InvitationThumbnailListComponent implements OnInit, OnDestroy {

  @Input() job: ProjectJob;

  @Input() project: Project;

  @Input() user: User;

  @Input() community: Community;

  @Input() canApprove = false;

  @Input() canDiscard = false;

  @Input() displayNoneMessage = true;

  projectInvitations: ProjectInvitation[] = [];
  projectInvitationsFetched = false;
  communityInvitations: CommunityInvitation[] = [];
  communityInvitationsFetched = false;

  private alive = true;

  constructor(private projectInvitationService: ProjectInvitationService,
              private communityInvitationService: CommunityInvitationService) { }

  ngOnInit() {

    const projectOptions = {
      job: '',
      project: '',
      user: ''
    };

    const communityOptions = {
      community: '',
      user: ''
    };

    if (this.job) {
      projectOptions.job = this.job.hash;
    }
    if (this.project) {
      projectOptions.project = this.project.hash;
    }
    if (this.community) {
      communityOptions.community = this.community.hash;
    }
    if (this.user) {
      projectOptions.user = this.user.username;
      communityOptions.user = this.user.username;
    }

    this.projectInvitationService.getProjectInvitations(projectOptions)
      .takeWhile(() => this.alive)
      .finally(() => this.projectInvitationsFetched = true)
      .subscribe(
        data => this.projectInvitations = data,
        err => console.log(err)
      );

    this.communityInvitationService.getCommunityInvitations(communityOptions)
      .takeWhile(() => this.alive)
      .finally(() => this.communityInvitationsFetched = true)
      .subscribe(
        data => this.communityInvitations = data,
        err => console.log(err)
      );
  }

  ngOnDestroy() {
    this.alive = false;
  }

  manageProjectInvitation(invitation: ProjectInvitation) {
    this.projectInvitations.splice(this.projectInvitations.findIndex(indexedInvitation => indexedInvitation.hash === invitation.hash), 1);
  }

  manageCommunityInvitation(invitation: CommunityInvitation) {
    this.communityInvitations.splice(this.communityInvitations.findIndex(indexedInvitation => indexedInvitation.hash === invitation.hash), 1);
  }

}
