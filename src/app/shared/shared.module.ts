/* Basics Angular Modules*/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

/* Material Design Modules */
import {
  MatAutocompleteModule,
  MatButtonModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatFormFieldModule,
  MatStepperModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSlideToggleModule,
  MatTableModule,
  MatTabsModule,
  MatTooltipModule,
  MatSnackBarModule
} from '@angular/material';
import 'hammerjs';

/* Module for markdown support */
import {MarkdownModule} from 'angular2-markdown';

/* Module for drag and drop */
import { DragulaModule, DragulaService } from 'ng2-dragula';

/* Shared components of the application */
import { ProjectThumbnailComponent } from './project/project-thumbnail/project-thumbnail.component';
import { CommunityThumbnailComponent } from './community-thumbnail/community-thumbnail.component';
import { UserThumbnailComponent } from './user-thumbnail/user-thumbnail.component';
import { UserAvatarCircleComponent } from './user-avatar-circle/user-avatar-circle.component';
import { TagComponent } from './tag/tag.component';
import { DocumentListComponent } from './documents/document-list/document-list.component';
import { DocumentUploadComponent } from './documents/document-upload/document-upload.component';
import { PostComponent } from './newsfeed/post/post.component';
import { PostListComponent } from './newsfeed/post-list/post-list.component';
import { CommentComponent } from './newsfeed/comment/comment.component';
import { ProjectThumbnailListComponent } from './project/project-thumbnail-list/project-thumbnail-list.component';
import { CandidacyDialogComponent } from './candidacy/candidacy-dialog/candidacy-dialog.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

import { BrainstormingComponent } from './brainstorming/brainstorming.component';
import { BrainstormingSessionsListComponent } from './brainstorming/brainstorming-sessions-list/brainstorming-sessions-list.component';
import { BrainstormingSessionCardComponent } from './brainstorming/brainstorming-session-card/brainstorming-session-card.component';
import { BrainstormingSessionDetailComponent } from './brainstorming/brainstorming-session-detail/brainstorming-session-detail.component';
import { BrainstormingIdeaDetailComponent } from './brainstorming/brainstorming-idea-detail/brainstorming-idea-detail.component';
import { BrainstormingIdeaCardComponent } from './brainstorming/brainstorming-idea-card/brainstorming-idea-card.component';
import { BrainstormingEnhancementListComponent } from './brainstorming/brainstorming-enhancement-list/brainstorming-enhancement-list.component';
import { BrainstormingEnhancementCardComponent } from './brainstorming/brainstorming-enhancement-card/brainstorming-enhancement-card.component';
import { BrainstormingIdeaListComponent } from './brainstorming/brainstorming-idea-list/brainstorming-idea-list.component';
import { BrainstormingSessionCreateDialogComponent } from './brainstorming/brainstorming-session-create-dialog/brainstorming-session-create-dialog.component';
import { DocumentPanelComponent } from './documents/document-panel/document-panel.component';
import { DocumentService } from './documents/document.service';
import { BoardComponent } from './kanban/board/board.component';
import { BoardListComponent } from './kanban/board-list/board-list.component';
import { ColumnComponent } from './kanban/column/column.component';
import { TaskComponent } from './kanban/task/task.component';
import { TaskDetailDialogComponent } from './kanban/task-detail-dialog/task-detail-dialog.component';
import { BoardCreateDialogComponent } from './kanban/board-create-dialog/board-create-dialog.component';
import { BoardEditDialogComponent } from './kanban/board-edit-dialog/board-edit-dialog.component';
import { JobThumbnailComponent } from './job/job-thumbnail/job-thumbnail.component';
import { JobCreateDialogComponent } from './job/job-create-dialog/job-create-dialog.component';
import { JobThumbnailListComponent } from './job/job-thumbnail-list/job-thumbnail-list.component';
import { CandidacyThumbnailComponent } from './candidacy/candidacy-thumbnail/candidacy-thumbnail.component';
import { CandidacyThumbnailListComponent } from './candidacy/candidacy-thumbnail-list/candidacy-thumbnail-list.component';
import { CommunityCandidacyListComponent } from './candidacy/community-candidacy-list/community-candidacy-list.component';
import { ProjectCandidacyListComponent } from './candidacy/project-candidacy-list/project-candidacy-list.component';
import { FeedbackComponent } from './kanban/feedback/feedback.component';
import { InvitationThumbnailComponent } from './invitations/invitation-thumbnail/invitation-thumbnail.component';
import { InvitationThumbnailListComponent } from './invitations/invitation-thumbnail-list/invitation-thumbnail-list.component';
import { FeedbackListComponent } from './kanban/feedback-list/feedback-list.component';

/*
 * Pipes
 */
import { DateIntervalPipe } from './pipes/date-interval.pipe';
import { TruncatePipe } from './pipes/truncate.pipe';
import { SuccessSnackBarComponent } from './snackbars/success-snack-bar/success-snack-bar.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,

    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatStepperModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule,

    MarkdownModule.forRoot(),

    DragulaModule,
  ],
  declarations: [
    /*
     * Brainstorming component
     */
    BrainstormingComponent,
    BrainstormingSessionCreateDialogComponent,
    BrainstormingSessionsListComponent,
    BrainstormingSessionCardComponent,
    BrainstormingSessionDetailComponent,
    BrainstormingIdeaListComponent,
    BrainstormingIdeaCardComponent,
    BrainstormingIdeaDetailComponent,
    BrainstormingEnhancementListComponent,
    BrainstormingEnhancementCardComponent,
    /*
     * New feed components (posts, comments...)
     */
    PostComponent,
    PostListComponent,
    CommentComponent,
    /*
     * Documents components
     */
    DocumentListComponent,
    DocumentPanelComponent,
    DocumentUploadComponent,
    /*
     * Job Components
     */
    JobCreateDialogComponent,
    JobThumbnailComponent,
    JobThumbnailListComponent,
    /*
     * Job thumbnail
     */
    ProjectThumbnailComponent,
    ProjectThumbnailListComponent,
    /*
     * Community thumbnail
     */
    CommunityThumbnailComponent,
    /*
     * Kanban components
     */
    BoardComponent,
    BoardListComponent,
    BoardCreateDialogComponent,
    BoardEditDialogComponent,
    ColumnComponent,
    FeedbackComponent,
    FeedbackListComponent,
    TaskComponent,
    TaskDetailDialogComponent,
    /*
     * General purpose components
     */
    CandidacyThumbnailComponent,
    CandidacyThumbnailListComponent,
    CommunityCandidacyListComponent,
    ProjectCandidacyListComponent,
    CandidacyDialogComponent,
    InvitationThumbnailComponent,
    InvitationThumbnailListComponent,
    ConfirmDialogComponent,
    TagComponent,
    UserThumbnailComponent,
    UserAvatarCircleComponent,

    /*Snackbars*/
    SuccessSnackBarComponent,

    /*
     * Pipes
     */
    TruncatePipe,
    DateIntervalPipe,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,

    /* Export material design modules for further use */
    MatAutocompleteModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatStepperModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatTableModule,
    MatTabsModule,
    MatTooltipModule,
    MatProgressBarModule,

    /* Exports Markdown Module*/
    MarkdownModule,

    DragulaModule,

    BrainstormingComponent,
    CommunityThumbnailComponent,
    CommentComponent,
    ConfirmDialogComponent,
    CandidacyDialogComponent,
    CandidacyThumbnailComponent,
    CandidacyThumbnailListComponent,
    CommunityCandidacyListComponent,
    ProjectCandidacyListComponent,
    DocumentListComponent,
    DocumentPanelComponent,
    DocumentUploadComponent,
    InvitationThumbnailComponent,
    InvitationThumbnailListComponent,
    JobCreateDialogComponent,
    JobThumbnailComponent,
    JobThumbnailListComponent,
    ProjectThumbnailComponent,
    ProjectThumbnailListComponent,
    PostComponent,
    PostListComponent,
    TagComponent,
    UserThumbnailComponent,
    UserAvatarCircleComponent,

    /* Kanban components */
    BoardComponent,
    BoardListComponent,
    BoardCreateDialogComponent,
    BoardEditDialogComponent,
    ColumnComponent,
    FeedbackComponent,
    FeedbackListComponent,
    TaskComponent,
    TaskDetailDialogComponent,

    /*Snackbars*/
    SuccessSnackBarComponent,

    /*Pipes*/
    TruncatePipe,
    DateIntervalPipe
  ],
  providers: [
    DocumentService,
    DragulaService
  ],
  entryComponents: [
    BrainstormingSessionCreateDialogComponent,
    DocumentUploadComponent,
    CandidacyDialogComponent,
    ConfirmDialogComponent,
    BoardCreateDialogComponent,
    BoardEditDialogComponent,
    TaskDetailDialogComponent,
    JobCreateDialogComponent,
    SuccessSnackBarComponent,
  ]
})
export class SharedModule { }
