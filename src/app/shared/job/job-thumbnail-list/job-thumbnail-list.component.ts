import { Component, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { ProjectJob } from '../../models/project/project-job.model';
import { ProjectJobService } from '../../../core/services/project/project-job.service';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { Project } from '../../models/project/project.model';
import { JobCreateDialogComponent } from '../job-create-dialog/job-create-dialog.component';
import { User } from '../../models/users/user.model';


@Component({
  selector: 'app-job-thumbnail-list',
  templateUrl: 'job-thumbnail-list.component.html',
  styleUrls: ['job-thumbnail-list.component.scss']
})
export class JobThumbnailListComponent implements OnChanges, OnDestroy {

  @Input() config = {
    holder: '',
    candidate: '',
    tags: [],
    skills: [],
    project: '',
    min_reward: 0,
    max_reward: 0,
    task: '',
    attributed: false,
    name: '',
    nb: 20,
    offset: 0};

  @Input() project: Project;

  @Input() user: User;

  @Input() displayCreateButton = false;

  jobs: ProjectJob[] = [];

  jobsFetched = false;

  moreToLoad = true;

  private alive = true;

  constructor(private projectJobService: ProjectJobService,
              public dialog: MatDialog) { }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (propName === 'project') {
        this.config.project = this.project.hash;
      } else if (propName === 'user') {
        this.config.holder = this.user.username;
      }
    }
    this.loadJobs();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  loadJobs() {
    this.jobsFetched = false;
    this.config.offset = this.jobs.length;

    /*
     * If the project is defined, use the getProjectJobs method to fetch them.
     * Otherwise, use the generic getJobs method.
     */
    if (this.project) {
      this.projectJobService.getProjectJobs(this.project.hash)
        .takeWhile(() => this.alive)
        .finally(() => this.jobsFetched = true)
        .subscribe(
          jobs => {
            this.jobs = this.jobs.concat(jobs);
            this.moreToLoad = jobs.length === this.config.nb;
          },
          err => console.log(err)
        );
    } else {
      this.projectJobService.getJobs(this.config)
        .takeWhile(() => this.alive)
        .finally(() => this.jobsFetched = true)
        .subscribe(
          jobs => {
            this.jobs = this.jobs.concat(jobs);
            this.moreToLoad = jobs.length === this.config.nb;
          },
          err => console.log(err)
        );
    }


  }

  openNewJobDialog() {
    const config = new MatDialogConfig();
    config.data = this.project;

    const dialogRef = this.dialog.open(JobCreateDialogComponent, config);

    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(result => {
        if (typeof result !== 'undefined' && result !== null && result !== false) {
          this.jobs.push(result);
        }
      });
  }

}
