import {Component, Input} from '@angular/core';
import {ProjectJob} from '../../models/project/project-job.model';

@Component({
  selector: 'app-job-thumbnail',
  templateUrl: 'job-thumbnail.component.html',
  styleUrls: ['job-thumbnail.component.scss']
})
export class JobThumbnailComponent {

  @Input() job: ProjectJob;

 constructor() { }

}
