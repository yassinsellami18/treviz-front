import {Component, Inject, OnDestroy} from '@angular/core';
import {Project} from '../../models/project/project.model';
import {ProjectJobDto} from '../../models/project/project-job.model.dto';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProjectJobService} from '../../../core/services/project/project-job.service';
import {Observable} from 'rxjs';
import {User} from '../../models/users/user.model';
import {Skill} from '../../models/tags/skill.model';
import {Tag} from '../../models/tags/tag.model';
import {FormControl} from '@angular/forms';
import {UserService} from '../../../core/services/user.service';
import {TagService} from '../../../core/services/tag.service';
import {SkillService} from '../../../core/services/skill.service';






@Component({
  selector: 'app-job-create-dialog',
  templateUrl: 'job-create-dialog.component.html',
  styleUrls: ['job-create-dialog.component.scss']
})
export class JobCreateDialogComponent implements OnDestroy {

  project: Project;

  jobDto: ProjectJobDto = new ProjectJobDto();

  jobSubmitted = false;

  skillCtrl: FormControl;
  filteredSkills: Observable<Skill[]>;
  skills: Skill[] = [];

  tagCtrl: FormControl;
  filteredTags: Observable<Tag[]>;
  tags: Tag[] = [];

  holder: User;
  filteredHolder: User[];
  holderCtrl: FormControl;

  contact: User;
  filteredContact: User[];
  contactCtrl: FormControl;

  submitted = false;

  private alive = true;

  constructor(private dialogRef: MatDialogRef<JobCreateDialogComponent>,
              private projectJobService: ProjectJobService,
              private userService: UserService,
              private skillService: SkillService,
              private tagService: TagService,
              @Inject(MAT_DIALOG_DATA) public data: Project) {
    this.project = data;
    if (this.project.tags) { this.jobDto.tags = this.project.tags.map(tag => tag.name); }
    if (this.project.skills) { this.jobDto.skills = this.project.skills.map(skill => skill.name); }

    // Search by skill
    this.skillCtrl = new FormControl();
    this.skillService.getSkillsHttp()
      .takeWhile(() => this.alive)
      .subscribe(
        skills => {
          this.skills = skills;
        },
        err => console.log('impossible to fetch skills')
      );
    this.filteredSkills = this.skillCtrl.valueChanges
      .startWith(null)
      .map(name => this.filterSkill(name));

    // Search by tag
    this.tagCtrl = new FormControl();
    this.tagService.getTagsHttp()
      .takeWhile(() => this.alive)
      .subscribe(
        tags => {
          this.tags = tags;
        },
        err => console.log('impossible to fetch tags')
      );
    this.filteredTags = this.tagCtrl.valueChanges
      .startWith(null)
      .map(name => this.filterTag(name));


    this.holderCtrl = new FormControl();
    this.holderCtrl.valueChanges
      .debounceTime(300)
      .distinctUntilChanged()
      .takeWhile(() => this.alive)
      .subscribe(
        name => {
          this.filteredHolder = [];
          if (name != null && name.length >= 2) {
            this.userService.getUsers({name: name})
              .takeWhile(() => this.alive)
              .subscribe(
                users => this.filteredHolder = users,
                err => console.log(err)
              );
          }
        }
      );

    this.contactCtrl = new FormControl();
    this.contactCtrl.valueChanges
      .debounceTime(300)
      .distinctUntilChanged()
      .takeWhile(() => this.alive)
      .subscribe(
        name => {
          console.log(name);
          this.filteredContact = [];
          if (name != null && name.length >= 2) {
            this.userService.getUsers({name: name})
              .takeWhile(() => this.alive)
              .subscribe(
                users => this.filteredContact = users,
                err => console.log(err)
              );
          }
        }
      );

  }

  ngOnDestroy() {
    this.alive = false;
  }


  filterSkill(val: string) {
    return val ? this.skills.filter((skill) => new RegExp(val, 'gi').test(skill.name)) : this.skills;
  }

  addSkill(skill: string) {
    if (skill !== '' && this.jobDto.skills.indexOf(skill) === -1) {
      this.jobDto.skills.push(skill);
    }
  }

  removeSkill(skill: string) {
    this.jobDto.skills.splice(this.jobDto.skills.indexOf(skill), 1);
  }

  filterTag(val: string) {
    return val ? this.tags.filter((tag) => new RegExp(val, 'gi').test(tag.name)) : this.tags;
  }

  addTag(tag: string) {
    if (tag !== '' && this.jobDto.tags.indexOf(tag) === -1) {
      this.jobDto.tags.push(tag);
    }
  }

  removeTag(tag: string) {
    this.jobDto.tags.splice(this.jobDto.tags.indexOf(tag), 1);
  }


  setHolder(username: string): void {
    const holder = this.filteredHolder.find(user => user.username === username);
    if (holder) {
      this.holder = holder;
      this.jobDto.holder = username;
    }
  }

  removeHolder(): void {
    this.holder = null;
    this.jobDto.holder = undefined;
  }

  setContact(username: string): void {
    const contact = this.filteredContact.find(user => user.username === username);
    if (contact) {
      this.contact = contact;
      this.jobDto.contact = username;
    }
  }

  removeContact(): void {
    this.contact = null;
    this.jobDto.contact = undefined;
  }


  onSubmit() {
    this.jobSubmitted = true;
    this.projectJobService.postProjectJob(this.project.hash, this.jobDto)
      .takeWhile(() => this.alive)
      .finally(() => this.jobSubmitted = false)
      .subscribe(
        job => this.dialogRef.close(job),
        err => console.log(err)
      );
  }
}
