import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ProjectCandidacy} from '../../models/project/project-candidacy.model';
import {ProjectCandidacyService} from '../../../core/services/project/project-candidacy.service';
import {ProjectJob} from '../../models/project/project-job.model';
import {User} from '../../models/users/user.model';
import {Project} from '../../models/project/project.model';
import {CommunityCandidacy} from '../../models/community/community-candidacy.model';
import {CommunityCandidacyService} from '../../../core/services/community/community-candidacy.service';
import {Community} from '../../models/community/community.model';



@Component({
  selector: 'app-candidacy-thumbnail-list',
  templateUrl: 'candidacy-thumbnail-list.component.html',
  styleUrls: ['candidacy-thumbnail-list.component.scss']
})
export class CandidacyThumbnailListComponent implements OnInit, OnDestroy {

  @Input() job: ProjectJob;

  @Input() project: Project;

  @Input() user: User;

  @Input() community: Community;

  @Input() canApprove = false;

  @Input() canDiscard = false;

  @Input() displayNoneMessage = true;

  projectCandidacies: ProjectCandidacy[] = [];
  projectCandidaciesFetched = false;
  communityCandidacies: CommunityCandidacy[] = [];
  communityCandidaciesFetched = false;

  private alive = true;

  constructor(private projectCandidacyService: ProjectCandidacyService,
              private communityCandidacyService: CommunityCandidacyService) { }

  ngOnInit() {

    const projectOptions = {
      job: '',
      project: '',
      user: ''
    };

    const communityOptions = {
      community: '',
      user: ''
    };

    if (this.job) {
      projectOptions.job = this.job.hash;
    }
    if (this.project) {
      projectOptions.project = this.project.hash;
    }
    if (this.user) {
      projectOptions.user = this.user.username;
      communityOptions.user = this.user.username;
    }
    if (this.community) {
      communityOptions.community = this.community.hash;
    }

    this.projectCandidacyService.getProjectCandidacies(projectOptions)
      .takeWhile(() => this.alive)
      .finally(() => this.projectCandidaciesFetched = true)
      .subscribe(
        data => this.projectCandidacies = data,
        err => console.log(err)
      );

    this.communityCandidacyService.getCommunityCandidacies(communityOptions)
      .takeWhile(() => this.alive)
      .finally(() => this.communityCandidaciesFetched = true)
      .subscribe(
        data => {
          this.communityCandidaciesFetched = true;
          this.communityCandidacies = data;
          console.log(this.communityCandidaciesFetched);
          console.log(data);
        },
        err => console.log(err)
      );
  }

  ngOnDestroy() {
    this.alive = false;
  }

  manageProjectCandidacy(candidacy: ProjectCandidacy) {
    this.projectCandidacies.splice(this.projectCandidacies.findIndex(indexedCandidacy => indexedCandidacy.hash === candidacy.hash), 1);
  }

  manageCommunityCandidacy(candidacy: CommunityCandidacy) {
    this.communityCandidacies.splice(this.communityCandidacies.findIndex(indexedCandidacy => indexedCandidacy.hash === candidacy.hash), 1);
  }

}
