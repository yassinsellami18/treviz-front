import {Component, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {ProjectCandidacy} from '../../models/project/project-candidacy.model';
import {ProjectCandidacyService} from '../../../core/services/project/project-candidacy.service';
import {CommunityCandidacy} from '../../models/community/community-candidacy.model';
import {CommunityCandidacyService} from '../../../core/services/community/community-candidacy.service';


@Component({
  selector: 'app-candidacy-thumbnail',
  templateUrl: 'candidacy-thumbnail.component.html',
  styleUrls: ['candidacy-thumbnail.component.scss']
})
export class CandidacyThumbnailComponent implements OnDestroy {

  @Input() projectCandidacy: ProjectCandidacy;

  @Input() communityCandidacy: CommunityCandidacy;

  @Input() canApprove = false;

  @Input() canDiscard = false;

  @Output() onProjectCandidacyRemoval = new EventEmitter<ProjectCandidacy>();
  @Output() onProjectCandidacyAccepted = new EventEmitter<ProjectCandidacy>();

  @Output() onCommunityCandidacyRemoval = new EventEmitter<CommunityCandidacy>();
  @Output() onCommunityCandidacyAccepted = new EventEmitter<CommunityCandidacy>();

  private alive = true;

  constructor(private projectCandidacyService: ProjectCandidacyService,
              private communityCandidacyService: CommunityCandidacyService) {
  }

  ngOnDestroy() {
    this.alive = false;
  }

  /*
   * Candidacy management
   */
  acceptCandidacy(): void {
    if (this.projectCandidacy) {
      this.projectCandidacyService.acceptProjectCandidacy(this.projectCandidacy.hash)
        .takeWhile(() => this.alive)
        .subscribe(
          result => {
            this.onProjectCandidacyAccepted.emit(this.projectCandidacy);
          },
          err => console.log(err)
        );
    } else if (this.communityCandidacy) {
      this.communityCandidacyService.acceptCommunityCandidacy(this.communityCandidacy.hash)
        .takeWhile(() => this.alive)
        .subscribe(
          result => {
            this.onCommunityCandidacyAccepted.emit(this.communityCandidacy);
          },
          err => console.log(err)
        );
    }

  }

  removeCandidacy(): void {
    if (this.projectCandidacy) {
      this.projectCandidacyService.deleteProjectCandidacy(this.projectCandidacy.hash)
        .takeWhile(() => this.alive)
        .subscribe(
          result => {
            this.onProjectCandidacyRemoval.emit(this.projectCandidacy);
          },
          err => console.log(err)
        );
    } else if (this.communityCandidacy) {
      this.communityCandidacyService.deleteCommunityCandidacy(this.communityCandidacy.hash)
        .takeWhile(() => this.alive)
        .subscribe(
          result => {
            this.onCommunityCandidacyRemoval.emit(this.communityCandidacy);
          },
          err => console.log(err)
        );
    }
  }

}
