import { Component, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CommunityCandidacyService } from '../../../core/services/community/community-candidacy.service';
import { ProjectCandidacyService } from '../../../core/services/project/project-candidacy.service';
import { ProjectCandidacyDto } from '../../models/project/project-candidacy.model.dto';
import { CommunityCandidacyDto } from '../../models/community/community-candidacy.model.dto';
import { Project } from '../../models/project/project.model';
import { Community } from '../../models/community/community.model';
import { ProjectJob } from '../../models/project/project-job.model';

@Component({
  selector: 'app-candidacy-dialog',
  templateUrl: 'candidacy-dialog.component.html',
  styleUrls: ['candidacy-dialog.component.scss']
})
export class CandidacyDialogComponent implements OnDestroy {

  public message: string;

  private project: Project;

  private job: ProjectJob;

  private community: Community;

  private alive = true;

  public submitted = false;

  constructor(public dialogRef: MatDialogRef<CandidacyDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private projectCandidacyService: ProjectCandidacyService,
              private communityCandidacyService: CommunityCandidacyService) {

    data.project ? this.project = data.project : this.project = null;
    data.job ? this.job = data.job : this.job = null;
    data.community ? this.community = data.community : this.community = null;

  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  onSubmit() {
    this.submitted = true;
    if (this.project !== null) {

      const candidacy = new ProjectCandidacyDto();
      candidacy.message = this.message;

      this.projectCandidacyService.postProjectCandidacy(this.project.hash, candidacy)
        .takeWhile(() => this.alive)
        .subscribe(
          data => this.dialogRef.close(data),
          err => console.log(err),
          () => this.submitted = false
        );
    } else if (this.job !== null) {

        const candidacy = new ProjectCandidacyDto();
        candidacy.message = this.message;
        candidacy.job = this.job.hash;

        this.projectCandidacyService.postProjectCandidacy(this.job.project.hash, candidacy)
          .takeWhile(() => this.alive)
          .subscribe(
            data => this.dialogRef.close(data),
            err => console.log(err),
            () => this.submitted = false
          );
      } else if (this.community !== null) {

      const candidacy = new CommunityCandidacyDto();
      candidacy.message = this.message;

      this.communityCandidacyService.postCommunityCandidacy(this.community.hash, candidacy)
        .takeWhile(() => this.alive)
        .subscribe(
          data => this.dialogRef.close(data),
          err => console.log(err),
          () => this.submitted = false
        );
    }
  }
}
