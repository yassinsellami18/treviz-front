import { Component, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { BoardService } from '../../../core/services/kanban/board.service';
import { Board } from '../../models/kanban/board.model';
import { User } from '../../models/users/user.model';
import { Project } from '../../models/project/project.model';
import { BoardDto } from '../../models/kanban/board.model.dto';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { BoardCreateDialogComponent } from '../board-create-dialog/board-create-dialog.component';
import { ProjectMembership } from '../../models/project/project-membership.model';
import { BoardEditDialogComponent } from '../board-edit-dialog/board-edit-dialog.component';


@Component({
 selector: 'app-board-list',
 templateUrl: 'board-list.component.html',
 styleUrls: ['board-list.component.scss']
})
export class BoardListComponent implements OnChanges, OnDestroy {

  @Input() project: Project;
  @Input() membership: ProjectMembership = null; // used to decide if the user should see the edit buttons.
  @Input() user: User;

  boards: Board[] = [];
  selectedBoard: Board;
  newBoard: BoardDto;
  canEdit = false;
  boardsFetched = false;
  private alive = true;

  constructor(private boardService: BoardService,
              public dialog: MatDialog) {
    this.newBoard = new BoardDto();
  }

  ngOnChanges(changes: SimpleChanges) {
    const options = {
      project: '',
      user: '',
      archived: false
    };

    for (const propName in changes) {
      if (propName === 'project') {
        options.project = this.project.hash;
      } else if (propName === 'user') {
        options.user = this.user.username;
      }
    }

    this.boardService.getBoards(options)
      .takeWhile(() => this.alive)
      .subscribe(
        boards => {
          this.boards = boards;
          if (boards.length > 0) {
            this.selectedBoard = this.boards[0];
          }
          this.boardsFetched = true;
        },
        err => console.log(err)
      );

    if (this.membership) {
      this.canEdit = this.membership.role.permissions.some(permission => permission === 'MANAGE_KANBAN');
    }
  }

  ngOnDestroy() {
    this.alive = false;
  }

  selectBoard(board: Board) {
    this.selectedBoard = board;
  }

  openBoardCreationDialog() {
    const config = new MatDialogConfig();
    this.project !== null ? config.data = this.project.hash : config.data = '';

    const dialogRef = this.dialog.open(BoardCreateDialogComponent, config);
    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(
        board => {
          if (board) {
            this.boards.push(board);
            this.selectedBoard = board;
          }
        },
        err => console.log(err)
      );
  }

  openBoardUpdateDialog() {
    const config = new MatDialogConfig();
    config.data = this.selectedBoard;

    const dialogRef = this.dialog.open(BoardEditDialogComponent, config);
    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(
        updatedBoard => {
          if (updatedBoard) {
            const boardIndex = this.boards.findIndex(indexedBoard => indexedBoard.hash === this.selectedBoard.hash);
            this.selectedBoard = updatedBoard;
            this.boards[boardIndex] = updatedBoard;
          }
        },
        err => console.log(err)
      );
  }

}
