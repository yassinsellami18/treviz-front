import { Component, Input, OnChanges, OnDestroy, SimpleChanges } from '@angular/core';
import { Board } from '../../models/kanban/board.model';
import { ColumnDto } from '../../models/kanban/column.model.dto';
import { ColumnService } from '../../../core/services/kanban/column.service';
import { Column } from '../../models/kanban/column.model';
import { ProjectMembership } from "../../models/project/project-membership.model";


@Component({
  selector: 'app-board',
  templateUrl: 'board.component.html',
  styleUrls: ['board.component.scss']
})
export class BoardComponent implements OnChanges, OnDestroy {

  @Input() board: Board;
  @Input() membership: ProjectMembership; // used to decide if the user should see the edit buttons.

  canEdit = false;
  columnsFetched = false;
  private alive = true;
  columns: Column[] = [];
  columnDto: ColumnDto;
  columnSubmitted = false;

  constructor(private columnService: ColumnService) {
    this.columnDto = new ColumnDto();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.columns = [];
    this.columnsFetched = false;
    this.columnService.getColumns(this.board.hash)
      .finally(() => this.columnsFetched = true)
      .takeWhile(() => this.alive)
      .subscribe(
        columns => this.columns = columns,
        err => console.log(err)
      );

    if (this.membership) {
      this.canEdit = this.membership.role.permissions.some(permission => permission === 'MANAGE_KANBAN');
    }
  }

  ngOnDestroy() {
    this.alive = false;
  }

  onSubmit() {
    this.columnSubmitted = true;
    this.columnService.postColumn(this.board.hash, this.columnDto)
      .takeWhile(() => this.alive)
      .finally(() => {this.columnDto = new ColumnDto(); this.columnSubmitted = false; } )
      .subscribe(
        column => this.columns.push(column),
        err => console.log(err)
      );
  }

}
