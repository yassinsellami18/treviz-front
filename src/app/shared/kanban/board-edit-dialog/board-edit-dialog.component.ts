import { Component, Inject, OnDestroy } from '@angular/core';
import { BoardService } from '../../../core/services/kanban/board.service';
import { BoardDto } from '../../models/kanban/board.model.dto';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Board } from '../../models/kanban/board.model';
import { LabelDto } from '../../models/kanban/label.model.dto';
import { Label } from '../../models/kanban/label.model';
import { LabelService } from '../../../core/services/kanban/label.service';



@Component({
  selector: 'app-board-edit-dialog',
  templateUrl: 'board-edit-dialog.component.html',
  styleUrls: ['board-edit-dialog.component.scss']
})
export class BoardEditDialogComponent implements OnDestroy {

  board: Board;
  boardDto: BoardDto;
  labelDto: LabelDto; // Object used to update an existing label.
  newLabel: LabelDto; // Object used to create a new label.

  labels: Label[] = [];

  boardSubmitted = false;
  labelSubmitted = false;
  labelToUpdate: Label;

  availableLabelColors = [
    {name: 'red', color: '#D32F2F'},
    {name: 'pink', color: '#C2185B'},
    {name: 'purple', color: '#7B1FA2'},
    {name: 'blue', color: '#1976D2'},
    {name: 'indigo', color: '#303F9F'},
    {name: 'teal', color: '#00796B'},
    {name: 'green', color: '#388E3C'},
  ];

  private alive = true;

  constructor(private dialogRef: MatDialogRef<BoardEditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Board,
              private boardService: BoardService,
              private labelService: LabelService) {
    this.board = data;
    this.newLabel = new LabelDto();
    this.boardDto = new BoardDto();
    this.boardDto.archived = data.archived;
    this.boardDto.name = data.name;
    this.boardDto.description = data.description;
    this.labels = data.labels;
  }

  ngOnDestroy() {
    this.alive = false;
  }

  createLabel(): void {
    this.labelSubmitted = true;
    this.labelService.postLabel(this.board.hash, this.newLabel)
      .takeWhile(() => this.alive)
      .finally(() => this.labelSubmitted = false)
      .subscribe(
        label => this.board.labels.push(label),
        err => console.log(err)
      );
  }

  triggerEditLabel(label: Label): void {
    if (!this.labelToUpdate) {
      this.labelToUpdate = label;
      this.labelDto = new LabelDto();
      this.labelDto.name = label.name;
      this.labelDto.color = label.color;
    } else {
      this.labelToUpdate = null;
    }
  }

  editLabel(): void {
    this.labelSubmitted = true;
    this.labelService.putLabel(this.labelToUpdate.hash, this.labelDto)
      .takeWhile(() => this.alive)
      .finally(() => this.labelSubmitted = false)
      .subscribe(
        updatedLabel => {
          const index = this.board.labels.findIndex(label => label.hash === updatedLabel.hash);
          this.board.labels[index] = updatedLabel;
          this.labelToUpdate = null;
          this.labelDto = new LabelDto();
        },
        err => console.log(err)
      );
  }

  deleteLabel(label: Label): void {
    this.labelService.deleteLabel(label.hash)
      .takeWhile(() => this.alive)
      .subscribe(
        data => this.board.labels.splice(this.board.labels.indexOf(label), 1),
        err => console.log(err)
      );
  }

  updateBoard(): void {
    this.boardSubmitted = true;

    this.boardService.postBoard(this.boardDto)
      .finally(() => this.boardSubmitted = false)
      .takeWhile(() => this.alive)
      .subscribe(
        board => this.dialogRef.close(board),
        err => console.log(err)
      );
  }

  close(): void {
    this.dialogRef.close(this.board);
  }
}
