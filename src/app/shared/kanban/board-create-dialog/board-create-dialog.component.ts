import { Component, Inject, OnDestroy } from '@angular/core';
import { BoardService } from '../../../core/services/kanban/board.service';
import { BoardDto } from '../../models/kanban/board.model.dto';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Board } from '../../models/kanban/board.model';



@Component({
  selector: 'app-board-create-dialog',
  templateUrl: 'board-create-dialog.component.html',
  styleUrls: ['board-create-dialog.component.scss']
})
export class BoardCreateDialogComponent implements OnDestroy {

  boardDto: BoardDto;
  boardSubmitted = false;

  private alive = true;

  constructor(public dialogRef: MatDialogRef<BoardCreateDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: string,
              private boardService: BoardService) {
    this.boardDto = new BoardDto();
    this.boardDto.project = data;
    this.boardDto.archived = false;
    console.log(this.boardDto);
  }

  ngOnDestroy() {
    this.alive = false;
  }

  onSubmit() {
    this.boardSubmitted = true;

    this.boardService.postBoard(this.boardDto)
      .finally(() => this.boardSubmitted = false)
      .takeWhile(() => this.alive)
      .subscribe(
        board => this.dialogRef.close(board),
        err => console.log(err)
      );
  }
}
