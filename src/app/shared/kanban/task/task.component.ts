import {Component, Input, Output, EventEmitter} from '@angular/core';
import { Task } from '../../models/kanban/task.model';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { TaskDetailDialogComponent } from '../task-detail-dialog/task-detail-dialog.component';
import { Label } from '../../models/kanban/label.model';


import { InternalEvent, InternalEventType } from '../../models/internal-event.model';

@Component({
  selector: 'app-task',
  templateUrl: 'task.component.html',
  styleUrls: ['task.component.scss']
})
export class TaskComponent {

  @Input() task: Task;

  @Input() availableLabels: Label[];

  @Output() onTaskUpdate = new EventEmitter<Task>();

  @Output() onTaskDelete = new EventEmitter<Task>();

  constructor(public dialog: MatDialog) { }

  openEditDialog() {
    const config = new MatDialogConfig();

    try {
      config.data = {
        task: this.task,
        labels: this.task.column.board.labels
      };
    } catch (e) {
      config.data = {
        task: this.task,
        labels: this.availableLabels
      };
    }

    const dialogRef = this.dialog.open(TaskDetailDialogComponent, config);

    let alive = true;
    dialogRef.afterClosed()
      .takeWhile(() => alive)
      .finally(() => alive = false)
      .subscribe(
        (event: InternalEvent) => {
          if (event != null) {
            switch (event.type) {
              case InternalEventType.DELETE:
                this.onTaskDelete.emit(event.data);
                break;
              case InternalEventType.UPDATE:
              default:
                this.task = event.data;
                this.onTaskUpdate.emit(event.data);
                break;
            };
          }
        }
      );
  }
}
