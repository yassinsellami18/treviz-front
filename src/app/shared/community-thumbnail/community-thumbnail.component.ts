import { Component, Input } from '@angular/core';
import { Community } from '../models/community/community.model';

@Component({
    moduleId: module.id,
    selector: 'app-community-thumbnail',
    templateUrl: 'community-thumbnail.component.html',
    styleUrls: ['community-thumbnail.component.scss']
})
export class CommunityThumbnailComponent {

  @Input() community: Community;

  constructor() {}

}
