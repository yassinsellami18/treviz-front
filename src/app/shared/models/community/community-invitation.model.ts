import { User } from '../users/user.model';
import { Community } from './community.model';

/**
 * Created by Bastien on 16/03/2017.
 */

export class CommunityInvitation {
  hash: string;
  community: Community;
  user: User;
  message: string;
}
