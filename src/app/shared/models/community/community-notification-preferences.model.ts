export class CommunityNotificationPreferences {
    public onPost: Boolean;
    public onCandidacy: Boolean;
    public onNewBrainstorming: Boolean;
    public onNewProject: Boolean;
    public onNewDocument: Boolean;
}
