/**
 * Created by huber on 26/08/2017.
 */
import {Community} from '../community/community.model';

export class BrainstormingSession {
  hash: string;
  name: string;
  open: boolean;
  description: string;
  startDate: Date;
  nbIdeas: number;
  community: Community;
}
