/**
 * Created by huber on 26/08/2017.
 */
export class BrainstormingSessionDto {
  name: string;
  open: boolean|string;
  description: string;
  project: string; // Hash of the project to which create the session.
  community: string; // Hash of the community to which create the session.
}
