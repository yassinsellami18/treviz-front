/**
 * Created by Bastien on 04/03/2017.
 */

export class Tag {
  id: number;
  name: string;
}
