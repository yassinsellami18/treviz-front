/**
 * Default interface for internal event that can be emmited.
 */
export interface InternalEvent {
    type: InternalEventType;
    data: any;
}

/**
 * Defines the various type of events that can be triggered.
 */
export enum InternalEventType {
    CREATE = 'CREATE',
    UPDATE = 'UPDATE',
    DELETE = 'DELETE'
}
