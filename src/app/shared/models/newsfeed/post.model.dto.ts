export class PostDto {
  message: string;
  link: string;
  project: string;
  community: string;
  task: string;
  job: string;
  document: string;
}
