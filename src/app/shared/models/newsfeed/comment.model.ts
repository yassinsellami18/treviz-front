import {User} from '../users/user.model';

export class Comment {
  message: string;
  author: User;
  publicationDate: Date;
  interval: string;
}
