export class UserPreferences {
    public lightTheme: Boolean;
    public onCandidacyChange: Boolean;
    public onDirectMessage: Boolean;
    public onChatRoomMessage: Boolean;
    public onInvitation: Boolean;
}
