export class UserDto {
  username: string;
  firstName: string;
  lastName: string;
  description: string;
  address: string;
  email: string;
  skills: string[];
  interests: string[];
  avatarUrl: string;
  backgroundImageUrl: string;
  // These attributes are just used in the front-end.


  constructor() {
    this.skills = [];
    this.interests = [];
  }

}
