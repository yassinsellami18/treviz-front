export class DocumentDto {
  name: string;
  description: string;
  file: File;
  community: string;
  project: string;
  room: string;
  idea: string;
}
