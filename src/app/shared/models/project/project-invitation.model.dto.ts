/**
 * Created by Bastien on 16/03/2017.
 */

export class ProjectInvitationDto {
  user: string;
  message: string;
}
