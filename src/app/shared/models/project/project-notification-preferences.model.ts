export class ProjectNotificationPreferences {
    public onNewTask: Boolean;
    public onNewDocument: Boolean;
    public onAssignedTask: Boolean;
    public onCandidacy: Boolean;
    public onFork: Boolean;
    public onPost: Boolean;
    public onTaskApprovalOrRefusal: Boolean;
    public onTaskSubmission: Boolean;
}
