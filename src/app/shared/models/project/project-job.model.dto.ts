export class ProjectJobDto {
  name: string;
  description: string;
  monthlyReward: number;
  tasks: string[];
  tags: string[];
  skills: string[];
  holder: string;
  contact: string;
}
