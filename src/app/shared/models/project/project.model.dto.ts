export class ProjectDto {
  name: string;
  description: string;
  shortDescription: string;
  isPublic: boolean|string;
  isOpen: boolean|string;
  logo: File;
  logoUrl: string;
  skills: string[];
  tags: string[];
  communities: string[];
  idea: string;

  constructor() {
    this.skills = [];
    this.tags = [];
    this.communities = [];
  }
}
