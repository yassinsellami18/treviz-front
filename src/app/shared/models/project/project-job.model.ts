import {Project} from './project.model';
import {User} from '../users/user.model';
import {Tag} from '../tags/tag.model';
import {Skill} from "../tags/skill.model";
import {ProjectCandidacy} from './project-candidacy.model';
import {Task} from '../kanban/task.model';

export class ProjectJob {
  hash: string;
  name: string;
  description: string;
  monthlyReward: number;
  project: Project;
  holder: User;
  contact: User;
  tasks: Task[];
  tags: Tag[];
  skills: Skill[];
  applications: ProjectCandidacy[];

  constructor() {
    this.tags = [];
    this.applications = [];
  }
}
