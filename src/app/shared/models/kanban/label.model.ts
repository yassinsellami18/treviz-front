export class Label {
  hash: string;
  name: string;
  color: string;
}
