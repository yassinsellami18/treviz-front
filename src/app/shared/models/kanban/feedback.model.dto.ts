export class FeedbackDto {
  feedback: string;
  praise: boolean;
}
