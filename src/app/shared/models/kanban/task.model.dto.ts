export class TaskDto {
  name: string;
  description: string;
  deadline: Date|string;
  archived: boolean;
  position: number;
  labels: string[];
  supervisor: string;
  assignee: string;
  reward: number;
  job: string;
  column: string;
  pendingApproval: boolean;

  constructor() {
    this.labels = [];
  }

}
