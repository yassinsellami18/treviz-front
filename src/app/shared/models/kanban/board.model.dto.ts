export class BoardDto {
  name: string;
  description: string;
  archived: boolean;
  project: string;
}
