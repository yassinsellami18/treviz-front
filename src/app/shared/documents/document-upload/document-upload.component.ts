import {Component, Input} from '@angular/core';
import {DocumentDto} from '../../models/document/document.model.dto';
import {DocumentService} from '../document.service';
import {MatDialogRef} from '@angular/material';
import {Project} from '../../models/project/project.model';
import {BrainstormingIdea} from '../../models/brainstorming/brainstorming-idea.model';
import {Community} from '../../models/community/community.model';
import {Room} from '../../models/chat/room.model';



@Component({
  selector: 'app-document-upload',
  templateUrl: 'document-upload.component.html',
  styleUrls: ['document-upload.component.css']
})
export class DocumentUploadComponent {

  @Input() project: Project;
  @Input() community: Community;
  @Input() idea: BrainstormingIdea;
  @Input() room: Room;

  public documentDto: DocumentDto = new DocumentDto();

  public submitted = false;

  public fileLoaded = false;

  private alive = true;

  constructor(public dialogRef: MatDialogRef<DocumentUploadComponent>,
              private documentService: DocumentService) { }

  addFile(event) {
    console.log('loading file');
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.documentDto.file = fileList[0];
      this.fileLoaded = true;
    }
  }

  onSubmit() {
    this.submitted = true;
    this.documentService.postDocument(this.documentDto)
      .takeWhile(() => this.alive)
      .finally(() => this.alive = false)
      .subscribe(
        data => this.dialogRef.close(data),
        err => console.log(err),
        () => this.submitted = false
      );
  }

}
