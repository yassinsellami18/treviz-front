import {Component, Input, OnDestroy} from '@angular/core';
import {Document} from '../../models/document/document.model';

@Component({
  selector: 'app-document-panel',
  templateUrl: 'document-panel.component.html',
  styleUrls: ['document-panel.component.css']
})
export class DocumentPanelComponent implements OnDestroy {

  @Input()
  public document: Document;

  private alive = true;

  private fileTypes = [
    { extension: 'ai', file: '../../../assets/images/filetypes/ai.svg'},
    { extension: 'avi', file: '../../../assets/images/filetypes/avi.svg'},
    { extension: 'css', file: '../../../assets/images/filetypes/vss.svg'},
    { extension: 'csv', file: '../../../assets/images/filetypes/csv.svg'},
    { extension: 'dbf', file: '../../../assets/images/filetypes/dbf.svg'},
    { extension: 'doc', file: '../../../assets/images/filetypes/doc.svg'},
    { extension: 'docx', file: '../../../assets/images/filetypes/doc.svg'},
    { extension: 'dwg', file: '../../../assets/images/filetypes/dwg.svg'},
    { extension: 'exe', file: '../../../assets/images/filetypes/exe.svg'},
    { extension: 'file', file: '../../../assets/images/filetypes/file.svg'},
    { extension: 'fla', file: '../../../assets/images/filetypes/fla.svg'},
    { extension: 'html', file: '../../../assets/images/filetypes/html.svg'},
    { extension: 'iso', file: '../../../assets/images/filetypes/iso.svg'},
    { extension: 'js', file: '../../../assets/images/filetypes/javascript.svg'},
    { extension: 'jpeg', file: '../../../assets/images/filetypes/jpg.svg'},
    { extension: 'jpg', file: '../../../assets/images/filetypes/jpg.svg'},
    { extension: 'json', file: '../../../assets/images/filetypes/json-file.svg'},
    { extension: 'mp3', file: '../../../assets/images/filetypes/pm3.svg'},
    { extension: 'mp4', file: '../../../assets/images/filetypes/mp4.svg'},
    { extension: 'odt', file: '../../../assets/images/filetypes/doc.svg'},
    { extension: 'pdf', file: '../../../assets/images/filetypes/pdf.svg'},
    { extension: 'png', file: '../../../assets/images/filetypes/png.svg'},
    { extension: 'ppt', file: '../../../assets/images/filetypes/ppt.svg'},
    { extension: 'pptx', file: '../../../assets/images/filetypes/ppt.svg'},
    { extension: 'psd', file: '../../../assets/images/filetypes/psd.svg'},
    { extension: 'rtf', file: '../../../assets/images/filetypes/rtf.svg'},
    { extension: 'svg', file: '../../../assets/images/filetypes/svg.svg'},
    { extension: 'txt', file: '../../../assets/images/filetypes/txt.svg'},
    { extension: 'xls', file: '../../../assets/images/filetypes/xls.svg'},
    { extension: 'xlsx', file: '../../../assets/images/filetypes/xlsx.svg'},
    { extension: 'xml', file: '../../../assets/images/filetypes/xml.svg'},
    { extension: 'zip', file: '../../../assets/images/filetypes/zip.svg'}
    ];

  constructor() {}

  ngOnDestroy() {
    this.alive = false;
  }


  getSizeText(document: Document) {
    if (document.size < 1000) {
      return document.size + 'octets';
    } else if (document.size < 1000000) {
      return Math.ceil(document.size / 100 ) / 10 + 'ko';
    } else if (document.size < 1000000000) {
      return Math.ceil(document.size / 100000) / 10 + 'Mo';
    }
  }

  getFileTypeIcon(document: Document) {

    const element = this.fileTypes.filter(fileType => fileType.extension === document.extension);
    if (element.length === 1) {
      return element[0].file;
    }
    return '../../../assets/images/filetypes/file.svg';

  }

}
