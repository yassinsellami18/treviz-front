import {Component, Input, OnChanges, OnDestroy, SimpleChanges} from '@angular/core';
import {MatDialog} from '@angular/material';
import {DocumentUploadComponent} from '../document-upload/document-upload.component';
import {Community} from '../../models/community/community.model';
import {Project} from '../../models/project/project.model';
import {BrainstormingIdea} from '../../models/brainstorming/brainstorming-idea.model';
import {Room} from '../../models/chat/room.model';
import {DocumentService} from '../document.service';
import {Document} from '../../models/document/document.model';

@Component({
  selector: 'app-document-list',
  templateUrl: 'document-list.component.html',
  styleUrls: ['document-list.component.css']
})
export class DocumentListComponent implements OnChanges, OnDestroy {

  @Input()
  public community: Community;

  @Input()
  public project: Project;

  @Input()
  public idea: BrainstormingIdea;

  @Input()
  public room: Room;

  public documents: Document[] = [];

  public loading = true;

  private alive = true;

  private fileTypes = [
    { extension: 'ai', file: '../../../assets/images/filetypes/ai.svg'},
    { extension: 'avi', file: '../../../assets/images/filetypes/avi.svg'},
    { extension: 'css', file: '../../../assets/images/filetypes/vss.svg'},
    { extension: 'csv', file: '../../../assets/images/filetypes/csv.svg'},
    { extension: 'dbf', file: '../../../assets/images/filetypes/dbf.svg'},
    { extension: 'doc', file: '../../../assets/images/filetypes/doc.svg'},
    { extension: 'docx', file: '../../../assets/images/filetypes/doc.svg'},
    { extension: 'dwg', file: '../../../assets/images/filetypes/dwg.svg'},
    { extension: 'exe', file: '../../../assets/images/filetypes/exe.svg'},
    { extension: 'file', file: '../../../assets/images/filetypes/file.svg'},
    { extension: 'fla', file: '../../../assets/images/filetypes/fla.svg'},
    { extension: 'html', file: '../../../assets/images/filetypes/html.svg'},
    { extension: 'iso', file: '../../../assets/images/filetypes/iso.svg'},
    { extension: 'js', file: '../../../assets/images/filetypes/javascript.svg'},
    { extension: 'jpeg', file: '../../../assets/images/filetypes/jpg.svg'},
    { extension: 'jpg', file: '../../../assets/images/filetypes/jpg.svg'},
    { extension: 'json', file: '../../../assets/images/filetypes/json-file.svg'},
    { extension: 'mp3', file: '../../../assets/images/filetypes/pm3.svg'},
    { extension: 'mp4', file: '../../../assets/images/filetypes/mp4.svg'},
    { extension: 'odt', file: '../../../assets/images/filetypes/doc.svg'},
    { extension: 'pdf', file: '../../../assets/images/filetypes/pdf.svg'},
    { extension: 'png', file: '../../../assets/images/filetypes/png.svg'},
    { extension: 'ppt', file: '../../../assets/images/filetypes/ppt.svg'},
    { extension: 'pptx', file: '../../../assets/images/filetypes/ppt.svg'},
    { extension: 'psd', file: '../../../assets/images/filetypes/psd.svg'},
    { extension: 'rtf', file: '../../../assets/images/filetypes/rtf.svg'},
    { extension: 'svg', file: '../../../assets/images/filetypes/svg.svg'},
    { extension: 'txt', file: '../../../assets/images/filetypes/txt.svg'},
    { extension: 'xls', file: '../../../assets/images/filetypes/xls.svg'},
    { extension: 'xlsx', file: '../../../assets/images/filetypes/xlsx.svg'},
    { extension: 'xml', file: '../../../assets/images/filetypes/xml.svg'},
    { extension: 'zip', file: '../../../assets/images/filetypes/zip.svg'}
    ];

  constructor(private documentService: DocumentService,
              public dialog: MatDialog) {}

  ngOnChanges(changes: SimpleChanges) {

    const options = {
      community: '',
      room: '',
      project: '',
      idea: ''
    };

    for (const propName in changes) {
      if (propName === 'community') {
        options.community = this.community.hash;
      } else if (propName === 'room') {
        options.room = this.room.hash;
      } else if (propName === 'project') {
        options.project = this.project.hash;
      } else if (propName === 'idea') {
        options.idea = this.idea.hash;
      }
    }

    this.documentService.getDocuments(options)
      .takeWhile(() => this.alive)
      .subscribe(
        docs => this.documents = docs,
        err => console.log(err),
        () => this.loading = false
      );
  }

  ngOnDestroy() {
    this.alive = false;
  }

  openDocumentUploadDialog() {

    const dialogRef = this.dialog.open(DocumentUploadComponent);
    if (this.community !== undefined) {
      dialogRef.componentInstance.documentDto.community = this.community.hash;
    } else if (this.project !== undefined) {
      dialogRef.componentInstance.documentDto.project = this.project.hash;
    } else if (this.room !== undefined) {
      dialogRef.componentInstance.documentDto.room = this.room.hash;
    } else if (this.idea !== undefined) {
      dialogRef.componentInstance.documentDto.idea = this.idea.hash;
    }

    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(
      data => {
        if (data) {
          this.documents.push(data);
        }
      },
      err => console.log(err)
    );
  }

  getSizeText(document: Document) {
    if (document.size < 1000) {
      return document.size + 'octets';
    } else if (document.size < 1000000) {
      return Math.ceil(document.size / 100 ) / 10 + 'ko';
    } else if (document.size < 1000000000) {
      return Math.ceil(document.size / 100000) / 10 + 'Mo';
    }
  }

  getFileTypeIcon(document: Document) {

    const element = this.fileTypes.filter(x => x.extension === document.extension);
    if (element.length === 1) {
      return element[0].file;
    }
    return '../../../assets/images/filetypes/file.svg';

  }

}
