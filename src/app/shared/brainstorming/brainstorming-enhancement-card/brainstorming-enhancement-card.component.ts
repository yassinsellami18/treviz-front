import {Component, Input} from '@angular/core';
import {BrainstormingEnhancement} from '../../models/brainstorming/brainstorming-enhancement.model';

@Component({
  moduleId: module.id,
  selector: 'app-brainstorming-enhancement-card',
  templateUrl: 'brainstorming-enhancement-card.component.html',
  styleUrls: ['brainstorming-enhancement-card.component.scss']
})

export class BrainstormingEnhancementCardComponent {

  @Input() enhancement: BrainstormingEnhancement;

  constructor() {}

  /*    openCommentDialog(idea: BrainstormingIdea) {
        const dialogRef = this.dialog.open(BrainstormingCommentDialogComponent);
        dialogRef.componentInstance.idea = idea;
      }

      openForkDialog(idea: BrainstormingIdea) {
        const dialogRef = this.dialog.open(BrainstormingForkDialogComponent);
        dialogRef.componentInstance.idea = idea;
      }

      openLikedDialog(idea: BrainstormingIdea) {
        const dialogRef = this.dialog.open(BrainstormingLikedDialogComponent);
        dialogRef.componentInstance.users = idea.liked;
      }

      sendBrainstormingIdea() {
        this.isSubmitted = true;
        this.brainstormingSessionService.postBrainstormingIdea(this.newBrainstormingIdea, this.community)
          .subscribe(
            data => {
              this.ideas.push(data);
              this.newBrainstormingIdea = new BrainstormingIdea();
              this.isSubmitted = false;
            },
            err => console.log(err)
          );
      }*/

}
