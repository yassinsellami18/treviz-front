import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output} from '@angular/core';
import { BrainstormingIdea } from '../../models/brainstorming/brainstorming-idea.model';
import {BrainstormingEnhancementService} from '../../../core/services/brainstorming/brainstorming-enhancement.service';
import {MatDialog} from '@angular/material';
import {DocumentUploadComponent} from '../../documents/document-upload/document-upload.component';



@Component({
  moduleId: module.id,
  selector: 'app-brainstorming-idea-detail',
  templateUrl: 'brainstorming-idea-detail.component.html',
  styleUrls: ['brainstorming-idea-detail.component.scss']
})

export class BrainstormingIdeaDetailComponent implements OnChanges, OnDestroy {

  @Input() idea: BrainstormingIdea;

  @Output() onIdeaUnselect = new EventEmitter<boolean>();

  public enhancementsFetched = false;

  private alive = true;

  constructor(private brainstormingEnhancementsService: BrainstormingEnhancementService,
              public dialog: MatDialog) {
  }

  ngOnChanges() {
    this.enhancementsFetched = false;
    this.brainstormingEnhancementsService.getIdeaEnhancements(this.idea.hash)
      .takeWhile(() => this.alive)
      .finally(() => this.enhancementsFetched = true);
  }

  ngOnDestroy() {
    this.alive = false;
  }

  goBackToSession() {
    this.onIdeaUnselect.emit(true);
  }


  openDocumentUploadDialog() {
    const dialogRef = this.dialog.open(DocumentUploadComponent);
    dialogRef.componentInstance.documentDto.idea = this.idea.hash;
    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(
        data => {
          if (data) {
            this.idea.documents.push(data);
          }
        },
        err => console.log(err)
      );
  }

}
