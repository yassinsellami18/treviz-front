import {Component, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {BrainstormingIdea} from '../../models/brainstorming/brainstorming-idea.model';
import {BrainstormingIdeaService} from '../../../core/services/brainstorming/brainstorming-idea.service';


@Component({
  moduleId: module.id,
  selector: 'app-brainstorming-idea-card',
  templateUrl: 'brainstorming-idea-card.component.html',
  styleUrls: ['brainstorming-idea-card.component.scss']
})

export class BrainstormingIdeaCardComponent implements OnDestroy {

  @Input() idea: BrainstormingIdea;

  @Output()
  onIdeaSelect = new EventEmitter<BrainstormingIdea>();

  private alive = true;

  constructor(private brainstormingIdeaService: BrainstormingIdeaService) {
  }

  ngOnDestroy() {
    this.alive = false;
  }

  triggerLike() {
    if (this.currentUserLike()) {
      this.brainstormingIdeaService.unlikeIdea(this.idea.hash)
        .takeWhile(() => this.alive)
        .subscribe(
          data => this.idea = data,
          err => console.log(err)
        );
    } else {
      this.brainstormingIdeaService.likeIdea(this.idea.hash)
        .takeWhile(() => this.alive)
        .subscribe(
          data => this.idea = data,
          err => console.log(err)
        );
    }
  }

  currentUserLike() {
    return this.idea.liked.some(user => user.username === localStorage.getItem('username'));
  }

  selectIdea() {
    this.onIdeaSelect.emit(this.idea);
  }

  /*    openCommentDialog(idea: BrainstormingIdea) {
        const dialogRef = this.dialog.open(BrainstormingCommentDialogComponent);
        dialogRef.componentInstance.idea = idea;
      }

      openForkDialog(idea: BrainstormingIdea) {
        const dialogRef = this.dialog.open(BrainstormingForkDialogComponent);
        dialogRef.componentInstance.idea = idea;
      }

      openLikedDialog(idea: BrainstormingIdea) {
        const dialogRef = this.dialog.open(BrainstormingLikedDialogComponent);
        dialogRef.componentInstance.users = idea.liked;
      }
*/

}
