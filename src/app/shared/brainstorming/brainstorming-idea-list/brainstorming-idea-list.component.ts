import {Component, EventEmitter, Input, OnChanges, Output} from '@angular/core';
import { BrainstormingSession } from '../../models/brainstorming/brainstorming-session.model';
import { BrainstormingIdeaService } from '../../../core/services/brainstorming/brainstorming-idea.service';
import { BrainstormingIdeaDto } from '../../models/brainstorming/brainstorming-idea.model.dto';
import { BrainstormingIdea } from '../../models/brainstorming/brainstorming-idea.model';



@Component({
  moduleId: module.id,
  selector: 'app-brainstorming-idea-list',
  templateUrl: 'brainstorming-idea-list.component.html',
  styleUrls: ['brainstorming-idea-list.component.scss']
})
export class BrainstormingIdeaListComponent implements OnChanges {

  @Input() session: BrainstormingSession;

  ideas: BrainstormingIdea[] = [];

  ideaDto: BrainstormingIdeaDto;

  ideasFetched = false;

  ideaSubmitted = false;

  @Output()
  onIdeaSelect = new EventEmitter<BrainstormingIdea>();

  private alive = true;

  constructor(private brainstormingIdeaService: BrainstormingIdeaService) {
    this.ideaDto = new BrainstormingIdeaDto();
  }

  ngOnChanges() {
    this.brainstormingIdeaService.getIdeas(this.session.hash)
      .takeWhile(() => this.alive)
      .finally(() => this.ideasFetched = true)
      .subscribe(
        data => this.ideas = data,
        err => console.log(err)
      );
  }

  onSubmit() {
    this.ideaSubmitted = true;
    this.brainstormingIdeaService.postIdea(this.session.hash, this.ideaDto)
      .takeWhile(() => this.alive)
      .finally(() => {
        this.ideaSubmitted = false;
        this.ideaDto.content = '';
      }).subscribe(
      data => this.ideas.push(data),
      err => console.log(err)
    );
  }

  selectIdea(idea: BrainstormingIdea) {
    this.onIdeaSelect.emit(idea);
  }

}
