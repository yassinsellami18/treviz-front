import { Component, Inject, OnDestroy } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { BrainstormingSessionDto } from '../../models/brainstorming/brainstorming-session.model.dto';
import { BrainstormingSessionService } from '../../../core/services/brainstorming/brainstorming-session.service';



@Component({
  moduleId: module.id,
  selector: 'app-brainstorming-session-create-dialog',
  templateUrl: 'brainstorming-session-create-dialog.component.html',
  styleUrls: ['brainstorming-session-create-dialog.component.scss']
})

export class BrainstormingSessionCreateDialogComponent implements OnDestroy {

  sessionDto: BrainstormingSessionDto;

  submitted = false;

  private alive = true;

  constructor(public dialogRef: MatDialogRef<BrainstormingSessionCreateDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private brainstormingSessionService: BrainstormingSessionService) {

    this.sessionDto = new BrainstormingSessionDto();
    this.sessionDto.open = 'true';

    data.project ? this.sessionDto.project = data.project.hash : this.sessionDto.project = null;
    // data.job ? this.job = data.job : this.job = null;
    data.community ? this.sessionDto.community = data.community.hash : this.sessionDto.project = null;
  }

  ngOnDestroy() {
    this.alive = false;
  }

  onSubmit() {
    this.submitted = true;
    this.brainstormingSessionService.postSession(this.sessionDto)
      .takeWhile(() => this.alive)
      .finally(() => this.submitted = false)
      .subscribe(
        session => this.dialogRef.close(session),
        err => console.log(err)
      );
  }

}
