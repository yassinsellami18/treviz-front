import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges} from '@angular/core';
import {Community} from '../../models/community/community.model';
import {Project} from '../../models/project/project.model';
import {BrainstormingSession} from '../../models/brainstorming/brainstorming-session.model';
import {BrainstormingSessionService} from '../../../core/services/brainstorming/brainstorming-session.service';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {BrainstormingSessionCreateDialogComponent} from '../brainstorming-session-create-dialog/brainstorming-session-create-dialog.component';



@Component({
 selector: 'app-brainstorming-sessions-list',
 templateUrl: 'brainstorming-sessions-list.component.html',
 styleUrls: ['brainstorming-sessions-list.component.scss']
})
export class BrainstormingSessionsListComponent implements OnChanges, OnDestroy {

  @Input()
  private community: Community;

  @Input()
  private project: Project;

  @Output()
  onSessionSelection = new EventEmitter<BrainstormingSession>();

  public sessions: BrainstormingSession[];

  public sessionsFetched = false;

  public alive = true;

  constructor(private brainstormingSessionService: BrainstormingSessionService,
              public dialog: MatDialog) { }

  ngOnChanges(changes: SimpleChanges) {
    const options = {
      community: '',
      project: ''
    };

    for (const propName in changes) {
      if (propName === 'community' && this.community !== null) {
        options.community = this.community.hash;
      } else if (propName === 'project' && this.project !== null) {
        options.project = this.project.hash;
      }
    }

    this.brainstormingSessionService.getSessions(options)
      .takeWhile(() => this.alive)
      .finally(() => this.sessionsFetched = true)
      .subscribe(
        sessions => this.sessions = sessions,
        err => console.log(err),
      );
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  openSessionCreation() {
    const config = new MatDialogConfig();
    config.data = {
      community: this.community,
      project: this.project
    };

    const dialogRef = this.dialog.open(BrainstormingSessionCreateDialogComponent, config);

    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(result => {
        if (typeof result !== 'undefined' && result !== null && result !== false) {
          this.sessions.push(result);
        }
      });
  }

  selectSession(session: BrainstormingSession): void {
    this.onSessionSelection.emit(session);
  }

}
