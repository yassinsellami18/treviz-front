import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {Community} from '../models/community/community.model';
import {Project} from '../models/project/project.model';
import {BrainstormingSession} from '../models/brainstorming/brainstorming-session.model';
import {BrainstormingIdea} from '../models/brainstorming/brainstorming-idea.model';

@Component({
  moduleId: module.id,
  selector: 'app-brainstorming',
  templateUrl: 'brainstorming.component.html',
  styleUrls: ['brainstorming.component.css']
})

export class BrainstormingComponent {

  @Input()
  community: Community;

  @Input()
  project: Project;

  private selectedSession: BrainstormingSession;

  private selectedIdea: BrainstormingIdea;

  constructor() {
    this.project = null;
    this.community = null;
  }

  selectSession(session: BrainstormingSession): void {
    this.selectedSession = session;
  }

  selectIdea(idea: BrainstormingIdea): void {
    this.selectedIdea = idea;
  }

  unSelectSession(): void {
    this.selectedSession = undefined;
  }

  unSelectIdea(): void {
    this.selectedIdea = undefined;
  }
}
