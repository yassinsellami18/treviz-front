/**
 * Created by huber on 26/08/2017.
 */

import {Component, EventEmitter, Input, Output} from '@angular/core';
import { BrainstormingSessionService } from '../../../core/services/brainstorming/brainstorming-session.service';
import { BrainstormingSession } from '../../models/brainstorming/brainstorming-session.model';
import { BrainstormingIdea } from '../../models/brainstorming/brainstorming-idea.model';
import {BrainstormingIdeaDto} from "../../models/brainstorming/brainstorming-idea.model.dto";

@Component({
    selector: 'app-brainstorming-session-detail',
    templateUrl: 'brainstorming-session-detail.component.html',
    styleUrls: ['brainstorming-session-detail.component.css']
})
export class BrainstormingSessionDetailComponent {

  @Input()
  private session: BrainstormingSession;

  @Output()
  onSessionUnselect = new EventEmitter<boolean>();

  selectedIdea: BrainstormingIdea;

  constructor() {
  }

  selectIdea(idea: BrainstormingIdea) {
    this.selectedIdea = idea;
  }

  unselectIdea() {
    this.selectedIdea = undefined;
  }

  goBack() {
    this.onSessionUnselect.emit(true);
  }
}
