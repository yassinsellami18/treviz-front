import { Component, Input } from '@angular/core';
import { Project } from '../../models/project/project.model';

@Component({
    moduleId: module.id,
    selector: 'app-project-thumbnail',
    templateUrl: 'project-thumbnail.component.html',
    styleUrls: ['project-thumbnail.component.scss']
})
export class ProjectThumbnailComponent {

  @Input() project: Project;

  constructor() {}

}
