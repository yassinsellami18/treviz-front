import {Component, Input, OnChanges, OnDestroy} from '@angular/core';
import { Project } from '../../models/project/project.model';
import {ProjectService} from '../../../core/services/project/project.service';

@Component({
  moduleId: module.id,
  selector: 'app-project-thumbnail-list',
  templateUrl: 'project-thumbnail-list.component.html',
  styleUrls: ['project-thumbnail-list.component.scss']
})
export class ProjectThumbnailListComponent implements OnChanges, OnDestroy {

  @Input() name: string;

  @Input() community: string;

  @Input() user: string;

  @Input() role: string;

  @Input() tags: string[];

  @Input() skills: string[];

  projects: Project[] = [];

  projectsFetched = false;

  moreToLoad = true;

  private alive = true;

  constructor(private projectService: ProjectService) {}

  ngOnChanges() {
    this.loadProjects(0);
  }

  ngOnDestroy() {
    this.alive = false;
  }

  loadProjects(offset: number) {
    this.projectsFetched = false;

    this.projectService.getProjects({name: this.name, tags: this.tags, skills: this.skills, user: this.user, role: this.role, community: this.community, offset: offset})
      .takeWhile(() => this.alive)
      .subscribe(
        projects => {
          this.projects = projects;
          this.moreToLoad = projects.length === 10;
        },
        err => console.log(err),
        () => this.projectsFetched = true
      );
  }

}
