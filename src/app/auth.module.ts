import { NgModule } from '@angular/core';
import { JwtModule, JwtModuleOptions } from '@auth0/angular-jwt'; // Authentication with JWT
import { environment } from 'environments/environment';

export function tokenGetter() {
  return localStorage.getItem('access_token');
}

export const jwtModuleConfig: JwtModuleOptions = {
  config: {
    tokenGetter: tokenGetter,
    whitelistedDomains: [environment.api_host],
    blacklistedRoutes: [
      `${environment.api_url}/login_check`,
      `${environment.api_url}/password`,
      '/api\.treviz\.org\/users\/[A-z-0-9]*\/confirm/',
      '/api\.treviz\.org\/users\/[A-z-0-9]*\/reset/'
    ]
  }
};

@NgModule({
  imports: [JwtModule.forRoot(jwtModuleConfig)],
  exports: [JwtModule]
})
export class AuthModule {}
