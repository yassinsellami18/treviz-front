import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../core/services/auth/auth-gard.service';

import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ProjectCreateComponent } from './project-create/project-create.component';

export const routes: Routes = [
  { path: '', component: ProjectListComponent, canActivate: [ AuthGuard ]},
  { path: 'create', component: ProjectCreateComponent, canActivate: [ AuthGuard ]},
  { path: ':hash', component: ProjectDetailComponent , canActivate: [ AuthGuard ]},
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class ProjectsRoutingModule { }
