/**
 * Created by Bastien on 15/02/2017.
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Skill } from '../../shared/models/tags/skill.model';
import { SkillService } from '../../core/services/skill.service';
import { ProjectService } from '../../core/services/project/project.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl } from '@angular/forms';
import { Tag } from '../../shared/models/tags/tag.model';
import { TagService } from '../../core/services/tag.service';
import { CommunityService } from '../../core/services/community/community.service';
import { Community } from '../../shared/models/community/community.model';
import { ChatRoomService } from '../../core/services/chat/chat-room.service';
import { NotificationService } from '../../core/services/notification.service';
import { ProjectDto } from '../../shared/models/project/project.model.dto';
import { Observable} from 'rxjs';
import { BrainstormingIdea } from '../../shared/models/brainstorming/brainstorming-idea.model';
import { BrainstormingIdeaService } from '../../core/services/brainstorming/brainstorming-idea.service';
import { Project } from '../../shared/models/project/project.model';



@Component({
  selector: 'app-create-project',
  templateUrl: 'project-create.component.html',
  styleUrls: ['project-create.component.scss']
})

export class ProjectCreateComponent implements OnInit, OnDestroy {

  projectDto: ProjectDto;

  skillCtrl: FormControl;
  filteredSkills: Observable<Skill[]>;
  skills: Skill[] = [];

  tagCtrl: FormControl;
  filteredTags: Observable<Tag[]>;
  tags: Tag[] = [];

  communities: Community[] = [];

  visibility = [
    {value: 'not_interested', viewValue: 'Private', description: 'Only connected users members of the communities this project is linked to will be able to see it.', selected: false},
    {value: 'public', viewValue: 'Public', description: 'Any connected user can see your project', selected: false}
  ];

  base_project_illustrations;
  logoBeingUploaded: File;
  logoUploaded = false;

  submitted = false;

  idea: BrainstormingIdea;
  ideaFetched = false;
  parentProject: Project;
  parentProjectFetched = false;

  private alive = true;

  constructor(private skillService: SkillService,
              private tagService: TagService,
              private projectService: ProjectService,
              private notificationService: NotificationService,
              private chatService: ChatRoomService,
              private communityService: CommunityService,
              private router: Router,
              private route: ActivatedRoute,
              private brainstormingIdeaService: BrainstormingIdeaService) {

    this.projectDto = new ProjectDto();

    // Search by skill
    this.skillCtrl = new FormControl();
    this.skillService.getSkillsHttp()
      .takeWhile(() => this.alive)
      .subscribe(
        skills => {
          this.skills = skills;
        },
        err => console.log('impossible to fetch skills')
      );
    this.filteredSkills = this.skillCtrl.valueChanges
      .startWith(null)
      .map(name => this.filterSkill(name));

    // Search by tag
    this.tagCtrl = new FormControl();
    this.tagService.getTagsHttp()
      .takeWhile(() => this.alive)
      .subscribe(
        tags => {
          this.tags = tags;
        },
        err => console.log('impossible to fetch tags')
      );
    this.filteredTags = this.tagCtrl.valueChanges
      .startWith(null)
      .map(name => this.filterTag(name));
  }

  ngOnInit(): void {

    this.communityService.getCommunities({user: localStorage.getItem('username')})
      .takeWhile(() => this.alive)
      .subscribe(
        availableCommunities => {
          this.communities = availableCommunities;
          for (const community of this.communities) {
            if (this.projectDto.communities.indexOf(community.hash) > -1) {
              community.isSelected = true;
            }
          }
          this.route.queryParams
            .takeWhile(() => this.alive)
            .subscribe((params: Params) => {
              const idea = params['idea'];
              const project = params['project'];
              if (idea) {
                this.brainstormingIdeaService.getIdea(idea)
                  .takeWhile(() => this.alive)
                  .finally(() => this.ideaFetched = true)
                  .subscribe(
                    data => {
                      this.idea = data;
                      this.projectDto.shortDescription = data.content.substring(0, 140);
                      this.projectDto.description = data.content;
                      this.projectDto.idea = idea;
                      try {
                        this.triggerSelect(
                          this.communities.find(community => community.hash === data.session.community.hash)
                        );
                      } catch {
                        console.log('The idea this project is based one does not belong to a community');
                      }
                    }
                  );
              }

              if (project) {
                this.projectService.getProject(project)
                .takeWhile(() => this.alive)
                .finally(() => this.parentProjectFetched = true)
                .subscribe(
                  data => {
                    console.log(data);
                    this.parentProject = data;
                    this.projectDto.name = `${data.name} (Fork)`;
                    this.projectDto.shortDescription = data.shortDescription;
                    this.projectDto.description = data.description;
                    this.projectDto.skills = data.skills.map((skill) => skill.name);
                    this.projectDto.tags = data.tags.map((skill) => skill.name);
                    this.projectDto.communities = data.communities.map((community) => community.hash);
                    this.projectDto.isPublic = data.isPublic;
                  },
                  err => console.log(err)
                );

              }
            });
        },
        err => console.log(err)
      );

    this.projectService.getProjectBaseIllustration()
      .takeWhile(() => this.alive)
      .subscribe(
        data => this.base_project_illustrations = data,
        err => console.log(err)
      );

  }

  ngOnDestroy() {
    this.alive = false;
  }

  onSubmit() {

    for (const visibility_type of this.visibility) {
      if (visibility_type.selected) {
        this.projectDto.isPublic = (visibility_type.value === 'public');
      }
    }

    this.projectDto.isPublic = this.projectDto.isPublic.toString();

    this.submitted = true;

    this.projectService.postProject(this.projectDto)
      .takeWhile(() => this.alive)
      .subscribe(
        project => {

          this.chatService.getRooms({project: project.hash})
            .takeWhile(() => this.alive)
            .subscribe(
              rooms => {
                project.rooms = rooms;
                this.chatService.projects.push(project);

                // Tell websocket server to instantiate new room.
                for (const room of rooms) {
                  const notification = {
                    type: 'CHAT_POST_ROOM',
                    content: room
                  };

                  this.notificationService.sendNotification(notification);
                }

                if (this.logoUploaded) {
                  this.projectService.postLogo(this.logoBeingUploaded, project.hash)
                    .subscribe(
                      () => this.router.navigate(['/projects', project.hash])
                    );
                } else {
                  this.router.navigate(['/projects', project.hash]);
                }

              },
              err => console.log(err)
            );
        },
        err => {
          console.log(err);
          alert('An error occured');
        },
        () => this.submitted = false
    );

  }

  filterSkill(val: string) {
    return val ? this.skills.filter((skill) => new RegExp(val, 'gi').test(skill.name)) : this.skills;
  }

  addSkill(skill: string) {
    if (skill !== '' && this.projectDto.skills.indexOf(skill) === -1) {
      this.projectDto.skills.push(skill);
    }
  }

  removeSkill(skill: string) {
    this.projectDto.skills.splice(this.projectDto.skills.indexOf(skill), 1);
  }

  filterTag(val: string) {
    return val ? this.tags.filter((tag) => new RegExp(val, 'gi').test(tag.name)) : this.tags;
  }

  addTag(tag: string) {
    if (tag !== '' && this.projectDto.tags.indexOf(tag) === -1) {
      this.projectDto.tags.push(tag);
    }
  }

  removeTag(tag: string) {
    this.projectDto.tags.splice(this.projectDto.tags.indexOf(tag), 1);
  }

  selectVisibility(i: number) {
    this.visibility.forEach((item, index) => {
      item.selected = index === i;
    });
  }

  triggerSelect(community: Community) {
    const index = this.projectDto.communities.indexOf(community.hash);
    if (index === -1) {
      this.projectDto.communities.push(community.hash);
      community.isSelected = true;
    } else {
      if (index > -1) {
        this.projectDto.communities.splice(index, 1);
        community.isSelected = false;
      }
    }
  }

  selectPicture(imageUrl: string) {
    if (this.projectDto.logoUrl === imageUrl) {
      this.projectDto.logoUrl = '';
    } else {
      this.projectDto.logoUrl = imageUrl;
    }
  }

  addFile(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.logoBeingUploaded = fileList[0];
      this.logoUploaded = true;
    }
  }

}
