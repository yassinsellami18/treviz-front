import { NgModule } from '@angular/core';
import { SharedModule} from '../shared/shared.module';

import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectListComponent } from './project-list/project-list.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { ProjectListFilterDialogComponent } from './project-list/project-list-filter-dialog.component';
import { ProjectEditDialogComponent } from './project-detail/project-edit-dialog.component';
import { ProjectCreateComponent } from './project-create/project-create.component';
import { ProjectManageTeamDialogComponent } from './project-manage-team-dialog/project-manage-team-dialog.component';
import { ProjectRoleManagementComponent } from './project-role-management/project-role-management.component';
import { ProjectNotificationPreferencesDialogComponent } from './project-notification-preferences-dialog/project-notification-preferences-dialog.component';

@NgModule({
  imports: [
    SharedModule,
    ProjectsRoutingModule
  ],
  declarations: [
    ProjectCreateComponent,
    ProjectDetailComponent,
    ProjectEditDialogComponent,
    ProjectManageTeamDialogComponent,
    ProjectListComponent,
    ProjectListFilterDialogComponent,
    ProjectRoleManagementComponent,
    ProjectNotificationPreferencesDialogComponent
  ],
  exports: [],
  providers: [],
  entryComponents: [
    ProjectEditDialogComponent,
    ProjectListFilterDialogComponent,
    ProjectManageTeamDialogComponent,
    ProjectNotificationPreferencesDialogComponent
  ]
})
export class ProjectsModule { }

