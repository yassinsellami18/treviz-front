
import {Component, Inject, OnDestroy} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FormControl} from '@angular/forms';
import {Skill} from '../../shared/models/tags/skill.model';
import {SkillService} from '../../core/services/skill.service';
import {Observable} from 'rxjs';
import {Tag} from '../../shared/models/tags/tag.model';
import {TagService} from '../../core/services/tag.service';



@Component({
  selector: 'app-project-list-filter-dialog',
  templateUrl: 'project-list-filter-dialog.component.html',
  styleUrls: ['project-list-filter-dialog.component.scss']
})
export class ProjectListFilterDialogComponent implements OnDestroy {

  params = {
    name: '',
    tags: [],
    skills: [],
    offset: 0,
    nb: 10,
    user: '',
    role: '',
    community: ''
  };

  skillCtrl: FormControl;
  filteredSkills: Observable<Skill[]>;
  skills: Skill[] = [];

  tagCtrl: FormControl;
  filteredTags: Observable<Tag[]>;
  tags: Tag[] = [];

  submitted: boolean;
  private alive = true;

  constructor(public dialogRef: MatDialogRef<ProjectListFilterDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data,
              private skillService: SkillService,
              private tagService: TagService) {
    this.params = data;
    this.skillCtrl = new FormControl();
    this.skillService.getSkillsHttp()
      .takeWhile(() => this.alive)
      .subscribe(
        skills => {
          this.skills = skills;
        },
        err => console.log('impossible to fetch skills')
      );

    this.filteredSkills = this.skillCtrl.valueChanges
      .startWith(null)
      .map(name => this.filterSkill(name));

    this.tagCtrl = new FormControl();
    this.tagService.getTagsHttp()
      .takeWhile(() => this.alive)
      .subscribe(
        tags => {
          this.tags = tags;
        },
        err => console.log('impossible to fetch tags')
      );

    this.filteredTags = this.tagCtrl.valueChanges
      .startWith(null)
      .map(name => this.filterTag(name));
  }

  ngOnDestroy() {
    this.alive = false;
  }

  filterSkill(val: string) {
    return val ? this.skills.filter((skill) => new RegExp(val, 'gi').test(skill.name)) : this.skills;
  }

  addSkill(skill: string) {
    if (skill !== '' && this.params.skills.indexOf(skill) === -1) {
      this.params.skills.push(skill);
    }
  }

  removeSkill(skill: string) {
    this.params.skills.splice(this.params.skills.indexOf(skill), 1);
  }

  filterTag(val: string) {
    return val ? this.tags.filter((tag) => new RegExp(val, 'gi').test(tag.name)) : this.tags;
  }

  addTag(tag: string) {
    if (tag !== '' && this.params.tags.indexOf(tag) === -1) {
      this.params.tags.push(tag);
    }
  }

  removeTag(tag: string) {
    this.params.tags.splice(this.params.tags.indexOf(tag), 1);
  }

  emptyFilters() {
    this.params = {
      name: '',
      tags: [],
      skills: [],
      offset: 0,
      nb: 10,
      user: '',
      role: '',
      community: ''
    };
  }

  onSubmit() {
    this.dialogRef.close(this.params);
  }

}
