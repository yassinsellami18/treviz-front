/**
 * Created by Bastien on 15/02/2017.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import { ProjectService } from '../../core/services/project/project.service';
import { Project } from '../../shared/models/project/project.model';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ProjectListFilterDialogComponent } from './project-list-filter-dialog.component';



@Component({
  selector: 'app-project-list',
  templateUrl: 'project-list.component.html',
  styleUrls: ['project-list.component.scss']
})
export class ProjectListComponent implements OnInit, OnDestroy {

  projects: Project[] = [];
  personalProjects: Project[] = [];

  private params = {
    name: '',
    tags: [],
    skills: [],
    offset: 0,
    nb: 10,
    user: '',
    role: '',
    community: ''
  };

  personalProjectsFetched = false;
  projectsFetched = false;

  moreToLoad = true;

  private alive= true;

  constructor(private projectService: ProjectService,
              public dialog: MatDialog) { }


  ngOnInit(): void {
    this.loadProjects();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  loadProjects() {
    this.projectService.getProjects({})
      .takeWhile(() => this.alive)
      .subscribe(
        projects => {
          this.projects = projects;
          this.moreToLoad = projects.length === 10;
          },
        err => console.log(err),
        () => this.projectsFetched = true
      );

    this.projectService.getProjects({user: localStorage.getItem('username')})
      .takeWhile(() => this.alive)
      .subscribe(
        data => this.personalProjects = data,
        err => console.log(err),
        () => this.personalProjectsFetched = true
      );
  }

  openFilterDialog() {
    const config = new MatDialogConfig();
    config.data = this.params;

    const dialogRef = this.dialog.open(ProjectListFilterDialogComponent, config);

    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(result => {
        if (typeof result !== 'undefined' && result !== null && result !== false) {
          this.params = result;
          this.projectsFetched = false;
          this.projectService.getProjects(result)
            .takeWhile(() => this.alive)
            .finally(() => this.projectsFetched = true)
            .subscribe(
              data => {
                this.projects = data;
                this.moreToLoad = data.length === 10;
                },
              err => console.log(err)
            );
        }
      });
  }

  loadMore() {
    if (this.projectsFetched) {
      this.projectsFetched = false;
      this.params.offset += this.params.offset + 10;
      this.projectService.getProjects(this.params)
        .takeWhile(() => this.alive)
        .subscribe(
          data => {
            this.moreToLoad = data.length === 10;
            for (const project of data) {
              this.projects.push(project);
            }
          },
          err => console.log(err),
          () => this.projectsFetched = true
        );
    }
  }

}
