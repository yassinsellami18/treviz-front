/**
 * Created by Bastien on 26/02/2017.
 */

import { Component, OnInit, OnDestroy, Inject} from '@angular/core';
import { SkillService } from '../../core/services/skill.service';
import { ProjectService } from '../../core/services/project/project.service';
import { Skill } from '../../shared/models/tags/skill.model';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl } from '@angular/forms';

import { Tag } from '../../shared/models/tags/tag.model';
import { TagService } from '../../core/services/tag.service';
import { Project } from '../../shared/models/project/project.model';
import { Community } from '../../shared/models/community/community.model';
import { CommunityService } from '../../core/services/community/community.service';
import { ProjectDto } from '../../shared/models/project/project.model.dto';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-project-dialog',
  templateUrl: 'project-edit-dialog.component.html',
  styleUrls: ['project-edit-dialog.component.scss']
})
export class ProjectEditDialogComponent implements OnInit, OnDestroy {

  skillCtrl: FormControl;
  filteredSkills: Observable<Skill[]>;
  skills: Skill[] = [];

  tagCtrl: FormControl;
  filteredTags: Observable<Tag[]>;
  tags: Tag[] = [];

  communities: Community[] = [];
  projectDto: ProjectDto;

  // True when request is being made.
  pendingChange = false;
  deleteIntent = false;

  hash: string;

  private alive = true;

  visibility = [
    {value: 'not_interested', viewValue: 'Private', description: 'Only connected users members of the communities this project is linked to will be able to see it.', selected: false},
    {value: 'public', viewValue: 'Public', description: 'Any connected user can see your project', selected: false}
  ];

  constructor(public dialogRef: MatDialogRef<ProjectEditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Project,
              private skillService: SkillService,
              private tagService: TagService,
              private communityService: CommunityService,
              private projectService: ProjectService,
              private router: Router) {

    this.hash = data.hash;

    this.projectDto = new ProjectDto();

    this.projectDto.name = data.name;
    data.shortDescription !== null ? this.projectDto.shortDescription = data.shortDescription : this.projectDto.shortDescription = '';
    this.projectDto.description = data.description;
    this.projectDto.logoUrl = data.logoUrl;
    this.projectDto.isPublic = data.isPublic;
    if (data.skills) {
      this.projectDto.skills = data.skills.map(a => a.name);
    }
    if (data.tags) {
      this.projectDto.tags = data.tags.map(a => a.name);
    }
    if (data.idea) {
      this.projectDto.idea = data.idea.hash;
    }

    // Search by skill
    this.skillCtrl = new FormControl();
    this.skillService.getSkillsHttp()
      .takeWhile(() => this.alive)
      .subscribe(
        skills => {
          this.skills = skills;
        },
        err => console.log('impossible to fetch skills')
      );
    this.filteredSkills = this.skillCtrl.valueChanges
      .startWith(null)
      .map(name => this.filterSkill(name));

    // Search by tag
    this.tagCtrl = new FormControl();
    this.tagService.getTagsHttp()
      .takeWhile(() => this.alive)
      .subscribe(
        tags => {
          this.tags = tags;
        },
        err => console.log('impossible to fetch tags')
      );
    this.filteredTags = this.tagCtrl.valueChanges
      .startWith(null)
      .map(name => this.filterTag(name));
  }

  /*
   * Load the communities of the project and those of the user.
   */
  ngOnInit(): void {
    this.communityService.getCommunities({project: this.hash})
      .takeWhile(() => this.alive)
      .subscribe(
        communities => {
          this.projectDto.communities = communities.map(a => a.hash);
          this.communityService.getCommunities({user: localStorage.getItem('username')})
            .takeWhile(() => this.alive)
            .subscribe(
              availableCommunities => {
                this.communities = availableCommunities;
                for (const community of this.communities) {
                  if (this.projectDto.communities.indexOf(community.hash) > -1) {
                    community.isSelected = true;
                  }
                }
              },
              err => console.log(err)
            );
        },
        err => console.log(err)
      );


  }

  ngOnDestroy(): void {
    this.alive = false;
    console.log('Stopping ProjectEditDialog component');
  }

  filterSkill(val: string) {
    return val ? this.skills.filter((skill) => new RegExp(val, 'gi').test(skill.name)) : this.skills;
  }

  addSkill(skill: string) {
    if (skill !== '' && this.projectDto.skills.indexOf(skill) === -1) {
      this.projectDto.skills.push(skill);
    }
  }

  removeSkill(skill: string) {
    this.projectDto.skills.splice(this.projectDto.skills.indexOf(skill), 1);
  }

  filterTag(val: string) {
    return val ? this.tags.filter((tag) => new RegExp(val, 'gi').test(tag.name)) : this.tags;
  }

  addTag(tag: string) {
    if (tag !== '' && this.projectDto.tags.indexOf(tag) === -1) {
      this.projectDto.tags.push(tag);
    }
  }

  removeTag(tag: string) {
    this.projectDto.tags.splice(this.projectDto.tags.indexOf(tag), 1);
  }

  selectVisibility(i: number) {
    this.visibility.forEach((item, index) => {
      if (index === i) {
        item.selected = true;
      } else {
        item.selected = false;
      }
    });
  }

  triggerSelect(community: Community) {
    const index = this.projectDto.communities.indexOf(community.hash);
    if (index === -1) {
      this.projectDto.communities.push(community.hash);
      community.isSelected = true;
    } else {
      if (index > -1) {
        this.projectDto.communities.splice(index, 1);
        community.isSelected = false;
      }
    }
    console.log(community.isSelected);
  }

  putProject() {

    for (const visibility_type of this.visibility) {
      if (visibility_type.selected) {
        this.projectDto.isPublic = (visibility_type.value === 'public');
      }
    }

    this.pendingChange = true;

    this.projectService.putProject(this.hash, this.projectDto)
      .takeWhile(() => this.alive)
      .subscribe(
        data => this.dialogRef.close(data),
        err => {
          alert('Une erreur est survenue, réessayez plus tard');
          console.log(err);
        },
        () => {
          console.log('Project updated');
          this.pendingChange = false;
        }
      );
  }

  deleteProjectIntent() {
    this.deleteIntent = !this.deleteIntent;
  }

  deleteProject() {

    this.pendingChange = true;

    this.projectService.deleteProject(this.hash)
      .takeWhile(() => this.alive)
      .subscribe(
        data => {
          this.dialogRef.close();
          this.router.navigate(['']);
        },
        err => {
          alert('Impossible de supprimer le projet, ré-essayez plus tard');
          this.dialogRef.close();
        },
        () => {
          this.pendingChange = false;
        }
      );

  }

}
