/**
 * Created by Bastien on 26/02/2017.
 */

import { Component, OnInit, OnDestroy} from '@angular/core';
import { ProjectService } from '../../core/services/project/project.service';
import { MatDialog, MatDialogConfig, MatTableDataSource } from '@angular/material';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { Project } from '../../shared/models/project/project.model';
import { ProjectMembershipService } from '../../core/services/project/project-membership.service';
import { ProjectInvitationService } from '../../core/services/project/project-invitation.service';
import { ProjectCandidacyService } from '../../core/services/project/project-candidacy.service';
import { ProjectMembership } from '../../shared/models/project/project-membership.model';
import { ProjectCandidacy } from '../../shared/models/project/project-candidacy.model';
import { ProjectInvitation } from '../../shared/models/project/project-invitation.model';
import { ProjectRole } from '../../shared/models/project/project-role.model';
import { ProjectEditDialogComponent } from './project-edit-dialog.component';
import { ProjectManageTeamDialogComponent } from '../project-manage-team-dialog/project-manage-team-dialog.component';
import { CandidacyDialogComponent } from '../../shared/candidacy/candidacy-dialog/candidacy-dialog.component';
import { ProjectNotificationPreferencesDialogComponent } from '../project-notification-preferences-dialog/project-notification-preferences-dialog.component';

@Component({
    selector: 'app-project-detail',
    templateUrl: 'project-detail.component.html',
    styleUrls: ['project-detail.component.scss']
})
export class ProjectDetailComponent implements OnInit, OnDestroy {

  project: Project;

  memberships: ProjectMembership[] = [];
  userMembership: ProjectMembership = null;
  membershipsFetched = false;
  membershipSource: MatTableDataSource<ProjectMembership>;
  membershipsColumns = ['user', 'role'];

  candidacies: ProjectCandidacy[]  = [];
  userCandidacy: ProjectCandidacy = null;
  candidaciesFetched = false;

  invitations: ProjectInvitation[] = [];
  userInvitation: ProjectInvitation = null;
  invitationsFetched = false;

  logoBeingUploaded = false;

  private alive = true;

  constructor(private projectService: ProjectService,
              private projectMembershipService: ProjectMembershipService,
              private projectInvitationService: ProjectInvitationService,
              private projectCandidacyService: ProjectCandidacyService,
              private route: ActivatedRoute,
              public dialog: MatDialog,
              public router: Router) { }

  ngOnInit() {
    this.fetchData();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  private fetchData() {

    this.route.params
      .takeWhile(() => this.alive)
      .subscribe((params: Params) => {

        this.clear();

        const hash = params['hash'];

        this.projectService.getProject(hash)
          .takeWhile(() => this.alive)
          .subscribe(project => {
              this.project = project;
            },
            err => {
              console.log(err);
              this.router.navigate(['/projects']);
            }
          );

        this.userMembership = new ProjectMembership();
        this.userMembership.role = new ProjectRole();

        this.updateMemberships(hash);

        this.projectCandidacyService.getProjectCandidacies({project: hash})
          .takeWhile(() => this.alive)
          .subscribe(
            candidacies => {
              this.candidacies = candidacies;
              this.userCandidacy = candidacies.find((s: ProjectCandidacy) => (s.user.username === localStorage.getItem('username')));
            },
            err => console.log(err),
            () => this.candidaciesFetched = true
          );

        this.projectInvitationService.getProjectInvitations({project: hash})
          .takeWhile(() => this.alive)
          .subscribe(
            invitations =>  {
              this.invitations = invitations;
              this.userInvitation = invitations.find((s: ProjectInvitation) => (s.user.username === localStorage.getItem('username')));
            },
            err => console.log(err),
            () => this.invitationsFetched = true
          );
      });

  }

  updateMemberships(hash) {
    this.projectMembershipService.getProjectMemberships({project: hash})
    .takeWhile(() => this.alive)
    .subscribe(
      memberships => {
        this.memberships = memberships;
        this.userMembership = memberships.find((s: ProjectMembership) => (s.user.username === localStorage.getItem('username')));
        this.membershipSource = new MatTableDataSource<ProjectMembership>(memberships);
      },
      err => console.log(err),
      () => this.membershipsFetched = true
    );
  }

  acceptCandidacy(candidacy: ProjectCandidacy): void {
    this.projectCandidacyService.acceptProjectCandidacy(candidacy.hash)
      .takeWhile(() => this.alive)
      .subscribe(
        data => this.removeCandidacy(candidacy),
        err => console.log(err)
      );
  }

  declineCandidacy(candidacy: ProjectCandidacy): void {
    this.projectCandidacyService.deleteProjectCandidacy(candidacy.hash)
      .takeWhile(() => this.alive)
      .subscribe(
        data => this.removeCandidacy(candidacy),
        err => console.log(err)
      );
  }

  acceptInvitation(): void {
    this.projectInvitationService.acceptProjectInvitation(this.userInvitation.hash)
      .takeWhile(() => this.alive)
      .subscribe(
        data => {
          this.clear();
          this.fetchData();
        },
        err => console.log(err)
      );
  }

  deleteInvitation(invitation: ProjectInvitation): void {
    this.projectInvitationService.deleteProjectInvitation(invitation.hash)
      .takeWhile(() => this.alive)
      .subscribe(
        data => this.invitations.splice(this.invitations.indexOf(invitation), 1),
        err => console.log(err)
      );
  }

  handleCandidacyAccepted(candidacy: ProjectCandidacy) {
    this.removeCandidacy(candidacy);
    this.updateMemberships(this.project.hash);
  }

  removeCandidacy(candidacy: ProjectCandidacy) {
    this.candidacies.splice(this.candidacies.indexOf(candidacy), 1);
  }

  hasPermission(permission: string): boolean {
    try {
      return this.userMembership.role.permissions.find(a => a === permission) !== null;
    } catch (e) {
      return false;
    }
  }

  changeLogo(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];

      this.logoBeingUploaded = true;

      this.projectService.postLogo(file, this.project.hash)
        .takeWhile(() => this.alive)
        .subscribe(
          data => {
            this.project = data;
            this.logoBeingUploaded = false;
          },
          error => {
            alert('The picture could not be uploaded. Try reduce its size.');
            this.logoBeingUploaded = false;
          }
        );
    }
  }

  openEditMode() {
    const config = new MatDialogConfig();
    config.data = this.project;

    const dialogRef = this.dialog.open(ProjectEditDialogComponent, config);

    dialogRef.afterClosed().takeWhile(() => this.alive)
      .subscribe(result => {
        if (typeof result !== 'undefined' && result !== null && result !== false) {
          this.project = result;
        }
      });
  }

  openTeamManagementDialog() {
    const config = new MatDialogConfig();

    config.data = {
      memberships: this.memberships,
      invitations: this.invitations,
      candidacies: this.candidacies,
      project: this.project
    };

    const dialogRef = this.dialog.open(ProjectManageTeamDialogComponent, config);

    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe( result => {
      console.log('The dialog was closed');
    });

  }

  openCandidacyDialog() {
    const config = new MatDialogConfig();
    config.data = {
      project: this.project
    };

    const dialogRef = this.dialog.open(CandidacyDialogComponent, config);

    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(result => {
        if (typeof result !== 'undefined' && result !== null && result !== false) {
          this.userCandidacy = result;
        }
      });
  }

  openNotificationPreferencesDialog() {
    const config = new MatDialogConfig();
    config.data = this.userMembership;

    const dialogRef = this.dialog.open(ProjectNotificationPreferencesDialogComponent, config);
  }

  private clear() {
    this.membershipsFetched = false;
    this.project = null;
    this.memberships = [];
    this.candidacies  = [];
    this.invitations = [];
    this.logoBeingUploaded = false;
  }

}
