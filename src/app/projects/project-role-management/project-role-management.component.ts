import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output} from '@angular/core';
import {ProjectRole} from '../../shared/models/project/project-role.model';
import {ProjectRoleService} from '../../core/services/project/project-role.service';
import {Project} from '../../shared/models/project/project.model';
import {MatSnackBar, MatSnackBarConfig} from "@angular/material";
import {SuccessSnackBarComponent} from "../../shared/snackbars/success-snack-bar/success-snack-bar.component";

@Component({
  selector: 'app-project-role-management',
  templateUrl: 'project-role-management.component.html',
  styleUrls: ['project-role-management.component.scss']
})
export class ProjectRoleManagementComponent implements OnChanges, OnDestroy {

  @Input()
  project: Project;

  @Input()
  role: ProjectRole;

  @Output()
  onRoleUpdate = new EventEmitter<ProjectRole>();

  permissions = {
    'UPDATE_PROJECT': false,
    'DELETE_PROJECT': false,
    'MANAGE_ROLE': false,
    'MANAGE_MEMBERSHIP': false,
    'MANAGE_POST': false,
    'MANAGE_CANDIDACIES': false,
    'MANAGE_INVITATIONS': false,
    'MANAGE_DOCUMENT': false,
    'MANAGE_KANBAN': false,
    'MANAGE_CROWD_FUND': false,
    'MANAGE_JOBS': false
  };

  submitted = false;

  private alive = true;

  constructor(private projectRoleService: ProjectRoleService,
              public snackBar: MatSnackBar) {
    this.role = new ProjectRole();
  }

  ngOnChanges() {
    for (const permission of this.role.permissions) {
      this.permissions[permission] = true;
    }
  }

  ngOnDestroy() {
    this.alive = false;
  }

  onSubmit() {
    this.submitted = true;
    this.role.permissions = [];
    const permissionsNames =  Object.getOwnPropertyNames(this.permissions);
    for(const permissionName of permissionsNames) {
      if (this.permissions[permissionName]) {
        console.log(permissionName);
        this.role.permissions.push(permissionName);
      }
    }

    /*
     * If the roles does not have any hash, it is a new one that must be created.
     * Otherwise, update it.
     */
    if (!this.role.hash) {
      this.projectRoleService.postProjectRole(this.project.hash, this.role)
        .takeWhile(() => this.alive)
        .subscribe(
          data => {
            this.role = data;
            this.submitted = false;
            this.onRoleUpdate.emit(data);
            const config = new MatSnackBarConfig;
            config.duration = 1000;
            this.snackBar.openFromComponent(SuccessSnackBarComponent, config );
          },
          err => console.log(err)
        );
    } else {
      const updatedRole = new ProjectRole();
      updatedRole.name = this.role.name;
      updatedRole.defaultMember = this.role.defaultMember;
      updatedRole.defaultCreator = this.role.defaultCreator;
      updatedRole.permissions = this.role.permissions;
      const hash = this.role.hash;
      this.projectRoleService.putProjectRole(hash, updatedRole)
        .takeWhile(() => this.alive)
        .subscribe(
          data => {
            this.role = data;
            this.submitted = false;
            const config = new MatSnackBarConfig;
            config.duration = 1000;
            this.snackBar.openFromComponent(SuccessSnackBarComponent, config );
            this.onRoleUpdate.emit(data);
          },
          err => console.log(err)
        );
    }

  }

}
