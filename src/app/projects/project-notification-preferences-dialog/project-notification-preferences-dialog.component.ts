import { ProjectPreferencesService } from '../../core/services/project/project-preferences.service';
import { OnDestroy, Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ProjectMembership } from '../../shared/models/project/project-membership.model';
import { ProjectNotificationPreferences } from '../../shared/models/project/project-notification-preferences.model';


@Component({
    selector: 'app-project-notification-preferences-dialog',
    templateUrl: 'project-notification-preferences-dialog.component.html',
    styleUrls: ['project-notification-preferences-dialog.component.scss']
})
export class ProjectNotificationPreferencesDialogComponent implements OnDestroy {

    public projectNotificationPreferences = new ProjectNotificationPreferences();
    public loading = true;
    private membership;
    private alive = true;

    constructor(public dialogRef: MatDialogRef<ProjectNotificationPreferencesDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: ProjectMembership,
                private preferencesService: ProjectPreferencesService,
                public snackBar: MatSnackBar) {
        this.membership = data;
        this.preferencesService.getProjectPreferences(data.hash)
            .takeWhile(() => this.alive)
            .finally(() => this.loading = false)
            .subscribe(
                prefs => this.projectNotificationPreferences = prefs,
                err => console.log(err)
            );
    }

    ngOnDestroy() {
        this.alive = false;
    }

    updatePreferences() {
        this.loading = true;
        this.preferencesService.updatePreferences(this.membership.hash, this.projectNotificationPreferences)
            .takeWhile(() => this.alive)
            .finally(() => this.loading = false)
            .subscribe(
                data => this.snackBar.open('Your preferences have been updated !', 'Close', { duration: 3000 }),
                err => {
                    this.snackBar.open('We could not update your preferences, try again later !', 'Close', { duration: 3000 });
                    console.log(err);
                }
            );
    }

}
