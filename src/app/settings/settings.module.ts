// Angular Imports
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

// This Module's Components
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { AccountPreferencesComponent } from './account-preferences/account-preferences.component';

@NgModule({
    imports: [
        SharedModule,
        SettingsRoutingModule
    ],
    declarations: [
        SettingsComponent,
        NotificationsComponent,
        AccountPreferencesComponent,
    ],
    exports: []
})
export class SettingsModule {}
