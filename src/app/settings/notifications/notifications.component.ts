import { Component, OnDestroy } from '@angular/core';
import { UserPreferences } from '../../shared/models/users/user-preferences.model';
import { UserPreferencesService } from '../../core/services/user-preferences.service';
import { CurrentUserService } from '../../core/services/current-user.service';
import { MatSnackBar } from '@angular/material';

@Component({
    selector: 'app-settings-notifications',
    templateUrl: 'notifications.component.html',
    styleUrls: ['notifications.component.scss']
})
export class NotificationsComponent implements OnDestroy {

    public userPreferences = new UserPreferences();
    public loading = false;
    private username: string;
    private alive = true;

    // TODO: fetch and update preferences

    constructor(private userPreferencesService: UserPreferencesService,
                private currentUserService: CurrentUserService,
                public snackBar: MatSnackBar) {
        this.username = currentUserService.getCurrentUser().username;
        this.userPreferencesService.getUserPreferences(this.username)
                    .takeWhile(() => this.alive)
                    .subscribe(
                        data => this.userPreferences = data,
                        err => console.log(err)
                    );
    }

    ngOnDestroy() {
        this.alive = false;
    }

    public updateUserPreferences() {
        this.loading = true;
        this.userPreferencesService.updatePreferences(this.username, this.userPreferences)
            .takeWhile(() => this.alive)
            .finally(() => this.loading = false)
            .subscribe(
                data => {
                    this.snackBar.open('Your preferences have been updated !', 'Close', { duration: 3000 });
                },
                err => {
                    console.error(err);
                    this.snackBar.open('We could not update your preferences, please try again later.', 'Close', { duration: 3000 });
                }
            );
    }
}
