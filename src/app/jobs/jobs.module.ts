import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { JobsRoutingModule } from './jobs-routing.module';
import { JobListComponent } from './job-list/job-list.component';
import { JobDetailComponent } from './job-detail/job-detail.component';
import { JobManageDialogComponent } from './job-manage-dialog/job-manage-dialog.component';
import { JobListFilterDialogComponent } from './job-list/job-list-filter-dialog.component';

@NgModule({
  imports: [
    SharedModule,
    JobsRoutingModule
  ],
  declarations: [
    JobDetailComponent,
    JobListComponent,
    JobListFilterDialogComponent,
    JobManageDialogComponent,
  ],
  exports: [],
  providers: [],
  entryComponents: [
    JobManageDialogComponent,
    JobListFilterDialogComponent,
  ]
})
export class JobsModule { }

