import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {ProjectJob} from '../../shared/models/project/project-job.model';
import {ProjectJobDto} from '../../shared/models/project/project-job.model.dto';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProjectJobService} from '../../core/services/project/project-job.service';
import {ProjectCandidacyService} from '../../core/services/project/project-candidacy.service';
import {ProjectCandidacy} from '../../shared/models/project/project-candidacy.model';
import {Tag} from '../../shared/models/tags/tag.model';
import {Skill} from '../../shared/models/tags/skill.model';
import {SkillService} from '../../core/services/skill.service';
import {TagService} from '../../core/services/tag.service';
import {User} from '../../shared/models/users/user.model';
import {UserService} from '../../core/services/user.service';







@Component({
  selector: 'app-job-manage-dialog',
  templateUrl: 'job-manage-dialog.component.html',
  styleUrls: ['job-manage-dialog.component.scss']
})
export class JobManageDialogComponent implements OnInit, OnDestroy {

  jobDto: ProjectJobDto = new ProjectJobDto();

  jobSubmitted = false;

  candidacies: ProjectCandidacy[] = [];

  skillCtrl: FormControl;
  filteredSkills: Observable<Skill[]>;
  skills: Skill[] = [];

  tagCtrl: FormControl;
  filteredTags: Observable<Tag[]>;
  tags: Tag[] = [];

  holder: User;
  filteredHolder: User[];
  holderCtrl: FormControl;

  contact: User;
  filteredContact: User[];
  contactCtrl: FormControl;

  submitted = false;

  private alive = true;

  constructor(private dialogRef: MatDialogRef<JobManageDialogComponent>,
              private projectJobService: ProjectJobService,
              private userService: UserService,
              private skillService: SkillService,
              private tagService: TagService,
              private projectCandidacyService: ProjectCandidacyService,
              @Inject(MAT_DIALOG_DATA) public data: ProjectJob) {

    this.jobDto.name = data.name;
    this.jobDto.description = data.description;
    this.jobDto.monthlyReward = data.monthlyReward;
    if (data.tasks) { this.jobDto.tasks = data.tasks.map(task => task.hash); }
    if (data.tags) { this.jobDto.tags = data.tags.map(task => task.name); }
    if (data.skills) { this.jobDto.skills = data.skills.map(skill => skill.name); }
    if (data.holder) {
      this.jobDto.holder = data.holder.username;
      this.holder = data.holder;
    }
    if (data.contact) {
      this.jobDto.contact = data.contact.username;
      this.contact = data.contact;
    }

    // Search by skill
    this.skillCtrl = new FormControl();
    this.skillService.getSkillsHttp()
      .takeWhile(() => this.alive)
      .subscribe(
        skills => {
          this.skills = skills;
        },
        err => console.log('impossible to fetch skills')
      );
    this.filteredSkills = this.skillCtrl.valueChanges
      .startWith(null)
      .map(name => this.filterSkill(name));

    // Search by tag
    this.tagCtrl = new FormControl();
    this.tagService.getTagsHttp()
      .takeWhile(() => this.alive)
      .subscribe(
        tags => {
          this.tags = tags;
        },
        err => console.log('impossible to fetch tags')
      );
    this.filteredTags = this.tagCtrl.valueChanges
      .startWith(null)
      .map(name => this.filterTag(name));


    this.holderCtrl = new FormControl();
    this.holderCtrl.valueChanges
      .debounceTime(300)
      .distinctUntilChanged()
      .takeWhile(() => this.alive)
      .subscribe(
        name => {
          this.filteredHolder = [];
          if (name != null && name.length >= 2) {
            this.userService.getUsers({name: name})
              .subscribe(
                users => this.filteredHolder = users,
                err => console.log(err)
              );
          }
        }
      );

    this.contactCtrl = new FormControl();
    this.contactCtrl.valueChanges
      .debounceTime(300)
      .distinctUntilChanged()
      .takeWhile(() => this.alive)
      .subscribe(
        name => {
          console.log(name);
          this.filteredContact = [];
          if (name != null && name.length >= 2) {
            this.userService.getUsers({name: name})
              .takeWhile(() => this.alive)
              .subscribe(
                users => this.filteredContact = users,
                err => console.log(err)
              );
          }
        }
      );

  }

  ngOnInit() {
    this.projectCandidacyService.getProjectCandidacies({job: this.data.hash})
      .takeWhile(() => this.alive)
      .subscribe(
        candidacies => this.candidacies = candidacies,
        err => console.log(err)
      );
  }

  ngOnDestroy() {
    this.alive = false;
  }

  filterSkill(val: string) {
    return val ? this.skills.filter((skill) => new RegExp(val, 'gi').test(skill.name)) : this.skills;
  }

  addSkill(skill: string) {
    if (skill !== '' && this.jobDto.skills.indexOf(skill) === -1) {
      this.jobDto.skills.push(skill);
    }
  }

  removeSkill(skill: string) {
    this.jobDto.skills.splice(this.jobDto.skills.indexOf(skill), 1);
  }

  filterTag(val: string) {
    return val ? this.tags.filter((tag) => new RegExp(val, 'gi').test(tag.name)) : this.tags;
  }

  addTag(tag: string) {
    if (tag !== '' && this.jobDto.tags.indexOf(tag) === -1) {
      this.jobDto.tags.push(tag);
    }
  }

  removeTag(tag: string) {
    this.jobDto.tags.splice(this.jobDto.tags.indexOf(tag), 1);
  }


  setHolder(username: string): void {
    const holder = this.filteredHolder.find(user => user.username === username);
    if (holder) {
      this.holder = holder;
      this.jobDto.holder = username;
    }
  }

  removeHolder(): void {
    this.holder = null;
    this.jobDto.holder = undefined;
  }

  setContact(username: string): void {
    const contact = this.filteredContact.find(user => user.username === username);
    if (contact) {
      this.contact = contact;
      this.jobDto.contact = username;
    }
  }

  removeContact(): void {
    this.contact = null;
    this.jobDto.contact = undefined;
  }

  acceptCandidacy(candidacy: ProjectCandidacy): void {
    this.projectCandidacyService.acceptProjectCandidacy(candidacy.hash)
      .takeWhile(() => this.alive)
      .subscribe(
        data => {
          this.holder = candidacy.user;
          this.candidacies.splice(this.candidacies.indexOf(candidacy), 1);
        },
        err => console.log(err)
      );
  }

  removeCandidacy(candidacy: ProjectCandidacy): void {
    this.projectCandidacyService.deleteProjectCandidacy(candidacy.hash)
      .takeWhile(() => this.alive)
      .subscribe(
        data => this.candidacies.splice(this.candidacies.indexOf(candidacy), 1),
        err => console.log(err)
      );
  }

  onSubmit() {
    this.jobSubmitted = true;
    this.projectJobService.putProjectJob(this.data.hash, this.jobDto)
      .takeWhile(() => this.alive)
      .finally(() => this.jobSubmitted = false)
      .subscribe(
        job => this.dialogRef.close(job),
        err => console.log(err)
      );
  }
}
