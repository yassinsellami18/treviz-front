import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { ProjectJob } from '../../shared/models/project/project-job.model';
import { ProjectJobService } from '../../core/services/project/project-job.service';
import { JobListFilterDialogComponent } from './job-list-filter-dialog.component';



@Component({
  selector: 'app-job-list',
  templateUrl: 'job-list.component.html',
  styleUrls: ['job-list.component.scss']
})
export class JobListComponent implements OnInit, OnDestroy {

  jobs: ProjectJob[] = [];
  personalJobs: ProjectJob[] = [];

  private params = {
    holder: '',
    tags: [],
    skills: [],
    min_reward: 0,
    max_reward: 0,
    attributed: false,
    name: '',
    nb: 20,
    offset: 0};

  personalJobsFetched = false;
  jobsFetched = false;
  moreToLoad = false;

  private alive= true;

  constructor(private jobService: ProjectJobService,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.loadJobs();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  loadJobs() {
    this.jobService.getJobs(this.params)
      .takeWhile(() => this.alive)
      .subscribe(
        jobs => {
          this.jobs = jobs;
          this.moreToLoad = jobs.length === this.params.nb;
        },
        err => console.log(err),
        () => this.jobsFetched = true
      );

    this.jobService.getJobs({holder: localStorage.getItem('username')})
      .takeWhile(() => this.alive)
      .subscribe(
        data => this.personalJobs = data,
        err => console.log(err),
        () => this.personalJobsFetched = true
      );
  }

  openFilterDialog() {
    const config = new MatDialogConfig();
    config.data = this.params;

    const dialogRef = this.dialog.open(JobListFilterDialogComponent, config);

    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(result => {
        if (typeof result !== 'undefined' && result !== null && result !== false) {
          this.params = result;
          this.jobsFetched = false;
          this.jobService.getJobs(result)
            .takeWhile(() => this.alive)
            .finally(() => this.jobsFetched = true)
            .subscribe(
              data => this.jobs = data,
              err => console.log(err)
            );
        }
      });
  }

  loadMore() {
    if (this.jobsFetched) {
      this.jobsFetched = false;
      this.params.offset += this.params.offset + 10;
      this.jobService.getJobs(this.params)
        .takeWhile(() => this.alive)
        .subscribe(
          data => {
            for (const job of data) {
              this.jobs.push(job);
            }
          },
          err => console.log(err),
          () => this.jobsFetched = true
        );
    }
  }

}
