import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { CurrentUserService } from '../../core/services/current-user.service';
import { ProjectJobService } from '../../core/services/project/project-job.service';
import { ProjectJob } from '../../shared/models/project/project-job.model';
import { ProjectCandidacyService } from '../../core/services/project/project-candidacy.service';
import { ProjectCandidacy } from '../../shared/models/project/project-candidacy.model';
import { CandidacyDialogComponent } from '../../shared/candidacy/candidacy-dialog/candidacy-dialog.component';
import {JobManageDialogComponent} from '../job-manage-dialog/job-manage-dialog.component';



@Component({
  selector: 'app-job-detail',
  templateUrl: 'job-detail.component.html',
  styleUrls: ['job-detail.component.scss']
})
export class JobDetailComponent implements OnInit, OnDestroy {

  job: ProjectJob;
  jobFetched = false;
  canEdit = false;

  candidaciesFetched = false;
  userCandidacy: ProjectCandidacy;

  private alive = true;

  constructor(private jobService: ProjectJobService,
              private currentUserService: CurrentUserService,
              private projectCandidacyService: ProjectCandidacyService,
              private route: ActivatedRoute,
              public dialog: MatDialog) {
  }

  ngOnInit() {
    this.fetchData();
  }

  /**
   * Fetches the job identified by the current route.
   * Once the job is fetches, looks what kind of action the user should be displayed with:
   * manage applications, candidate, edit...
   */
  private fetchData() {

    this.route.params
      .takeWhile(() => this.alive)
      .subscribe(
        params => {
          const hash = params['hash'];

          this.jobService.getProjectJob(hash)
            .takeWhile(() => this.alive)
            .finally(() => this.jobFetched = true)
            .subscribe(
              data => {
                this.job = data;

                /*
                 * A user can edit a job only if he·she is the contact for the job,
                 * or if he·she has the rights to manage the jobs of the job's project.
                 */
                try {
                  this.canEdit = ((data.contact.username === localStorage.getItem('username'))
                    || this.currentUserService.getCurrentUser()
                      .projectMemberships
                      .find(project => project.hash === data.project.hash)
                      .role
                      .permissions
                      .some(permission => permission === 'MANAGE_JOBS'));
                } catch (e) {
                  this.canEdit = false;
                }

                /*
                 * Fetches the applications for this job.
                 * If the user cannot edit the job, and if no candidacy is found for him, he can candidate.
                 */
                this.projectCandidacyService.getProjectCandidacies({job: hash, user: localStorage.getItem('username')})
                  .takeWhile(() => this.alive)
                  .finally(() => this.candidaciesFetched = true)
                  .subscribe(
                    candidacies => {
                      if (candidacies.length > 0) {
                        this.userCandidacy = candidacies[0];
                      }
                    },
                    err => console.log(err)
                  );

              },
              err => console.log(err)
            );
        }
      );
  }


  openCandidacyDialog() {
    const config = new MatDialogConfig();
    config.data = {
      job: this.job
    };

    const dialogRef = this.dialog.open(CandidacyDialogComponent, config);

    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(result => {
        if (result) {
          this.userCandidacy = result;
        }
      });
  }

  openJobManagementDialog() {
    const config = new MatDialogConfig();
    config.data = this.job;

    const dialogRef = this.dialog.open(JobManageDialogComponent, config);

    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(result => {
        if (result) {
          this.job = result;
        }
      });
  }

  removeCandidacy(): void {
    this.projectCandidacyService.deleteProjectCandidacy(this.userCandidacy.hash)
      .takeWhile(() => this.alive)
      .subscribe(
        data => this.userCandidacy = null,
        err => console.log(err)
      );
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
