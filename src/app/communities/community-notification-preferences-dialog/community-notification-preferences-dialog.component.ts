import { CommunityPreferencesService } from '../../core/services/community/community-preferences.service';
import { OnDestroy, Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { CommunityMembership } from '../../shared/models/community/community-membership.model';
import { CommunityNotificationPreferences } from '../../shared/models/community/community-notification-preferences.model';


@Component({
    selector: 'app-community-notification-preferences-dialog',
    templateUrl: 'community-notification-preferences-dialog.component.html',
    styleUrls: ['community-notification-preferences-dialog.component.scss']
})
export class CommunityNotificationPreferencesDialogComponent implements OnDestroy {

    public communityNotificationPreferences = new CommunityNotificationPreferences();
    public loading = true;
    private alive = true;
    private membership;

    constructor(public dialogRef: MatDialogRef<CommunityNotificationPreferencesDialogComponent>,
                @Inject(MAT_DIALOG_DATA) public data: CommunityMembership,
                private preferencesService: CommunityPreferencesService,
                public snackBar: MatSnackBar) {
        this.membership = data;
        this.preferencesService.getCommunityPreferences(data.hash)
            .takeWhile(() => this.alive)
            .finally(() => this.loading = false)
            .subscribe(
                prefs => this.communityNotificationPreferences = prefs,
                err => console.log(err)
            );
    }

    ngOnDestroy() {
        this.alive = false;
    }

    updatePreferences() {
        this.loading = true;
        this.preferencesService.updatePreferences(this.membership.hash, this.communityNotificationPreferences)
            .takeWhile(() => this.alive)
            .finally(() => this.loading = false)
            .subscribe(
                data => this.snackBar.open('Your preferences have been updated !', 'Close', { duration: 3000 }),
                err => {
                    this.snackBar.open('We could not update your preferences, try again later !', 'Close', { duration: 3000 });
                    console.log(err);
                }
            );
    }

}
