import {Component, EventEmitter, Input, OnChanges, OnDestroy, Output} from '@angular/core';
import {Community} from '../../shared/models/community/community.model';
import {CommunityRole} from '../../shared/models/community/community-role.model';
import {CommunityRoleService} from '../../core/services/community/community-role.service';

@Component({
  selector: 'app-community-role-management',
  templateUrl: 'community-role-management.component.html',
  styleUrls: ['community-role-management.component.scss']
})
export class CommunityRoleManagementComponent implements OnChanges, OnDestroy {

  @Input()
  community: Community;

  @Input()
  role: CommunityRole;

  @Output()
  onRoleUpdate = new EventEmitter<CommunityRole>();

  permissions = {
    'UPDATE_COMMUNITY': false,
    'DELETE_COMMUNITY': false,
    'MANAGE_ROLE': false,
    'MANAGE_MEMBERSHIP': false,
    'MANAGE_POST': false,
    'MANAGE_CANDIDACIES': false,
    'MANAGE_INVITATIONS': false,
    'MANAGE_DOCUMENT': false,
    'MANAGE_BRAINSTORMING_SESSION': false,
    'MANAGE_BRAINSTORMING_IDEAS': false,
  };

  private alive = true;
  private changes = false;

  constructor(private communityRoleService: CommunityRoleService) {
    this.role = new CommunityRole();
  }

  ngOnChanges() {
    for (const permission of this.role.permissions) {
      this.permissions[permission] = true;
    }
  }

  ngOnDestroy() {
    this.alive = false;
  }

  onSubmit() {

    this.role.permissions = [];
    const permissions =  Object.getOwnPropertyNames(this.permissions);
    for (const permission in permissions) {
      if (this.permissions[permission]) {
        this.role.permissions.push(permission);
      }
    }

    /*
     * If the roles does not have any hash, it is a new one that must be created.
     * Otherwise, update it.
     */
    if (!this.role.hash) {
      this.communityRoleService.postCommunityRole(this.community.hash, this.role)
        .takeWhile(() => this.alive)
        .subscribe(
          data => {
            this.role = data;
            this.changes = false;
            this.onRoleUpdate.emit(data);
          },
          err => console.log(err)
        );
    } else {
      const hash = this.role.hash;
      this.role.hash = null;
      this.communityRoleService.putCommunityRole(hash, this.role)
        .takeWhile(() => this.alive)
        .subscribe(
          data => {
            this.role = data;
            this.changes = false;
            this.onRoleUpdate.emit(data);
          },
          err => console.log(err)
        );
    }

  }

}
