/**
 * Created by huber on 25/05/2017.
 */

import {Component, OnDestroy} from '@angular/core';
import {CommunityService} from '../../core/services/community/community.service';
import {Router} from '@angular/router';
import {ChatRoomService} from '../../core/services/chat/chat-room.service';
import {NotificationService} from '../../core/services/notification.service';
import {CommunityDto} from '../shared/community.model.dto';

@Component({
  selector: 'app-community-create',
  templateUrl: 'community-create.component.html',
  styleUrls: ['community-create.component.scss']
})
export class CommunityCreateComponent implements OnDestroy {

  public community: CommunityDto;
  public logoUploaded = false;
  public submitted = false;
  private alive = true;

  constructor(private communityService: CommunityService,
              private router: Router,
              private chatService: ChatRoomService,
              private notificationService: NotificationService) {
    this.community = new CommunityDto();
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  addFile(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.community.logo = fileList[0];
      this.logoUploaded = true;
    }
  }

  onSubmit() {

    this.submitted = true;
    this.community.open = this.community.open && this.community.isPublic; // communities can be open only if they are also public.

    this.communityService.postCommunity(this.community)
      .subscribe(
        data => {
          this.submitted = false;
          this.router.navigate(['/communities', data.hash]);
          this.chatService.getRooms({community: data.hash})
            .subscribe(
              rooms => {
                data.rooms = rooms;
                this.chatService.communities.push(data);

                // Tell websocket server to instantiate new room.
                for (const room of rooms) {
                  const notification = {
                    type: 'CHAT_POST_ROOM',
                    content: room
                  };

                  this.notificationService.sendNotification(notification);
                }

              },
              err => console.log(err)
            );
        },
        err => {
          this.submitted = false;
          console.log(err);
          alert('An error occurred');
        },
        () => console.log('Community created')
      );

  }


}
