import { NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommunityListComponent } from './community-list/community-list.component';
import { CommunityCreateComponent } from './community-create/community-create.component';
import { CommunityDetailComponent } from './community-detail/community-detail.component';
import { AuthGuard } from '../core/services/auth/auth-gard.service';

export const routes: Routes = [
  { path: '', component: CommunityListComponent, canActivate: [ AuthGuard ]},
  { path: 'create', component: CommunityCreateComponent, canActivate: [ AuthGuard ]},
  { path: ':hash', component: CommunityDetailComponent, canActivate: [ AuthGuard ]},
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class CommunitiesRoutingModule { }
