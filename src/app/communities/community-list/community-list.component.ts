/**
 * Created by huber on 29/04/2017.
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { Community } from '../../shared/models/community/community.model';
import { CommunityService } from '../../core/services/community/community.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-community-list',
    templateUrl: 'community-list.component.html',
    styleUrls: ['community-list.component.scss']
})
export class CommunityListComponent implements OnInit, OnDestroy {

    communities: Community[] = [];
    personalCommunities: Community[] = [];

    communitiesFetched = false;
    personalCommunitiesFetched = false;

    moreToLoad = true;

    private alive = true;

    constructor(private communityService: CommunityService,
                private router: Router) { }

    ngOnInit() {
      this.communityService.getCommunities({user: localStorage.getItem('username')})
        .takeWhile(() => this.alive)
        .subscribe(
          communities => this.personalCommunities = communities,
          err => console.log(err),
          () => this.personalCommunitiesFetched = true
        );

      this.communityService.getCommunities({})
        .takeWhile(() => this.alive)
        .subscribe(
          communities => {this.communities = communities; this.moreToLoad = communities.length === 10;},
          err => console.log(err),
          () => this.communitiesFetched = true
        );
    }

    ngOnDestroy() {
      this.alive = false;
    }

    goToCommunity(community: Community): void {
      this.router.navigate(['/communities', community.hash]);
    }

    loadMore() {
      this.communityService.getCommunities({offset: this.communities.length})
        .takeWhile(() => this.alive)
        .subscribe(
          communities => {this.communities = communities; this.moreToLoad = communities.length === 10;},
          err => console.log(err),
          () => this.communitiesFetched = true
        );
    }

}
