export class CommunityDto {
  name: string;
  isPublic: boolean|string;
  open: boolean|string;
  description: string;
  website: string;
  logo: File;
  logoUrl: string;
  backgroundImage: File;
  backgroundImageUrl: string;
}
