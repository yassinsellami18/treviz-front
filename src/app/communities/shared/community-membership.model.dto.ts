export class CommunityMembershipDto {
  name: string;
  role: string;
}
