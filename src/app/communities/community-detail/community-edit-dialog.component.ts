
import {Component, OnDestroy, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {CommunityService} from '../../core/services/community/community.service';
import {Community} from '../../shared/models/community/community.model';
import {Router} from '@angular/router';
import {CommunityDto} from '../shared/community.model.dto';

@Component({
  selector: 'app-community-edit-dialog',
  templateUrl: 'community-edit-dialog.component.html',
  styleUrls: ['community-edit-dialog.component.css']
})
export class CommunityEditDialogComponent implements OnDestroy {

  communityDto: CommunityDto;

  hash: string;

  // True when request is being made.
  pendingChange = false;
  deleteIntent = false;
  logoBeingUploaded: File;

  private alive = true;

  constructor(public dialogRef: MatDialogRef<CommunityEditDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Community,
              private communityService: CommunityService,
              private router: Router) {

    this.hash = data.hash;

    this.communityDto = new CommunityDto();
    this.communityDto.name = data.name;
    this.communityDto.description = data.description;
    this.communityDto.isPublic = data.isPublic;
    this.communityDto.open = data.open;
    this.communityDto.logoUrl = data.logoUrl;
    this.communityDto.backgroundImageUrl = data.backgroundImageUrl;
    this.communityDto.website = data.website;
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  onSubmit() {

    this.pendingChange = true;
    this.communityDto.open = this.communityDto.open && this.communityDto.isPublic; // communities can be open only if they are also public.

    /*
     * First, update the community without the logo.
     * if a logo is given, update it after.
     */
    this.communityService.putCommunity(this.hash, this.communityDto)
      .takeWhile(() => this.alive)
      .subscribe(
        data => {
          if (this.logoBeingUploaded) {
            this.communityService.postLogo(this.hash, this.logoBeingUploaded)
              .takeWhile(() => this.alive)
              .subscribe(
                community => this.dialogRef.close(community),
                err => {
                  alert('An error occurred');
                  console.log(err);
                },
                () => {
                  console.log('Community updated');
                  this.pendingChange = false;
                }
              );
          } else {
            this.dialogRef.close(data);
          }
        },
        err => {
          alert('An error occurred');
          console.log(err);
        },
        () => {
          console.log('Community updated');
          this.pendingChange = false;
        }
      );
  }

  deleteCommunityIntent() {
    this.deleteIntent = !this.deleteIntent;
  }

  deleteCommunity() {

    this.pendingChange = true;

    this.communityService.deleteCommunity(this.hash)
      .takeWhile(() => this.alive)
      .subscribe(
        data => {
          this.dialogRef.close();
          this.router.navigate(['']);
        },
        err => {
          alert('An error occurred');
          this.dialogRef.close();
        },
        () => {
          this.pendingChange = false;
        }
      );

  }

  addFile(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.logoBeingUploaded = fileList[0];
    }
  }

}
