import { Component, OnDestroy, OnInit } from '@angular/core';
import { Community } from '../../shared/models/community/community.model';
import { CommunityService } from '../../core/services/community/community.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogConfig } from '@angular/material';
import { CommunityMembership } from '../../shared/models/community/community-membership.model';
import { CommunityMembershipService } from '../../core/services/community/community-membership.service';
import { CommunityCandidacy } from '../../shared/models/community/community-candidacy.model';
import { CommunityInvitation } from '../../shared/models/community/community-invitation.model';
import { CommunityCandidacyService } from '../../core/services/community/community-candidacy.service';
import { CommunityInvitationService } from '../../core/services/community/community-invitation.service';
import { CandidacyDialogComponent } from '../../shared/candidacy/candidacy-dialog/candidacy-dialog.component';
import { ConfirmDialogComponent } from '../../shared/confirm-dialog/confirm-dialog.component';
import { CommunityEditDialogComponent } from './community-edit-dialog.component';
import { CommunityManageTeamDialogComponent } from '../community-manage-team-dialog/community-manage-team-dialog.component';
import { CommunityNotificationPreferencesDialogComponent } from '../community-notification-preferences-dialog/community-notification-preferences-dialog.component';

/**
 * Created by huber on 29/04/2017.
 */
@Component({
    selector: 'app-community',
    templateUrl: 'community-detail.component.html',
    styleUrls: ['community-detail.component.scss']
})
export class CommunityDetailComponent implements OnInit, OnDestroy {

    community: Community;

    memberships: CommunityMembership[] = [];
    userMembership: CommunityMembership;
    membershipsFetched = false;

    candidacies: CommunityCandidacy[] = [];
    userCandidacy: CommunityCandidacy;
    candidaciesFetched = false;

    invitations: CommunityInvitation[] = [];
    userInvitation: CommunityInvitation;
    invitationsFetched = false;

    private alive = true;

    constructor(private communityService: CommunityService,
                private communityMembershipService: CommunityMembershipService,
                private communityCandidacyService: CommunityCandidacyService,
                private communityInvitationService: CommunityInvitationService,
                private route: ActivatedRoute,
                private router: Router,
                public dialog: MatDialog) { }

  ngOnInit() {
    this.fetchData();
  }

  private fetchData() {

    this.clear();

    this.route.params
      .subscribe(
        params => {
          this.clear();

          const hash = params['hash'];
          this.communityService.getCommunity(hash)
            .takeWhile(() => this.alive)
            .subscribe(
              community => {
                this.community = community;
                this.updateMemberships();
              },
              err => {
                console.log(err);
                alert('An error occurred');
              },
              () => {
                console.log('Community Fetched');
              }
            );

          this.communityCandidacyService.getCommunityCandidacies({community: hash})
            .takeWhile(() => this.alive)
            .finally(() => this.candidaciesFetched = true)
            .subscribe(
              candidacies => {
                this.candidacies = candidacies;
                this.userCandidacy = candidacies.find((c: CommunityCandidacy) => c.user.username === localStorage.getItem('username'));
              },
              err => console.log(err)
            );

          this.communityInvitationService.getCommunityInvitations({community: hash})
            .takeWhile(() => this.alive)
            .finally(() => this.invitationsFetched = true)
            .subscribe(
              invitations => {
                this.invitations = invitations;
                this.userInvitation = invitations.find((i: CommunityInvitation) => i.user.username === localStorage.getItem('username'));
              },
              err => console.log(err)
            );
        }
      );
  }

  ngOnDestroy() {
      this.alive = false;
  }

  public updateMemberships() {
    this.communityMembershipService.getCommunityMemberships({community: this.community.hash})
      .takeWhile(() => this.alive)
      .finally(() => this.membershipsFetched = true)
      .subscribe(
        memberships => {
          this.memberships = memberships;
          this.userMembership = memberships.find((s: CommunityMembership) => s.user.username === localStorage.getItem('username'));
        },
        err => console.log(err)
      );
  }

  public handleCandidacyAccepted(candidacy: CommunityCandidacy) {
    this.removeCandidacy(candidacy);
    this.updateMemberships();
  }

  public removeCandidacy(candidacy: CommunityCandidacy) {
    this.candidacies.splice(this.candidacies.findIndex(indexedCandidacy => indexedCandidacy.hash === candidacy.hash), 1);
  }

  private clear() {
    this.community = null;
    this.memberships = [];
    this.userMembership = null;
    this.membershipsFetched = false;
    this.candidacies = [];
    this.userCandidacy = null;
    this.candidaciesFetched = false;
    this.invitations = [];
    this.userInvitation = null;
    this.invitationsFetched = false;
  }

  leaveCommunity() {
    const config = new MatDialogConfig();
    config.data = 'Are you sure you want to leave this community ?';

    const dialogRef = this.dialog.open(ConfirmDialogComponent, config);

    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(result => {
        if (result) {
          this.communityMembershipService.deleteCommunityMembership(this.userMembership.hash)
            .takeWhile(() => this.alive)
            .subscribe(
              data => this.router.navigate(['/communities']),
              err => console.log(err)
            );
        }
      });
  }

  openCandidacyDialog() {
    const config = new MatDialogConfig();
    config.data = {
      community: this.community
    };

    const dialogRef = this.dialog.open(CandidacyDialogComponent, config);

    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(result => {
        if (typeof result !== 'undefined' && result !== null && result !== false) {
          this.userCandidacy = result;
        }
      });
  }

  cancelCandidacy() {
    const config = new MatDialogConfig();
    config.data = 'Are you sure you want to cancel your candidacy ?';

    const dialogRef = this.dialog.open(ConfirmDialogComponent, config);

    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(result => {
        if (result) {
          this.communityCandidacyService.deleteCommunityCandidacy(this.userCandidacy.hash)
            .takeWhile(() => this.alive)
            .subscribe(
              data => this.router.navigate(['/communities']),
              err => console.log(err)
            );
        }
      });
  }

  acceptInvitation() {
    this.communityInvitationService.acceptCommunityInvitation(this.userInvitation.hash)
      .takeWhile(() => this.alive)
      .subscribe(
        data => this.fetchData(),
        err => console.log(err)
      );
  }

  rejectInvitation() {
    const config = new MatDialogConfig();
    config.data = 'Are you sure you want to decline this invitation ?';

    const dialogRef = this.dialog.open(ConfirmDialogComponent, config);

    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(result => {
        if (result) {
          this.communityInvitationService.deleteCommunityInvitation(this.userInvitation.hash)
            .takeWhile(() => this.alive)
            .subscribe(
              data => this.router.navigate(['/communities']),
              err => console.log(err)
            );
        }
      });
  }

  canEdit(): boolean {
      return this.canUpdateMemberships() || this.canUpdateCommunity() || this.canUpdateRoles();
  }

  hasPermission(permission: string): boolean {
    return (this.userMembership !== undefined
      && this.userMembership.role !== undefined
      && this.userMembership.role.permissions.indexOf(permission) > -1);
  }

  canUpdateCommunity(): boolean {
    return this.hasPermission('UPDATE_COMMUNITY');
  }

  canUpdateMemberships(): boolean {
    return this.hasPermission('MANAGE_MEMBERSHIPS') || this.hasPermission('UPDATE_MEMBERSHIP');
  }

  canManageCandidacies(): boolean {
    return this.hasPermission('MANAGE_CANDIDACIES');
  }

  canUpdateRoles(): boolean {
    return this.hasPermission('MANAGE_ROLES');
  }

  changeBackgroundPicture(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      const file: File = fileList[0];

      this.communityService.postBackgroundImage(this.community.hash, file)
        .takeWhile(() => this.alive)
        .subscribe(
          data => {
            this.community = data;
          },
          error => {
            alert('The image could not be uploaded. It must not be over 500KB.');
          }
        );
    }
  }

  openEditMode() {
    const config = new MatDialogConfig();
    config.data = this.community;

    const dialogRef = this.dialog.open(CommunityEditDialogComponent, config);

    dialogRef.afterClosed()
      .takeWhile(() => this.alive)
      .subscribe(result => {
        if (typeof result !== 'undefined' && result !== null && result !== false) {
          this.community = result;
        }
      });
  }

  openTeamManagementDialog() {
    const config = new MatDialogConfig();

    config.data = {
      memberships: this.memberships,
      invitations: this.invitations,
      candidacies: this.candidacies,
      community: this.community
    };

    const dialogRef = this.dialog.open(CommunityManageTeamDialogComponent, config);

    dialogRef.afterClosed()
      .finally(() => 'The team management dialog was closed');
  }

  openPreferencesDialog() {
    const config = new MatDialogConfig();
    config.data = this.userMembership;

    const dialogRef = this.dialog.open(CommunityNotificationPreferencesDialogComponent, config);
  }

}
