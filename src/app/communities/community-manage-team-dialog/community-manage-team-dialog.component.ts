import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {User} from '../../shared/models/users/user.model';
import {CommunityInvitationDto} from '../../shared/models/community/community-invitation.model.dto';
import {CommunityInvitation} from '../../shared/models/community/community-invitation.model';
import {CommunityCandidacy} from '../../shared/models/community/community-candidacy.model';
import {CommunityMembership} from '../../shared/models/community/community-membership.model';
import {CommunityRole} from '../../shared/models/community/community-role.model';
import {Community} from '../../shared/models/community/community.model';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {UserService} from '../../core/services/user.service';
import {CommunityMembershipService} from '../../core/services/community/community-membership.service';
import {CommunityRoleService} from '../../core/services/community/community-role.service';
import {CommunityInvitationService} from '../../core/services/community/community-invitation.service';
import {CommunityCandidacyService} from '../../core/services/community/community-candidacy.service';
import {CommunityMembershipDto} from '../shared/community-membership.model.dto';


@Component({
    moduleId: module.id,
    selector: 'app-community-manage-team-dialog',
    templateUrl: 'community-manage-team-dialog.component.html',
    styleUrls: ['community-manage-team-dialog.component.scss']
})
export class CommunityManageTeamDialogComponent implements OnInit, OnDestroy {

  /*
   * Invite users
   */
  userCtrl: FormControl;
  filteredUsers: User[] = [];
  invitation: CommunityInvitationDto = new CommunityInvitationDto();
  invitations: CommunityInvitation[] = [];

  /*
   * Manage candidacies
   */
  candidacies: CommunityCandidacy[] = [];

  /*
   * Manage memberships
   */
  memberships: CommunityMembership[] = [];
  membershipToEdit: CommunityMembership;
  membershipNewRole: CommunityRole;

  /*
   * Manage roles
   */
  roles: CommunityRole[] = [];
  rolesFetched = false;
  selectedRole: CommunityRole;

  /*
   * Manage community
   */
  community: Community;

  private alive = true;

  constructor(public dialogRef: MatDialogRef<CommunityManageTeamDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private userService: UserService,
              private communityMembershipService: CommunityMembershipService,
              private communityRoleService: CommunityRoleService,
              private communityInvitationService: CommunityInvitationService,
              private communityCandidacyService: CommunityCandidacyService) {

    /*
     * Fill current attributes with passed value
     */
    data.memberships !== null ? this.memberships = data.memberships : this.memberships = [];
    data.candidacies !== null ? this.candidacies = data.candidacies : this.candidacies = [];
    data.invitations !== null ? this.invitations = data.invitations : this.invitations = [];
    this.community = data.community;

    this.membershipToEdit = new CommunityMembership();
    this.invitation = new CommunityInvitationDto();

    this.userCtrl = new FormControl();
    this.userCtrl.valueChanges
      .debounceTime(300)
      .distinctUntilChanged()
      .takeWhile(() => this.alive)
      .subscribe(
        name => {
          this.filteredUsers = [];
          if (name != null && name.length >= 2) {
            this.userService.getUsers({name: name})
              .takeWhile(() => this.alive)
              .subscribe(
                users => {
                  this.filteredUsers = users;
                  },
                err => console.log(err)
              );
          }
        }
      );
  }

  ngOnInit() {
    this.communityRoleService.getCommunityRoles(this.community.hash)
      .takeWhile(() => this.alive)
      .subscribe(
        roles => {
          this.roles = roles;
          this.selectedRole = roles[0];
        },
        err => console.log(err),
        () => this.rolesFetched = true
      );
  }

  ngOnDestroy() {
    this.alive = false;
  }

  /*
   * Membership management
   */

  editMembership(membership: CommunityMembership): void {
    if (membership !== this.membershipToEdit) {
      this.membershipNewRole = this.roles.find(r => r.hash === membership.role.hash);
      this.membershipToEdit = Object.assign({}, membership);
    }
  }

  saveMembershipChanges(): void {
    const updatedMembership = new CommunityMembershipDto();
    updatedMembership.role = this.membershipNewRole.hash;
    this.communityMembershipService.putCommunityMembership(this.membershipToEdit.hash, updatedMembership)
      .takeWhile(() => this.alive)
      .subscribe(
        data => {
          this.memberships.find(m => m.hash === data.hash).role = (data.role);
          this.membershipToEdit = new CommunityMembership();
        }
      );
  }

  abortMembershipChange(): void {
    this.membershipToEdit = new CommunityMembership();
  }

  /*
   * Invitation management
   */

  isInvited(user): boolean {
    return (this.invitations.find(i => i.user.username === user.username) !== undefined);
  }

  isMember(user): boolean {
    return  (this.memberships.find(m => m.user.username === user.username) !== undefined);
  }

  isCandidate(user): boolean {
    return (this.candidacies.find(c => c.user.username === user.username) !== undefined);
  }

  canInvite(user: User): boolean {
    return !(this.isInvited(user) || this.isMember(user) || this.isCandidate(user));
  }

  triggerInvitation(user: User): void {
    this.invitation = new CommunityInvitationDto();
    this.invitation.user = user.username;
  }

  sendInvitation(): void {
    this.communityInvitationService.postCommunityInvitation(this.community.hash, this.invitation)
      .takeWhile(() => this.alive)
      .subscribe(
        data => {
          this.invitations.push(data);
          this.invitation = new CommunityInvitationDto();
        },
        err => console.log(err)
      );
  }

  removeInvitation(invitation: CommunityInvitation): void {
    this.communityInvitationService.deleteCommunityInvitation(invitation.hash)
      .takeWhile(() => this.alive)
      .subscribe(
        result => this.invitations.splice(this.invitations.indexOf(invitation), 1),
        err => console.log(err)
      );
  }

  /*
   * Candidacy management
   */
  candidacyAccepted(candidacy: CommunityCandidacy): void {
    this.removeCandidacy(candidacy);
    this.communityMembershipService.getCommunityMemberships({ community: this.community.hash})
      .takeWhile(() => this.alive)
      .subscribe(
        data => this.memberships = data,
        err => console.error(err)
      );
  }

  removeCandidacy(candidacy: CommunityCandidacy): void {
    this.candidacies.splice(this.candidacies.indexOf(candidacy), 1);
  }

  /*
   * Role management
   */

  selectRole(role: CommunityRole): void {
    this.selectedRole = role;
  }

  newRole(): void {
    this.selectedRole = new CommunityRole();
  }

  updateRole(role: CommunityRole): void {
    if (this.roles.find(r => r.hash === role.hash)) {
      this.roles.find(r => r.hash === role.hash).permissions = role.permissions;
      this.roles.find(r => r.hash === role.hash).defaultMember = role.defaultMember;
    } else {
      this.roles.push(role);
    }
  }

}
