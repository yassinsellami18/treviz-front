import {throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Skill } from '../../shared/models/tags/skill.model';

@Injectable()
export class SkillService {

  skill_url = environment.api_url + '/skills';

  constructor(public http: HttpClient) {}

  getSkillsHttp(): Observable<Skill[]> {

    return this.http.get<Skill[]>(this.skill_url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }


  postSkill(name: string): Observable<Skill> {

    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    const body = 'name=' + name;

    return this.http.post<Skill>(this.skill_url, body, { headers: headers })
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));

  }

}
