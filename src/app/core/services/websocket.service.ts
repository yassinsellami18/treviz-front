import {Injectable} from '@angular/core';
import * as Rx from 'rxjs';

/**
 * Created by huber on 29/07/2017.
 */
@Injectable()
export class WebsocketService {

  private socket: Rx.Subject<MessageEvent>;
  private ws: WebSocket;

  public connect(url): Rx.Subject<MessageEvent> {

    if (!this.socket) {
      this.socket = this.create(url);
    }
    return this.socket;
  }

  private create(url): Rx.Subject<MessageEvent> {
    this.ws = new WebSocket(url);

    const observable = Rx.Observable.create(
      (obs: Rx.Observer<MessageEvent>) => {
        this.ws.onmessage = obs.next.bind(obs);
        this.ws.onerror = obs.error.bind(obs);
        this.ws.onclose = obs.complete.bind(obs);
        return this.ws.close.bind(this.ws);
      }
    );
    const observer = {
      next: (data: Object) => {
        if (this.ws.readyState === WebSocket.OPEN) {
          this.ws.send(JSON.stringify(data));
        }
      },
    };

    return Rx.Subject.create(observer, observable);
  }

  public close() {
    if (this.socket) {
      this.ws.close();
      this.socket.unsubscribe();
      this.socket = null;
    }
  }

}
