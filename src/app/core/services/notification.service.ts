import {throwError as observableThrowError,  Observable ,  Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { WebsocketService } from './websocket.service';
import { Notification } from '../../shared/models/notification.model';
import { HttpClient } from '@angular/common/http';
import { CurrentUserService } from './current-user.service';
import { Room } from '../../shared/models/chat/room.model';
import 'rxjs/add/operator/share';

/**
 * Created by huber on 05/08/2017.
 */
@Injectable()
export class NotificationService {

  private NOTIFICATIONS_WS_URL = environment.notifications_socket_url;
  private NOTIFICATIONS_API_URL = environment.api_url + '/notifications';

  private notifications: Subject<Notification>;
  private notificationSharedObservable: Observable<Notification>;

  listening = false;

  public pendingNotifications = {
    chat: 0,
    unlinkedChat: 0,
    projects: 0,
    jobs: 0,
    communities: 0,
    feed: 0
  };

  constructor(private http: HttpClient,
              private webSocketService: WebsocketService,
              private currentUserService: CurrentUserService) {
    this.notifications = new Subject<Notification>();
  }

  subscribeToNotificationService(): Observable<Notification> {
    return this.notificationSharedObservable;
  }

  sendNotification(notification: Notification) {
    this.notifications.next(notification);
  }

  getNotifications(): Observable<any> {
    return this.http.get(this.NOTIFICATIONS_API_URL)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  connectToNotificationWs(ticket: string): void {
    this.notifications = <Subject<Notification>>this.webSocketService
      .connect(this.NOTIFICATIONS_WS_URL + '?ticket=' + ticket)
      .map((response: MessageEvent): Notification => {
        const data = JSON.parse(response.data);
        return {
          type: data.type,
          content: data.content
        };
      });
  }

  disconnectToNotificationWs(): void {
    this.listening = false;
    this.webSocketService.close();
    this.pendingNotifications = {
      chat: 0,
      unlinkedChat: 0,
      projects: 0,
      jobs: 0,
      communities: 0,
      feed: 0
    };
  }

  listenToNotifications() {

    this.listening = true;

    this.getNotifications()
      .subscribe(
        data => {

          if (data.notifications) {
            for (const notification of data.notifications) {
              this.parseNotification(notification);
            }
          }

          this.connectToNotificationWs(data.ticket);
          this.notificationSharedObservable = this.notifications.asObservable().share();
          this.subscribeToNotificationService()
            .subscribe(msg => {
              this.parseNotification(msg);
            });
        }
      );
  }

  parseNotification(msg: any): void {
    if (msg.type === 'CHAT_POST_MESSAGE') {

      /*
       * On chat message reception, add the message to the corresponding chat room, and set it to active
       */
      const chatRoom = this.currentUserService.getCurrentUser().rooms
        .find(room => room.hash === msg.content.room_hash);
      if (chatRoom) {
        if (chatRoom.messages) {
          chatRoom.messages.push(msg.content);
        } else {
          chatRoom.messages = [msg.content];
        }

        /*
         * Then add a "pending notification" to its project or community.
         */
        this.addNotificationToChatroom(chatRoom);

      }

    } else if (msg.type === 'CHAT_POST_ROOM' || msg.type === 'CHAT_INVITE_USER') {

      /*
       * On new message reception, adds the chat room to the list of the user's
       */
      const room = msg.content;
      if (this.currentUserService.getCurrentUser().rooms) {
        this.currentUserService.getCurrentUser().rooms.push(room);
      } else {
        this.currentUserService.getCurrentUser().rooms = [room];
      }

      /*
       * Then sets the notification for the user to see.
       */
      this.addNotificationToChatroom(room);

    }
  }

  /*
   * Increments the pending notification counter of the room, project or community it is linked to, and general chat
   * indicator.
   */
  private addNotificationToChatroom(room: Room): void {
    room.active = true;
    if (room.project) {
      const membership = this.currentUserService.getCurrentUser().projectMemberships
        .find(indexedMembership => indexedMembership.project.hash === room.project.hash);
      /*
       * The project memberships should have been fetched, but an error could have occurred.
       */
      if (membership) {
        if (membership.project.pendingNotification) {
          membership.project.pendingNotification++;
        } else {
          membership.project.pendingNotification = 1;
        }
      }
    } else if (room.community) {
      /*
       * The community memberships should have been fetched, but an error could have occurred.
       */
      const membership = this.currentUserService.getCurrentUser().communityMemberships
        .find(indexedMembership => indexedMembership.community.hash === room.community.hash);
      if (membership) {
        membership.community
          .pendingNotification++;
      }
    } else {
      if (this.pendingNotifications.unlinkedChat) {
        this.pendingNotifications.unlinkedChat++;
      } else {
        this.pendingNotifications.unlinkedChat = 1;
      }
    }

    if (this.pendingNotifications.chat) {
      this.pendingNotifications.chat++;
    } else {
      this.pendingNotifications.chat = 1;
    }

  }

}
