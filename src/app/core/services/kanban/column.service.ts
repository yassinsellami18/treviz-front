import {throwError as observableThrowError, Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Column} from '../../../shared/models/kanban/column.model';
import {ColumnDto} from '../../../shared/models/kanban/column.model.dto';
import {environment} from 'environments/environment';


@Injectable()
export class ColumnService {

  private columnsUrl = environment.api_url + '/boards/';

  constructor(private http: HttpClient) {}

  /**
   * Fetches the columns of a board.
   * @param {string} board Hash of the board from which fetch columns
   * @returns {Observable<Column[]>}
   */
  getColumns(board: string): Observable<Column[]> {
    const url = this.columnsUrl + board + '/columns';
    return this.http.get<Column[]>(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Fetches a specific column according to its hash.
   * @param {string} column Hash of the column to fetch
   * @returns {Observable<Column>}
   */
  getColumn(column: string): Observable<Column> {
    const url = this.columnsUrl + 'columns/' + column;
    return this.http.get<Column>(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Creates a new column
   * @param board Hash of the board to which create the column
   * @param {ColumnDto} column Column to create
   * @returns {Observable<Column>}
   */
  postColumn(board: string, column: ColumnDto): Observable<Column> {
    const url = this.columnsUrl + board + '/columns';
    return this.http.post<Column>(url, column)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Updates an existing column.
   * @param {string} column Hash of the column to update
   * @param {ColumnDto} updatedColumn JSON containing the updated column
   * @returns {Observable<Column>}
   */
  putColumn(column: string, updatedColumn: ColumnDto): Observable<Column> {
    const url = this.columnsUrl + 'columns/' + column;
    return this.http.post<Column>(url, updatedColumn)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Deletes an existing column.
   * @param {string} column Hash of the column to delete
   * @returns {Observable<any>}
   */
  deleteColumn(column: string): Observable<any> {
    const url = this.columnsUrl + 'columns/' + column;
    return this.http.delete(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }
}
