import {throwError as observableThrowError, Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Board} from '../../../shared/models/kanban/board.model';
import {BoardDto} from '../../../shared/models/kanban/board.model.dto';
import { HttpParams } from '@angular/common/http';
import {environment} from 'environments/environment';

@Injectable()
export class BoardService {

  private boardsUrl = environment.api_url + '/boards';

  constructor(private http: HttpClient) {}

  /**
   * Fetches kanban boards according to a specific query.
   *
   * @param {string} project
   * @param {string} user
   * @param {boolean} archived
   * @returns {Observable<Board[]>}
   */
  getBoards({project = '', user = '', archived = false}: {project?: string, user?: string, archived?: boolean}): Observable<Board[]> {
    let params = new HttpParams().set('archived', archived.toString());
    if (project !== '') { params = params.set('project', project); }
    if (user !== '') { params = params.set('user', user); }

    return this.http.get<Board[]>(this.boardsUrl, { params: params })
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Fetches a specific board according to its hash.
   * @param {string} board Hash of the board to fetch
   * @returns {Observable<Board>}
   */
  getBoard(board: string): Observable<Board> {
    return this.http.get<Board>(this.boardsUrl + '/' + board)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Creates a new board
   * @param {BoardDto} board Board to create
   * @returns {Observable<Board>}
   */
  postBoard(board: BoardDto): Observable<Board> {
    return this.http.post<Board>(this.boardsUrl, board)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Updates an existing board.
   * @param {string} board Hash of the board to update
   * @param {BoardDto} updatedBoard JSON containing the updated board
   * @returns {Observable<Board>}
   */
  putBoard(board: string, updatedBoard: BoardDto): Observable<Board> {
    const url = this.boardsUrl + '/' + board;

    return this.http.post<Board>(url, updatedBoard)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Deletes an existing board.
   * @param {string} board Hash of the board to delete
   * @returns {Observable<any>}
   */
  deleteBoard(board: string): Observable<any> {
    return this.http.delete(this.boardsUrl + '/' + board)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }
}
