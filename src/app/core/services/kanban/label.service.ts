import {throwError as observableThrowError, Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Label} from '../../../shared/models/kanban/label.model';
import {LabelDto} from '../../../shared/models/kanban/label.model.dto';
import {environment} from 'environments/environment';


@Injectable()
export class LabelService {

  private labelsUrl = environment.api_url + '/boards/';

  constructor(private http: HttpClient) {}

  /**
   * Fetches the labels of a board.
   * @param {string} board Hash of the board from which fetch labels
   * @returns {Observable<Label[]>}
   */
  getLabels(board: string): Observable<Label[]> {
    const url = this.labelsUrl + board + '/labels';
    return this.http.get<Label[]>(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Fetches a specific label according to its hash.
   * @param {string} label Hash of the label to fetch
   * @returns {Observable<Label>}
   */
  getLabel(label: string): Observable<Label> {
    const url = this.labelsUrl + 'labels/' + label;
    return this.http.get<Label>(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Creates a new label
   * @param board Hash of the board to which create the label
   * @param {LabelDto} label Label to create
   * @returns {Observable<Label>}
   */
  postLabel(board: string, label: LabelDto): Observable<Label> {
    const url = this.labelsUrl + board + '/labels';
    return this.http.post<Label>(url, label)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Updates an existing label.
   * @param {string} label hash of the label to update
   * @param {LabelDto} updatedLabel Updated label
   * @returns {Observable<Label>}
   */
  putLabel(label: string, updatedLabel: LabelDto): Observable<Label> {
    const url = this.labelsUrl + 'labels/' + label;
    return this.http.put<Label>(url, updatedLabel)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Deletes an existing label.
   * @param {string} label Hash of the label to delete
   * @returns {Observable<any>}
   */
  deleteLabel(label: string): Observable<any> {
    const url = this.labelsUrl + 'labels/' + label;
    return this.http.delete(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }
}
