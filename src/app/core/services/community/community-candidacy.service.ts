import {throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommunityCandidacy } from '../../../shared/models/community/community-candidacy.model';
import { HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';
import { CommunityCandidacyDto } from '../../../shared/models/community/community-candidacy.model.dto';

@Injectable()
export class CommunityCandidacyService {

  constructor(private http: HttpClient) {}

  /**
   * Fetches the candidacy of a community or user.
   *
   * @param {string} community
   * @param {string} user
   * @returns {Observable<CommunityCandidacy[]>}
   */
  public getCommunityCandidacies({community = '', user = ''}: { community?: string; user?: string}): Observable<CommunityCandidacy[]> {

    let url = environment.api_url + '/communities/';

    /*
     * If a community hash is specified, url is <api-url>/communities/:hash/memberships
     * Otherwise, it is just <api-url>/communities/memberships
     */
    community !== '' ? (url += community + '/candidacies') : ( url += 'candidacies');

    /*
     * If a username is fetch, add query param to fetch only this user's memberships.
     */
    let params = new HttpParams();
    if (user !== '') {
      params = params.set('user', user);
    }

    return this.http.get<CommunityCandidacy[]>(url, { params: params })
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Candidate to an existing community.
   *
   * @param {string} community Hash of the community to which invite the user
   * @param {CommunityCandidacyDto} candidacy JSON Body containing the username of the user to invite and a message
   * @returns {Observable<CommunityCandidacy>}
   */
  public postCommunityCandidacy(community: string, candidacy: CommunityCandidacyDto): Observable<CommunityCandidacy> {

    const url = environment.api_url + '/communities/' + community + '/candidacies';

    return this.http.post<CommunityCandidacy>(url, candidacy)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Deletes an existing candidacy.
   *
   * @param {string} candidacy Hash of the candidacy to delete
   * @returns {Observable<any>}
   */
  public deleteCommunityCandidacy(candidacy: string): Observable<any> {
    const url = environment.api_url + '/communities/candidacies/' + candidacy;

    return this.http.delete(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Accepts an candidacy to a community.
   *
   * @param {string} candidacy
   * @returns {Observable<any>}
   */
  public acceptCommunityCandidacy(candidacy: string): Observable<any> {
    const url = environment.api_url + '/communities/candidacies/' + candidacy + '/accept';

    return this.http.post(url, candidacy)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }
}
