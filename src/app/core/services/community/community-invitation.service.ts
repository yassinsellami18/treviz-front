import {throwError as observableThrowError, Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {CommunityInvitation} from '../../../shared/models/community/community-invitation.model';
import { HttpParams } from '@angular/common/http';
import {environment} from 'environments/environment';
import {CommunityInvitationDto} from '../../../shared/models/community/community-invitation.model.dto';

@Injectable()
export class CommunityInvitationService {

  constructor(private http: HttpClient) {}

  /**
   * Fetches the invitation of a community or user.
   *
   * @param {string} community
   * @param {string} user
   * @returns {Observable<CommunityInvitation[]>}
   */
  public getCommunityInvitations({community = '', user = ''}: { community?: string; user?: string}): Observable<CommunityInvitation[]> {

    let url = environment.api_url + '/communities/';

    /*
     * If a community hash is specified, url is <api-url>/communities/:hash/memberships
     * Otherwise, it is just <api-url>/communities/memberships
     */
    community !== '' ? (url += community + '/invitations') : ( url += 'invitations');

    /*
     * If a username is fetch, add query param to fetch only this user's memberships.
     */
    let params = new HttpParams();
    if (user !== '') {
      params = params.set('user', user);
    }

    return this.http.get<CommunityInvitation[]>(url, { params: params })
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Invite a user to an existing community.
   *
   * @param {string} community Hash of the community to which invite the user
   * @param {CommunityInvitationDto} invitation JSON Body containing the username of the user to invite and a message
   * @returns {Observable<CommunityInvitation>}
   */
  public postCommunityInvitation(community: string, invitation: CommunityInvitationDto): Observable<CommunityInvitation> {

    const url = environment.api_url + '/communities/' + community + '/invitations';

    return this.http.post<CommunityInvitation>(url, invitation)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Deletes an existing invitation.
   *
   * @param {string} invitation Hash of the invitation to delete
   * @returns {Observable<any>}
   */
  public deleteCommunityInvitation(invitation: string): Observable<any> {
    const url = environment.api_url + '/communities/invitations' + invitation;

    return this.http.post(url, invitation)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Accepts an invitation to a community.
   *
   * @param {string} invitation
   * @returns {Observable<any>}
   */
  public acceptCommunityInvitation(invitation: string): Observable<any> {
    const url = environment.api_url + '/communities/invitations' + invitation + '/accept';

    return this.http.post(url, invitation)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }
}
