import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CommunityNotificationPreferences } from '../../../shared/models/community/community-notification-preferences.model';

@Injectable()
export class CommunityPreferencesService {

  constructor(private http: HttpClient) {}

  getCommunityPreferences(membership: string): Observable<CommunityNotificationPreferences> {
    const url = `${environment.api_url}/communities/memberships/${membership}/preferences`;
    return this.http.get<CommunityNotificationPreferences>(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  updatePreferences(membership: string, preferences: CommunityNotificationPreferences): Observable<CommunityNotificationPreferences> {
    const url = `${environment.api_url}/communities/memberships/${membership}/preferences`;
    return this.http.put<CommunityNotificationPreferences>(url, preferences)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

}
