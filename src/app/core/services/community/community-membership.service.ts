import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { CommunityMembershipDto } from '../../../communities/shared/community-membership.model.dto';
import { CommunityMembership } from '../../../shared/models/community/community-membership.model';

@Injectable()
export class CommunityMembershipService {

  constructor(private http: HttpClient) {}

  /**
   * Fetches the community memberships of a community or user.
   *
   * @param {string} community Hash of the community from which fetch memberships
   * @param {string} user Username of the user from whom fetch memberships
   * @returns {Observable<CommunityMembership[]>}
   */
  getCommunityMemberships( {community = '', user = ''}: {community?: string, user?: string} ): Observable<CommunityMembership[]> {
    let url = environment.api_url + '/communities/';

    /*
     * If a community hash is specified, url is <api-url>/communities/:hash/memberships
     * Otherwise, it is just <api-url>/communities/memberships
     */
    community !== '' ? (url += community + '/memberships') : ( url += 'memberships');

    /*
     * If a username is fetch, add query param to fetch only this user's memberships.
     */
    let params = new HttpParams();
    if (user !== '') {
      params = params.set('user', user);
    }

    return this.http.get<CommunityMembership[]>(url, { params: params })
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Creates a new memberships for a specified community.
   *
   * @param {string} communityHash Hash of the community to which create a membership
   * @param communityMembership Membership to create
   * @returns {Observable<CommunityMembership>}
   */
  postCommunityMembership(communityHash: string, communityMembership: CommunityMembershipDto): Observable<CommunityMembership> {

    const url = environment.api_url + '/communities/' + communityHash + '/memberships';

    return this.http.post<CommunityMembership>(url, communityMembership)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Updates an existing membership.
   *
   * @param membershipHash Hash of the membership to update
   * @param updatedCommunityMembership Update membership
   * @returns {Observable<CommunityMembership>}
   */
  putCommunityMembership(membershipHash, updatedCommunityMembership: CommunityMembershipDto): Observable<CommunityMembership> {
    const url = environment.api_url + '/communities/memberships/' + membershipHash;

    return this.http.put<CommunityMembership>(url, updatedCommunityMembership)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Deletes an existing membership.
   *
   * @param membershipHash Hash of the membership to delete
   * @returns {Observable<any>}
   */
  deleteCommunityMembership(membershipHash): Observable<any> {
    const url = environment.api_url + '/communities/memberships/' + membershipHash;

    return this.http.delete(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

}
