import {throwError as observableThrowError, Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from 'environments/environment';
import {CommunityRole} from '../../../shared/models/community/community-role.model';

@Injectable()
export class CommunityRoleService {

  constructor(private http: HttpClient) {}

  /**
   * Fetches the roles of a community.
   *
   * @param {string} community Hash of the community from which fetch roles
   * @returns {Observable<CommunityRole[]>}
   */
  public getCommunityRoles(community: string): Observable<CommunityRole[]> {

    const url = environment.api_url + '/communities/' + community + '/roles';

    return this.http.get<CommunityRole[]>(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Invite a user to an existing community.
   *
   * @param {string} community Hash of the community in which the role should be added
   * @param role Role to create for the community
   * @returns {Observable<CommunityRole>}
   */
  public postCommunityRole(community: string, role: CommunityRole): Observable<CommunityRole> {

    const url = environment.api_url + '/communities/' + community + '/roles';

    return this.http.post<CommunityRole>(url, role)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Invite a user to an existing community.
   *
   * @param {string} role Hash of the role to update
   * @param updatedRole Role to update
   * @returns {Observable<CommunityRole>}
   */
  public putCommunityRole(role: string, updatedRole: CommunityRole): Observable<CommunityRole> {

    const url = environment.api_url + '/communities/roles/' + role;

    return this.http.put<CommunityRole>(url, updatedRole)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Deletes a role
   *
   * @param {string} role
   * @returns {Observable<any>}
   */
  public deleteCommunityRole(role: string): Observable<any> {
    const url = environment.api_url + '/communities/roles/' + role;

    return this.http.delete(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }
}
