import { Injectable } from '@angular/core';
import { UserService } from './user.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from '../../shared/models/users/user.model';
import { ProjectMembershipService } from './project/project-membership.service';
import { CommunityMembershipService } from './community/community-membership.service';
import { ChatRoomService } from './chat/chat-room.service';
import {Subject, BehaviorSubject} from "rxjs";

/**
 * Created by Bastien on 19/03/2017.
 */

@Injectable()
export class CurrentUserService {

  private currentUser: User = null;

  private state = {
    loadingUser: true,
    loadingProjects: true,
    loadingCommunities: true,
    loadingRooms: true
  };

  /*
   * Subject matching the state of the user loading.
   */
  public loading = new BehaviorSubject(this.state);

  constructor(private userService: UserService,
              private projectMembershipService: ProjectMembershipService,
              private communityMembershipService: CommunityMembershipService,
              private chatRoomService: ChatRoomService,
              private jwtHelper: JwtHelperService) {
    if ( localStorage.getItem('username') ) {
      this.currentUser = new User();
      this.currentUser.username = localStorage.getItem('username');
    }

  }

  getCurrentUser() {
    return this.currentUser;
  }

  loadCurrentUser() {

    const token = localStorage.getItem('access_token');

    if (!this.jwtHelper.isTokenExpired(token)) {
      const username = localStorage.getItem('username');
      this.currentUser = new User();
      this.currentUser.username =  localStorage.getItem('username');

      /*
       * Fetch current user.
       */
      this.userService.getUser(username)
        .takeWhile(() => this.state.loadingUser)
        .finally(() => {
          this.state.loadingUser = false;
          this.loading.next(this.state);
        })
        .subscribe(
          user => {
            this.currentUser.firstName = user.firstName;
            this.currentUser.lastName = user.lastName;
            this.currentUser.description = user.description;
            this.currentUser.avatarUrl = user.avatarUrl;
            this.currentUser.backgroundImageUrl = user.backgroundImageUrl;
            this.currentUser.skills = user.skills;
            this.currentUser.interests = user.interests;
          },
          err => console.log(err)
        );

      /*
       * Fetch current user project memberships
       */
      this.projectMembershipService.getProjectMemberships({user: username})
        .finally(() => {
          this.state.loadingProjects = false;
          this.loading.next(this.state);
        })
        .takeWhile(() => this.state.loadingProjects)
        .subscribe(
          projectMemberships => this.currentUser.projectMemberships = projectMemberships,
          err => console.log(err)
        );

      /*
       * Fetch current user community memberships.
       */
      this.communityMembershipService.getCommunityMemberships({user: username})
        .finally(() => {
          this.state.loadingCommunities = false;
          this.loading.next(this.state);
        })
        .takeWhile(() => this.state.loadingCommunities)
        .subscribe(
          communityMemberships => this.currentUser.communityMemberships = communityMemberships,
          err => console.log(err)
        );

      /*
       * Fetch the chat rooms in which the user takes part
       */
      this.chatRoomService.getRooms({})
        .takeWhile(() => this.state.loadingRooms)
        .finally(() => {
          this.state.loadingRooms = false;
          this.loading.next(this.state);
        })
        .subscribe(
          rooms => this.currentUser.rooms = rooms,
          err => console.log(err)
        );

    }
  }
/*

  isOrganizationAdmin(): boolean {
    const jwtHelper = new JwtHelper();
    try {
      return jwtHelper.decodeToken(localStorage.getItem('access_token')).roles.some(role => role === 'ROLE_USER');
    } catch (e) {
      return false;
    }
  }
*/

  clear() {
    this.currentUser = null;
    this.state = {
      loadingUser: true,
      loadingProjects: true,
      loadingCommunities: true,
      loadingRooms: true
    };
  }

}
