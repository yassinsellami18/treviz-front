import { throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment } from 'environments/environment';
import { User } from '../../../shared/models/users/user.model';


@Injectable()
export class AuthService {

  usersUrl = environment.api_url + '/users';
  login_check_url = environment.api_url + '/login_check';
  resetPasswordUrl = environment.api_url + '/reset-password';

  constructor(private http: HttpClient,
              private jwtHelper: JwtHelperService) {}

  /**
   * Sends the credentials of a user, retrieves an observable containing the access_token.
   *
   * @param {string} username
   * @param {string} password
   * @returns {Observable<any>}
   */
  login(username: string, password: string): Observable<any> {

    const body = 'username=' + username + '&password=' + password;
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    return this.http.post<any>(this.login_check_url, body, { headers: headers });
  }

  loggedIn() {
    const token = localStorage.getItem('access_token');
    return !this.jwtHelper.isTokenExpired(token);
  }

  /**
   * Sends new user information for him or her to register.
   *
   * @param {User} user
   * @returns {Observable<any>}
   */
  register(user: User) {

    let body = new HttpParams();
    body = body.set('firstName', user.firstName);
    body = body.set('lastName', user.lastName);
    body = body.set('username', user.username);
    body = body.set('email', user.email);
    body = body.set('password', user.password);

    return this.http.post(this.usersUrl, body)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Sends the confirmation token in order to activate the user's account
   *
   * @param {string} user
   * @param {string} token
   * @returns {Observable<any>}
   */
  confirmUser(user: string, token: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    const body = 'token=' + token;
    const url = this.usersUrl + '/' + user + '/confirm';

    return this.http.post(url, body, { headers: headers })
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Sends a request to reset one user's password.
   *
   * @param {string} email
   * @returns {Observable<any>}
   */
  sendResetRequest(email: string) {
    const params: HttpParams = new HttpParams().set('email', email);

    return this.http.get(this.resetPasswordUrl, { params: params })
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Sends the new password of a user, alongside with the token allowing him or her to be authenticated.
   *
   * @param {string} user
   * @param {string} token
   * @param {string} password
   * @returns {Observable<any>}
   */
  sendResetPassword(user: string, token: string, password: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    const body = 'token=' + token + '&password=' + password;
    const url = this.usersUrl + '/' + user + '/reset';

    return this.http.post(url, body, { headers: headers })
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

}
