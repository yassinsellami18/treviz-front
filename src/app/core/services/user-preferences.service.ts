import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { UserPreferences } from '../../shared/models/users/user-preferences.model';

@Injectable()
export class UserPreferencesService {

    constructor(private http: HttpClient) {}

    getUserPreferences(username: string): Observable<UserPreferences> {
      const url = `${environment.api_url}/users/${username}/preferences`;
      return this.http.get<UserPreferences>(url)
        .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
    }

    updatePreferences(username: string, preferences: UserPreferences): Observable<UserPreferences> {
      const url = `${environment.api_url}/users/${username}/preferences`;
      return this.http.put<UserPreferences>(url, preferences)
        .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
    }

}
