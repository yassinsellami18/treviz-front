import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { ProjectNotificationPreferences } from '../../../shared/models/project/project-notification-preferences.model';

@Injectable()
export class ProjectPreferencesService {

  constructor(private http: HttpClient) {}

  getProjectPreferences(membership: string): Observable<ProjectNotificationPreferences> {
    const url = `${environment.api_url}/projects/memberships/${membership}/preferences`;
    return this.http.get<ProjectNotificationPreferences>(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  updatePreferences(membership: string, preferences: ProjectNotificationPreferences): Observable<ProjectNotificationPreferences> {
    const url = `${environment.api_url}/projects/memberships/${membership}/preferences`;
    return this.http.put<ProjectNotificationPreferences>(url, preferences)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

}
