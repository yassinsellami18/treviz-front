import { throwError as observableThrowError, Observable } from 'rxjs';
import { Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';
import { ProjectJob } from '../../../shared/models/project/project-job.model';
import { ProjectJobDto } from '../../../shared/models/project/project-job.model.dto';

@Injectable()
export class ProjectJobService {

  constructor(private http: HttpClient) {}

  /**
   * Fetches the jobs of a project.
   *
   * @param {string} project Hash of the project from which fetch jobs
   * @returns {Observable<ProjectJob[]>}
   */
  public getProjectJobs(project: string): Observable<ProjectJob[]> {

    const url = environment.api_url + '/projects/' + project + '/jobs';

    return this.http.get<ProjectJob[]>(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Fetches jobs according to a complex query.
   *
   * @param {string} holder
   * @param {string} candidate
   * @param {string[]} tags
   * @param {string[]} skills
   * @param {string} project
   * @param {number} min_reward
   * @param {number} max_reward
   * @param {string} task
   * @param {boolean} attributed
   * @param {string} name
   * @param {number} nb
   * @param {number} offset
   * @returns {Observable<ProjectJob[]>}
   */
  public getJobs( { holder = '',
                    candidate = '',
                    tags = [],
                    skills = [],
                    project = '',
                    min_reward = 0,
                    max_reward = 0,
                    task = '',
                    attributed = null,
                    name = '',
                    nb = 20,
                    offset = 0}:
                    { holder?: string,
                      candidate?: string,
                      tags?: string[],
                      skills?: string[],
                      project?: string,
                      min_reward?: number,
                      max_reward?: number,
                      task?: string,
                      attributed?: boolean,
                      name?: string,
                      nb?: number,
                      offset?: number} ): Observable<ProjectJob[]> {

    const url = environment.api_url + '/projects/jobs';

    let params: HttpParams = new HttpParams()
      .set('offset', offset.toString())
      .set('nb', nb.toString());

    if (holder !== '') { params = params.set('holder', holder); }
    if (candidate !== '') { params = params.set('candidate', candidate); }
    if (project !== '') { params = params.set('project', project); }
    if (min_reward !== 0) { params = params.set('min_reward', min_reward.toString()); }
    if (max_reward !== 0) { params = params.set('max_reward', max_reward.toString()); }
    if (task !== '') { params = params.set('task', task); }
    if (name !== '') { params = params.set('name', name); }

    for (const tag of tags) {
      params = params.append('tags[]', tag);
    }

    for (const skill of skills) {
      params = params.append('skills[]', skill);
    }

    if (attributed !== null) { params = params.set('attributed', attributed.toString()); }

    return this.http.get<ProjectJob[]>(url, { params: params })
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Fetches a specific job.
   *
   * @param {string} job Hash of the job to fetch
   * @returns {Observable<ProjectJob[]>}
   */
  public getProjectJob(job: string): Observable<ProjectJob> {

    const url = environment.api_url + '/projects/jobs/' + job;

    return this.http.get<ProjectJob>(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Invite a user to an existing project.
   *
   * @param {string} project Hash of the project in which the job should be added
   * @param job Job to create for the project
   * @returns {Observable<ProjectJob>}
   */
  public postProjectJob(project: string, job: ProjectJobDto): Observable<ProjectJob> {

    const url = environment.api_url + '/projects/' + project + '/jobs';

    return this.http.post<ProjectJob>(url, job)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Invite a user to an existing project.
   *
   * @param {string} job Hash of the job to update
   * @param updatedJob Job to update
   * @returns {Observable<ProjectJob>}
   */
  public putProjectJob(job: string, updatedJob: ProjectJobDto): Observable<ProjectJob> {

    const url = environment.api_url + '/projects/jobs/' + job;

    return this.http.put<ProjectJob>(url, updatedJob)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Deletes a job
   *
   * @param {string} job
   * @returns {Observable<any>}
   */
  public deleteProjectJob(job: string): Observable<any> {
    const url = environment.api_url + '/projects/jobs/' + job;

    return this.http.delete(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }
}
