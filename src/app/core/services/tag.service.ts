import {throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Tag } from '../../shared/models/tags/tag.model';
import { environment } from 'environments/environment';

@Injectable()
export class TagService {

  tags_url = environment.api_url + '/tags';

  constructor(public http: HttpClient) {}

  getTagsHttp(): Observable<Tag[]> {
    return this.http.get<Tag[]>(this.tags_url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }


  postTag(name: string): Observable<Tag> {

    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    const body = 'name=' + name;

    return this.http.post<Tag>(this.tags_url, body, { headers: headers })
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

}
