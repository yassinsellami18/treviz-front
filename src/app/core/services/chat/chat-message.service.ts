import {throwError as observableThrowError, Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from 'environments/environment';
import { HttpParams } from '@angular/common/http';
import {Message} from '../../../shared/models/chat/message.model';
import {MessageDto} from '../../../shared/models/chat/message.model.dto';

/**
 * Created by huber on 11/07/2017.
 */
@Injectable()
export class ChatMessageService {

  private roomUrl = environment.api_url + '/rooms/';

  constructor(private http: HttpClient) {}

  /**
   * Fetches the messages of a chat room
   * @param {string} room Hash of the room from which fetch the messages
   * @param limit Max number of messages to fetch
   * @param offset Pagination
   * @param timestamp Date from which fetch the messages
   * @returns {Observable<Message[]>}
   */
  public getMessages(room: string,
                     {limit = 20, offset = 0, date = null}: {limit?: number, offset?: number, date?: Date}): Observable<Message[]> {
    const url = this.roomUrl + room + '/messages';
    let params = new HttpParams()
      .set('offset', offset.toString())
      .set('limit', limit.toString());

    if (date) { params = params.set('since', date.toString()); }

    return this.http.get<Message[]>(url, { params: params })
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Adds a new message to an existing chat room
   * @param {string} room Hash of the room to which post the message
   * @param {MessageDto} message Message to post
   * @returns {Observable<Message>}
   */
  public postMessage(room: string, message: MessageDto): Observable<Message> {
    const url = this.roomUrl + room + '/messages';
    return this.http.post<Message>(url, message)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Updates a specified message
   * @param {string} message Hash of the message to update
   * @param {MessageDto} updatedMessage Updated message to send
   * @returns {Observable<Message>}
   */
  public putMessage(message: string, updatedMessage: MessageDto): Observable<Message> {
    const url = this.roomUrl + '/messages/' + message;
    return this.http.put<Message>(url, updatedMessage)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Deletes a specified message
   * @param {string} message Hash of the message to delete
   * @returns {Observable<any>}
   */
  public deleteMessage(message: string): Observable<any> {
    const url = this.roomUrl + '/messages/' + message;
    return this.http.delete(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

}
