import { throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';
import { User } from '../../shared/models/users/user.model';
import { Skill } from '../../shared/models/tags/skill.model';
import { Tag } from '../../shared/models/tags/tag.model';
import { environment } from 'environments/environment';
import { UserDto } from '../../shared/models/users/user.model.dto';

@Injectable()
export class UserService {

  usersUrl = environment.api_url + '/users';

  constructor(public http: HttpClient) {}

  /**
   * Fetches an array of users according to a query.
   * @param {string} name Name of the user to fetch
   * @param {string[]} tags Array of tag names the user should have.
   * @param {string[]} skills Array of skill names the user should have.
   * @param {number} offset
   * @param {number} nb
   * @returns {Observable<User[]>}
   */
  getUsers({ name = '',
             tags = [],
             skills = [],
             offset = 0,
             nb = 10}:
             { name?: string,
               tags?: string[],
               skills?: string[],
               offset?: number,
               nb?: number} ): Observable<User[]> {

    let params: HttpParams = new HttpParams()
      .set('offset', offset.toString())
      .set('nb', nb.toString());

    if (name !== '') {
      params = params.set('name', name);
    }

    for (const tag of tags) {
      params = params.append('tags[]', tag);
    }

    for (const skill of skills) {
      params = params.append('skills[]', skill);
    }

    return this.http.get<User[]>(this.usersUrl, { params: params })
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  getUsersHttp(search: string, interests: Tag[] = [], skills: Skill[] = [], offset = 0, nb = 20): Observable<User[]> {

    let params: HttpParams = new HttpParams()
      .set('nb', nb.toString())
      .set('offset', offset.toString());

    if (search) {
      params = params.set('name', search);
    }

    for (const interest of interests) {
      params = params.append('interests[]', interest.id.toString());
    }

    for (const skill of skills) {
      params = params.append('skills[]', skill.id.toString());
    }

    return this.http.get<User[]>(this.usersUrl, { params: params })
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  putUser(username: string, updatedUser: UserDto): Observable<User> {
    const url = this.usersUrl + '/' + username;

    return this.http.put<User>(url, updatedUser)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  getUser(username: string): Observable<User>  {
    return this.http.get<User>(this.usersUrl + '/' + username)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  getCurrentUser(): Observable<User> {
    return this.getUser(localStorage.getItem('username'));
  }

  postAvatar(file: File, user: User): Observable<User>  {

    const formData: FormData = new FormData();
    formData.append('avatar', file, file.name);

    const url = this.usersUrl + '/' + user.username + '/avatar';

    return this.http.post<User>( url , formData)
      .catch(error => observableThrowError(error));
  }

  postBackgroundImage(file: File, user: User): Observable<User> {

    const formData: FormData = new FormData();
    formData.append('background', file, file.name);

    const url = this.usersUrl + '/' + user.username + '/background';

    return this.http.post<User>( url , formData)
      .catch(error => observableThrowError(error));
  }

}
