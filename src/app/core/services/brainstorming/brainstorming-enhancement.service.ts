import { throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { BrainstormingEnhancement } from '../../../shared/models/brainstorming/brainstorming-enhancement.model';
import { BrainstormingEnhancementDto } from '../../../shared/models/brainstorming/brainstorming-enhancement.model.dto';

/**
 * Created by huber on 26/08/2017.
 * Interacts with Brainstorming Enhancements API.
 */
@Injectable()
export class BrainstormingEnhancementService {

  private brainstormingUrl = environment.api_url + '/brainstorming-sessions/ideas/';

  constructor(private http: HttpClient) {}

  /**
   * Fetches the enhancements of a brainstorming idea.
   * @param {string} idea Hash of the brainstorming idea from which fetch the enhancements
   * @returns {Observable<BrainstormingEnhancement[]>}
   */
  getIdeaEnhancements(idea: string): Observable<BrainstormingEnhancement[]> {
    const url = this.brainstormingUrl + idea + '/enhancements';
    return this.http.get<BrainstormingEnhancement[]>(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Fetches a specific enhancement.
   * @param {string} enhancement Hash of the enhancement to fetch
   * @returns {Observable<BrainstormingEnhancement>}
   */
  getEnhancement(enhancement: string): Observable<BrainstormingEnhancement> {
    const url = this.brainstormingUrl + 'enhancements/' + enhancement;
    return this.http.get<BrainstormingEnhancement>(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Creates a new enhancement into an existing brainstorming idea.
   * @param {string} idea Hash of the idea in which create the enhancement
   * @param {BrainstormingEnhancementDto} enhancement Enhancement to create
   * @returns {Observable<BrainstormingEnhancement>}
   */
  postEnhancement(idea: string, enhancement: BrainstormingEnhancementDto): Observable<BrainstormingEnhancement> {
    const url = this.brainstormingUrl + idea + '/enhancements';
    return this.http.post<BrainstormingEnhancement>(url, enhancement)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Updates an existing enhancement.
   * @param {string} enhancement Hash of the enhancement to edit
   * @param {BrainstormingEnhancementDto} updatedEnhancement New description of the enhancement
   * @returns {Observable<BrainstormingEnhancement>}
   */
  putEnhancement(enhancement: string, updatedEnhancement: BrainstormingEnhancementDto): Observable<BrainstormingEnhancement> {
    const url = this.brainstormingUrl + 'enhancements/' + enhancement;
    return this.http.put<BrainstormingEnhancement>(url, updatedEnhancement)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Deletes an existing enhancement
   * @param {string} enhancement
   * @returns {Observable<any>}
   */
  deleteEnhancement(enhancement: string): Observable<any> {
    const url = this.brainstormingUrl + 'enhancements/' + enhancement;
    return this.http.delete(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

}
