import { throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { BrainstormingIdea } from '../../../shared/models/brainstorming/brainstorming-idea.model';
import { BrainstormingIdeaDto } from '../../../shared/models/brainstorming/brainstorming-idea.model.dto';

/**
 * Created by huber on 26/08/2017.
 * Interacts with Brainstorming Ideas API.
 */
@Injectable()
export class BrainstormingIdeaService {

  private brainstormingUrl = environment.api_url + '/brainstorming-sessions/';

  constructor(private http: HttpClient) {}

  /**
   * Fetches the ideas of a brainstorming session.
   * @param {string} session Hash of the brainstorming session from which fetch the ideas
   * @returns {Observable<BrainstormingIdea[]>}
   */
  getIdeas(session: string): Observable<BrainstormingIdea[]> {
    const url = this.brainstormingUrl + session + '/ideas';
    return this.http.get<BrainstormingIdea[]>(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Fetches a specific idea.
   * @param {string} idea Hash of the idea to fetch
   * @returns {Observable<BrainstormingIdea>}
   */
  getIdea(idea: string): Observable<BrainstormingIdea> {
    const url = this.brainstormingUrl + 'ideas/' + idea;
    return this.http.get<BrainstormingIdea>(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Creates a new idea into an existing brainstorming session.
   * @param {string} session Hash of the session in which create the idea
   * @param {BrainstormingIdeaDto} idea Idea to create
   * @returns {Observable<BrainstormingIdea>}
   */
  postIdea(session: string, idea: BrainstormingIdeaDto): Observable<BrainstormingIdea> {
    const url = this.brainstormingUrl + session + '/ideas';
    return this.http.post<BrainstormingIdea>(url, idea)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Updates an existing idea.
   * @param {string} idea Hash of the idea to edit
   * @param {BrainstormingIdeaDto} updatedIdea New description of the idea
   * @returns {Observable<BrainstormingIdea>}
   */
  putIdea(idea: string, updatedIdea: BrainstormingIdeaDto): Observable<BrainstormingIdea> {
    const url = this.brainstormingUrl + 'ideas/' + idea;
    return this.http.put<BrainstormingIdea>(url, updatedIdea)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Deletes an existing idea
   * @param {string} idea
   * @returns {Observable<any>}
   */
  deleteIdea(idea: string): Observable<any> {
    const url = this.brainstormingUrl + 'ideas/' + idea;
    return this.http.delete(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Adds the current user as one of the members who like this idea.
   * @param {string} idea
   * @returns {Observable<any>}
   */
  likeIdea(idea: string): Observable<any> {
    const url = this.brainstormingUrl + 'ideas/' + idea + '/like';
    return this.http.get(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Removes the current user from the list of members who like this idea.
   * @param {string} idea
   * @returns {Observable<any>}
   */
  unlikeIdea(idea: string): Observable<any> {
    const url = this.brainstormingUrl + 'ideas/' + idea + '/unlike';
    return this.http.get(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }
}
