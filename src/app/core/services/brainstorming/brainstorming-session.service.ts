import {throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import { BrainstormingSession } from '../../../shared/models/brainstorming/brainstorming-session.model';
import { BrainstormingSessionDto } from '../../../shared/models/brainstorming/brainstorming-session.model.dto';

/**
 * Created by huber on 26/08/2017.
 * CRUD for Brainstorming sessions.
 */
@Injectable()
export class BrainstormingSessionService {

  private communityUrl = environment.api_url + '/brainstorming-sessions';

  constructor(private http: HttpClient) {}

  /**
   * Fetches the sessions of a project or community.
   * @param {string} project Hash of the project from which fetch the sessions.
   * @param {string} community Hash of the community from which fetch the sessions.
   * @returns {Observable<BrainstormingSession[]>}
   */
  getSessions({project = '', community = ''}: {project?: string, community?: string}): Observable<BrainstormingSession[]> {

    let params = new HttpParams();
    if (project !== '') { params = params.append('project', project); }
    if (community !== '') { params = params.append('community', community); }

    return this.http.get<BrainstormingSession[]>(this.communityUrl, { params: params })
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Fetches a brainstorming session according to its hash.
   * @param {string} session Hash of the session to fetch.
   * @returns {Observable<BrainstormingSession>}
   */
  getSession(session: string): Observable<BrainstormingSession> {
    const url = this.communityUrl + '/' + session;
    return this.http.get<BrainstormingSession>(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Creates a new brainstorming session.
   * @param {BrainstormingSessionDto} sessionDto
   * @returns {Observable<BrainstormingSession>}
   */
  postSession(sessionDto: BrainstormingSessionDto): Observable<BrainstormingSession> {
    return this.http.post<BrainstormingSession>(this.communityUrl, sessionDto)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Updates an existing brainstorming session.
   * @param {string} session Hash of the session to update
   * @param {BrainstormingSessionDto} updatedSession Updated session.
   * @returns {Observable<BrainstormingSession>}
   */
  putSession(session: string, updatedSession: BrainstormingSessionDto): Observable<BrainstormingSession> {
    const url = this.communityUrl + '/' + session;
    return this.http.put<BrainstormingSession>(url, updatedSession)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }

  /**
   * Deletes an existing brainstorming session.
   * @param {string} session Hash of the session to delete
   * @returns {Observable<any>}
   */
  deleteSession(session: string): Observable<any> {
    const url = this.communityUrl + '/' + session;
    return this.http.delete(url)
      .catch((error: any) => observableThrowError(error.json() || 'Server Error'));
  }
}
