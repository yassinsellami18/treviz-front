import {Component, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {MatDialog, MatSidenav} from '@angular/material';
import {AuthService} from '../services/auth/auth.service';
import {CurrentUserService} from '../services/current-user.service';
import {InfoDialogComponent} from '../../info/info.component';
import {NotificationService} from '../services/notification.service';

@Component({
    moduleId: module.id,
    selector: 'app-nav',
    templateUrl: 'nav.component.html',
    styleUrls: ['nav.component.scss']
})
export class NavComponent implements OnInit, OnDestroy {

  username: string;
  smallScreen = false;
  sidenavToggled = false;
  component: string;

  @ViewChild('sidenav')
  sidenav: MatSidenav;

  private alive = true;

  constructor(public auth: AuthService,
              public dialog: MatDialog,
              public router: Router,
              public activatedRoute: ActivatedRoute,
              public notificationService: NotificationService,
              public currentUserService: CurrentUserService) {
              }

  isHomePage(): boolean {
    return this.router.url === '/';
  }

  isCurrentRoute(route: string): boolean {
    return this.router.url.startsWith(route);
  }

  ngOnInit() {
    this.configureSideNav();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  /**
   * Logs out from the application, and reset its state:
   *  - remove the current information of the user
   *  - disconnect from the notification websocket and remove any pending notification display
   *  - remove any  other stored information
   */
  logout() {
    if (this.smallScreen) {
      this.sidenav.mode = 'side';
      this.sidenav.close();
    }
    this.notificationService.disconnectToNotificationWs();
    this.currentUserService.clear();
    localStorage.clear();
    this.router.navigate(['/login']);
  }

  goToProfile() {
    this.router.navigate(['/users', localStorage.getItem('username')]);
    if (this.smallScreen) {
      this.sidenav.mode = 'side';
      this.sidenav.close();
    }
  }

  openInfo() {
    this.dialog.open(InfoDialogComponent);
  }

  goToHomepage() {
    this.router.navigate(['']);
    if (this.smallScreen) {
      this.sidenav.mode = 'side';
      this.sidenav.close();
    }
  }

  goToProjects() {
    this.router.navigate(['/projects']);
    if (this.smallScreen) {
      this.sidenav.mode = 'side';
      this.sidenav.close();
    }
  }

  goToBoards() {
    this.router.navigate(['/boards']);
    if (this.smallScreen) {
      this.sidenav.mode = 'side';
      this.sidenav.close();
    }
  }

  goToJobs() {
    this.router.navigate(['/jobs']);
    if (this.smallScreen) {
      this.sidenav.mode = 'side';
      this.sidenav.close();
    }
  }

  goToUsers() {
    this.router.navigate(['/users']);
    if (this.smallScreen) {
      this.sidenav.mode = 'side';
      this.sidenav.close();
    }
  }

  goToCommunities() {
    this.router.navigate(['/communities']);
    if (this.smallScreen) {
      this.sidenav.mode = 'side';
      this.sidenav.close();
    }
  }

  goToChat() {
    this.router.navigate(['/chat']);
    if (this.smallScreen) {
      this.sidenav.mode = 'side';
      this.sidenav.close();
    }
  }

  openSettings() {
    this.router.navigate(['/settings']);
    if (this.smallScreen) {
      this.sidenav.mode = 'side';
      this.sidenav.close();
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.configureSideNav();
  }

  /**
   * Detects the size of the display. If it is small (for tablets or phones), hide the sidebar and set its mode
   * to 'over'. Otherwise, the sidebar is displayed side-by-side with the application content.
   */
  configureSideNav() {
    this.smallScreen = window.innerWidth <= 768;
    if (!this.auth.loggedIn()) {
      this.sidenav.opened = false;
    } else if (!this.smallScreen) {
      this.sidenav.mode = 'side';
      this.sidenav.opened = true;
    } else {
      this.sidenav.mode = 'over';
      this.sidenav.opened = false;
    }
  }

  toggleSideNav() {
    this.sidenavToggled = true;
    this.smallScreen = window.innerWidth <= 768;
    if (!this.smallScreen) {
      this.sidenav.mode = 'side';
      this.sidenav.toggle();
    } else {
      this.sidenav.mode = 'over';
      if (this.sidenav.opened) {
        this.sidenav.close();
      } else {
        this.sidenav.open();
      }
    }
  }

}
