import { Component, OnDestroy } from '@angular/core';
import { AuthService } from '../../core/services/auth/auth.service';
import { CurrentUserService } from '../../core/services/current-user.service';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ChatRoomService } from '../../core/services/chat/chat-room.service';
import { NotificationService } from '../../core/services/notification.service';
import { Observable } from 'rxjs';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['../shared/not-logged-style.scss']
})
export class LoginComponent implements OnDestroy {

  isSubmitted = false;
  errorMessage = '';
  credentials = {
    username: '',
    password: ''
  };

  private alive = true;

  constructor(public auth: AuthService,
              public jwtHelper: JwtHelperService,
              private router: Router,
              private chatService: ChatRoomService,
              private notificationService: NotificationService,
              private currentUserService: CurrentUserService) {}

  ngOnDestroy() {
    this.alive = false;
  }

  onLogin() {

    this.isSubmitted = true;
    this.errorMessage = '';

    this.auth.login(this.credentials.username, this.credentials.password)
      .takeWhile(() => this.alive)
      .subscribe(
        // We're assuming the response will be an object
        // with the JWT on an 'token' key
        data => {
          this.isSubmitted = false;
          localStorage.setItem('access_token', data.token);
          localStorage.setItem('username', this.jwtHelper.decodeToken(data.token).username);

          this.currentUserService.loadCurrentUser();
          this.currentUserService.loading
            .subscribe(
              state => {
                if (!(state.loadingRooms || state.loadingCommunities || state.loadingProjects || this.notificationService.listening)) {
                  this.notificationService.listenToNotifications();
                }
                if (!state.loadingUser) {
                  this.router.navigate(['']);
                }
              }
            );

        },
        error => {
          const err = JSON.parse(error._body);
          if (err.code === 401) {
            this.errorMessage = 'Invalid credentials';
          } else {
            this.errorMessage = err.message;
          }

          this.isSubmitted = false; }
      );
  }

}


