import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../core/services/auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserService } from '../../core/services/user.service';
import { CurrentUserService } from '../../core/services/current-user.service';

@Component({
  selector: 'app-register-confirmation',
  templateUrl: 'register-confirmation.component.html',
  styleUrls: ['../shared/not-logged-style.scss']
})
export class RegisterConfirmationComponent implements OnInit, OnDestroy {

  message = '';

  private alive = true;

  constructor(private route: ActivatedRoute,
              private router: Router,
              public auth: AuthService) {}

  ngOnInit(): void {

    this.route.queryParams
      .subscribe(params => {
        this.auth.confirmUser(params['user'], params['token'])
          .takeWhile(() => this.alive)
          .subscribe(
          data => {
            this.message = 'User successfully activated';
          },
          error => {
            console.log(error);
            this.message = error;
          }
        );
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }

}
