import { Component, OnDestroy } from '@angular/core';
import { AuthService } from '../../core/services/auth/auth.service';
import { User } from '../../shared/models/users/user.model';


@Component({
  selector: 'app-register',
  templateUrl: 'register.component.html',
  styleUrls: ['../shared/not-logged-style.scss']
})

export class RegisterComponent implements OnDestroy {

  displayError = false;
  errorMessage: string;
  isSubmitted = false;
  registrationSuccessFull = false;
  model = new User();

  private alive = true;

  constructor(public auth: AuthService) {}

  ngOnDestroy() {
    this.alive = false;
  }

  onRegister() {

    this.displayError = false;
    this.isSubmitted = true;

    console.log(this.model)

      this.auth.register(this.model)
        .takeWhile(() => this.alive)
        .finally(() => this.isSubmitted = false)
        .subscribe(
          data => {
            this.registrationSuccessFull = true;
          },
          error => {
            this.displayError = true;
            this.errorMessage = error;
          }
        );
  }
}
