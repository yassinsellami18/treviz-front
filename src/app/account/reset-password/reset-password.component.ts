import { Component } from '@angular/core';
import { AuthService } from '../../core/services/auth/auth.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../core/services/user.service';
import { CurrentUserService } from '../../core/services/current-user.service';

/**
 * Created by Bastien on 20/03/2017.
 */

@Component({
  selector: 'app-reset-password',
  templateUrl: 'reset-password.component.html',
  styleUrls: ['../shared/not-logged-style.scss']
})
export class ResetPasswordComponent {

  password1: '';
  password2: '';
  isSubmitted = false;
  success = false;
  message = '';

  constructor(public auth: AuthService,
              private route: ActivatedRoute,
              private userService: UserService,
              private router: Router) {}

  sendResetRequest() {

    this.isSubmitted = true;
    this.route.queryParams
      .subscribe( params => {
        this.auth.sendResetPassword(params['user'], params['token'], this.password1)
          .finally(() => this.isSubmitted = false)
          .subscribe(
            data => {
              this.message = 'Password successfully reset';
              this.success = true;
            },
            error => {
              console.log(error);
              this.message = error;
            }
          );
      });
  }

}
