/**
 * Created by Bastien on 20/03/2017.
 */

import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../../core/services/auth/auth.service';
import { Router } from '@angular/router';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-forgot-password',
  templateUrl: 'forgot-password.component.html',
  styleUrls: ['../shared/not-logged-style.scss']
})
export class ForgotPasswordComponent implements OnDestroy {

  email: '';
  errorMessage = '';
  isSubmitted = false;
  emailSent = false;
  private alive = true;

  constructor(public auth: AuthService,
              private router: Router) { }

  ngOnDestroy() {
    this.alive = false;
  }

  sendResetRequest() {

    this.errorMessage = '';
    this.isSubmitted = true;

    this.auth.sendResetRequest(this.email)
      .takeWhile(() => this.alive)
      .finally(() => this.isSubmitted = false)
      .subscribe(
        data => this.emailSent = true,
        err => {
          if (err.status === 404) {
            this.errorMessage = 'Unknown email address';
          } else {
            this.errorMessage = err._body;
          }
        }
      );
  }

  goToLogin() {
    this.router.navigate(['/login']);
  }

}
