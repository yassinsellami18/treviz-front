export const environment = {
  production: true,
  api_host: 'api.treviz.org',
  api_url: 'https://api.treviz.org/v1', // replace by your own api
  notifications_socket_url: 'wss://websocket.treviz.org/ws' // replace by your own websocket
};
