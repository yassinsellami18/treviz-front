FROM node:latest as builder
WORKDIR /var/www/app
ARG API_URL
ARG WEBSOCKET_URL
COPY ./package*.json ./
RUN npm install
COPY . ./
RUN sed -ri -e 's!https://api.treviz.org/v1!'$API_URL'!g' ./src/environments/environment.prod.ts
RUN sed -ri -e 's!wss://websocket.treviz.org/ws!'$WEBSOCKET_URL'!g' ./src/environments/environment.prod.ts
RUN npx ng build --prod

FROM nginx:alpine
WORKDIR /usr/share/nginx/html
COPY --from=builder /var/www/app/dist ./
RUN rm /etc/nginx/conf.d/default.conf
COPY ./webserver-conf/treviz-front.conf /etc/nginx/conf.d/default.conf