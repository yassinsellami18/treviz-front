import { IMTestFrontendPage } from './app.po';

describe('imtest-frontend App', function() {
  let page: IMTestFrontendPage;

  beforeEach(() => {
    page = new IMTestFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
