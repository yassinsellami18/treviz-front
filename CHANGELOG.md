## 0.4.3 (UNRELEASED)
### Added
* New board view
* New display for Sidenav on large screens
* Adds service worker to make Treviz a PWA
* Projects can be forked

### Chore
* Migrate the application to Angular 6
* Use Docker for Deployment
* Community candidacies can be accepted from the main community page

### Fixed
* User candidacies now display correctly on home screen
* Community admins can now correctly access the team management option

## 0.4.2 (2018-04-22)
### Fixed (3 change)
* Allow scroll on sidenav on small screens
* Sets a defaut background for community thumbnails when no logo is specified
* Updated Changelog with guidelines for fixes

## 0.4.1 (2018-01-15)
### Added (1 change)
* Add a Changelog

### Fixed (2 changes)
* Fix user list component so that newly loaded users do not overwrite the previously loaded ones
* Fix sidenav so that it does not open on application start when the user is not yet logged in
